// JavaScript Document

//////////////////////////
////////////////////////
//Qs
var qs;
//var qs={ "q1": { "title": "Get Moving Challenge - one of two", "qTxt": "What is the force that powers a land yacht?", "ans" : ["Powered by a combustion engine","A solar powered engine","Powered by the wind","Powered by nuclear power"], "helpTxt" : "In order to create motion, objects and vehicles often require another force or source of energy to help them do this. Currently, most cars rely on petroleum to fuel their engine. These cars produce carbon dioxide as a by-product that pollutes the atmosphere. Many different organizations recognize that this is unsustainable and are experimenting with other ways of powering vehicles including solar power. <br> <br>Given that the skipper's craft doesn't appear to be powered by either petroleum or solar power, work out what is the most logical answer. ", "offset": 3 }, "q2": { "title": "Get Moving Challenge - two of two", "qTxt": "When an object accelerates using the power of another force it is subject too?", "ans" : ["Newton's First Law","Newton's Second Law","Newton's Third Law","Newtown's Third Law"], "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 2 }, "q3": { "title": "De-sal Plant challenge", "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", }, "q4": { "title": "Travel Time Challenge - one of two", "qTxt": "Which of the formula below would you use to find out the amount of time taken to travel somewhere?", "ans" : ["Time = Speed/Distance","Time = Speed x Distance","Time = Distance x Speed","Time = Distance/Speed"], "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 4 }, "q5": { "title": "Travel Time Challenge - two of two", "qTxt": "If the right formula is Time = Distance/Speed, and we know that the De-Sal plant in 65km a way and that the land yacht travels 40km an hour how long would it take approximately to the nearest rounded up tens of minutes?", "ans" : ["It would take approximately 1 hr 40 mim","It would take approximately 2 hrs","It would take approximately 1 hr 30 min","It would tale approximately 1 hr 50 min"], "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 1 }, "q6": { "title": "Leaving the plant", "qTxt": "Now that you've re-started the desalination plant you'll need to figure out how far it is to the city, the captain mentioned it will take 30 mins travelling at 40 km an hour. How far away is the city?", "ans" : ["16km away","18km away","20km away","22km away"], "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 3 }, };


///////////////////////////


function getQs() {
/*	$.get("../resource/6.php", 'json',  { v: "2" },
   function(data){
     //alert("Data Loaded: " + data);
	 qs=data;
   })*/
   
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.open("GET", "../resource/6.php?v=2",true);
 xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState==4) {
   //qs=JSON.parse(xmlhttp.responseText);
   eval("qs = (" + xmlhttp.responseText + ")");
   
  }
 }
 xmlhttp.send(null);
	
	}
function showQ1() {
	
	document.getElementById('q1').style.display='block';
	document.getElementById('q1').style.opacity=1;
	setTimeout (function(){document.getElementById('q1-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-1');},800);
	setTimeout (function(){hideDialogue('s1-dia-1'); document.getElementById('q1-splash').style.opacity=0;},6000);
	
	/*setTimeout (function(){
		hideDialogue('s1-dia-1'); document.getElementById('q1-splash').style.opacity=0;
		},12000);*/
		
	setTimeout (function(){
		mkQs('q1');
		document.getElementById('q1-a').style.display='block';
		startRecording();
		},6500);
	}
	
function showQ2(){
	document.getElementById('q1-ans').innerHTML='';
	document.getElementById('q1').style.display='none';
	document.getElementById('q2').style.display='block';
	document.getElementById('q2').style.opacity=1;
	setTimeout (function(){document.getElementById('q2-splash').style.opacity=1;},100);
	setTimeout (function(){ document.getElementById('q2-splash').style.opacity=0;},4500);
	
	setTimeout (function(){
		mkQs('q2');
		document.getElementById('q2-a').style.display='block';
		startRecording();
		},1000);
	}
	
function showQ3 () {
	document.getElementById('q3').style.display='block';
	document.getElementById('q3').style.opacity=1;
	setTimeout (function(){document.getElementById('q3-splash').style.opacity=1;},100);
	setTimeout (function(){showDialogue('s1-dia-2');},800);
	setTimeout (function(){hideDialogue('s1-dia-2'); document.getElementById('q3-splash').style.opacity=0;},5000);
	
	setTimeout (function(){
	mkDragables3();
	listen_3();
	document.getElementById('q3-a').style.display='block';
	startRecording();
	var which='q3';
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
	},5500);
	}
	
function showQ4(){
	document.getElementById('q2-ans').innerHTML='';
	document.getElementById('q4').style.display='block';
	document.getElementById('q4').style.opacity=1;
	setTimeout (function(){document.getElementById('q4-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-3');},800);
	setTimeout (function(){hideDialogue('s1-dia-3'); document.getElementById('q4-splash').style.opacity=0;},7000);
	
	setTimeout (function(){
		mkQs('q4');
		document.getElementById('q4-a').style.display='block';
		startRecording();
		},7500);
	
	}
	
function showQ5(){
	document.getElementById('q4-ans').innerHTML='';
	document.getElementById('q5').style.display='none';
	document.getElementById('q5').style.display='block';
	document.getElementById('q5').style.opacity=1;
	setTimeout (function(){document.getElementById('q5-splash').style.opacity=1;},100);
	setTimeout (function(){ document.getElementById('q5-splash').style.opacity=0;},4500);
	
	setTimeout (function(){
		mkQs('q5');
		document.getElementById('q5-a').style.display='block';
		startRecording();
		},1500);
	}
	
function showQ6(){
	document.getElementById('q5-ans').innerHTML='';
	document.getElementById('q5').style.display='none';
	document.getElementById('q6').style.display='block';
	document.getElementById('q6').style.opacity=1;
	setTimeout (function(){document.getElementById('q6-splash').style.opacity=1;},100);
	setTimeout (function(){ document.getElementById('q6-splash').style.opacity=0;},4500);
	
	setTimeout (function(){
		mkQs('q6');
		document.getElementById('q6-a').style.display='block';
		startRecording();
		},1500);
	}
	
function hideQ2() {
	document.getElementById('q2-ans').innerHTML='';
	document.getElementById('q2').style.opacity=0;
	setTimeout(function(){document.getElementById('q2').style.display='none';},2000);
	
	}
function hideQ1() {
	document.getElementById('q1-ans').innerHTML='';
	document.getElementById('q1').style.opacity=0;
	setTimeout(function(){document.getElementById('q1').style.display='none';},2000);
	
	}
	
function hideQ3() {
	//document.getElementById('q1-ans').innerHTML='';
	document.getElementById('q3').style.opacity=0;
	setTimeout(function(){document.getElementById('q3').style.display='none';},2000);
	
	}
	
function hideQ4() {
	document.getElementById('q4-ans').innerHTML='';
	document.getElementById('q4').style.opacity=0;
	setTimeout(function(){document.getElementById('q4').style.display='none';},2000);
	
	}
	
function hideQ5() {
	document.getElementById('q5-ans').innerHTML='';
	document.getElementById('q5').style.opacity=0;
	setTimeout(function(){document.getElementById('q5').style.display='none';},2000);
	
	}

function hideQ6() {
	document.getElementById('q6-ans').innerHTML='';
	document.getElementById('q6').style.opacity=0;
	setTimeout(function(){document.getElementById('q6').style.display='none';},2000);
	
	}
function mkQs(which) {
	document.getElementById(which+'-title').innerHTML=qs[which].title;
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-question').innerHTML=qs[which].qTxt;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
	
	var a1=qs[which].ans[0];
	var a2=qs[which].ans[1];
	var a3=qs[which].ans[2];
	var a4=qs[which].ans[3];

var rads=		  "<input type=\"radio\" name=\"ans\" id=\""+which+"_1\" value=\"ans1\" onclick=\"chkAns('"+which+"','1')\" />  <label for=\""+which+"_1\">"+a1+"</label> <div id=\""+which+"_1X\" class='cross blink_continous'>  X  </div><br />";
        
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_2\"value=\"ans2\" onclick=\"chkAns('"+which+"','2')\" />  <label for=\""+which+"_2\">"+a2+"</label> <div id=\""+which+"_2X\" class='cross blink_continous'>  X  </div><br />"; 
		
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_3\" value=\"ans3\" onclick=\"chkAns('"+which+"','3')\" />  <label for=\""+which+"_3\">"+a3+"</label><div id=\""+which+"_3X\" class='cross blink_continous'>  X  </div><br />";
		
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_4\" value=\"ans4\" onclick=\"chkAns('"+which+"','4')\" />  <label for=\""+which+"_4\">"+a4+"</label> <div id=\""+which+"_4X\" class='cross blink_continous'>  X  </div><br />";         
	
		//console.log(rads);
	document.getElementById(which+'-ans').innerHTML= rads;
	
		$('input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	
	
	}
	
var iShouldWait=false;

function chkAns(qNo, ansNo) {

if (iShouldWait) return;

if (qs[qNo].offset == ansNo) {
	
	//sounds[48].play();
	playSound(48);
	//console.log('v2-'+qNo+':1');
	localStorage.setItem('v2-'+qNo,'1');
	recLap();
	stopRecording();
	var temp =  qNo.replace(/^q+/, "");
	sendResults(2,parseInt(temp),attempt,matrixStr);
	}
	
else{
	//playSound('49');
	recLap();
	document.getElementById(qNo+"_"+ansNo+"X").style.display='block';
	setTimeout (function () {document.getElementById(qNo+"_"+ansNo+"X").style.display='none';} , 700);
	//sounds[49].play();
	playSound(49);
	}
	
	
iShouldWait=true;
setTimeout(function(){iShouldWait=false;},200);

	}
	
////////////////////
var q3_1=0;
var q3_2=0;
var q3_3=0;
var q3_4=0;
var q3_5=0;
var q3_6=0;
var q3_7=0;


function listen_3() {
	if (q3_1 == 1)
	if (q3_2 == 1){
	if (q3_3 == 1){
	if (q3_4 == 1){
	if (q3_5 == 1){
	if (q3_6 == 1){
	if (q3_7 == 1){
		
		//alert('complete');
	localStorage.setItem('v2-q3','1');
	recLap();
	stopRecording();
	sendResults(2,3,attempt,matrixStr);
		return;
	}}}}}}
	

	setTimeout(listen_3,300);
	}
function mkDragables3() {
	var w = $('#q3-1').width();
	var h = $('#q3-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q1Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q3-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-3').position().top-$('#placeholder-3').parent().position().top;
	var ans_left= $('#placeholder-3').position().left-$('#placeholder-3').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_1=1;
 //sounds[48].play();
 playSound(48);
  snap2grid('q3-1','placeholder-3');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});
//
$( "#q3-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-5').position().top-$('#placeholder-5').parent().position().top;
	var ans_left= $('#placeholder-5').position().left-$('#placeholder-5').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_2=1;
//sounds[48].play();
playSound(48);
  snap2grid('q3-2','placeholder-5');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});
//
$( "#q3-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-1').position().top-$('#placeholder-1').parent().position().top;
	var ans_left= $('#placeholder-1').position().left-$('#placeholder-1').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_3=1;
// sounds[48].play();
playSound(48);
  snap2grid('q3-3','placeholder-1');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});
//
$( "#q3-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-6').position().top-$('#placeholder-6').parent().position().top;
	var ans_left= $('#placeholder-6').position().left-$('#placeholder-6').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_4=1;
// sounds[48].play();
playSound(48);
  snap2grid('q3-4','placeholder-6');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});
//
$( "#q3-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-7').position().top-$('#placeholder-7').parent().position().top;
	var ans_left= $('#placeholder-7').position().left-$('#placeholder-7').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_5=1;
//sounds[48].play();
playSound(48);
  snap2grid('q3-5','placeholder-7');
 }
 else {
	 //report
	 //sounds[49].play();
	 playSound(49);
	 }
});

//
$( "#q3-6" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-4').position().top-$('#placeholder-4').parent().position().top;
	var ans_left= $('#placeholder-4').position().left-$('#placeholder-4').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-6" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_6=1;
//sounds[48].play();
playSound(48);
  snap2grid('q3-6','placeholder-4');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

//
$( "#q3-7" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-2').position().top-$('#placeholder-2').parent().position().top;
	var ans_left= $('#placeholder-2').position().left-$('#placeholder-2').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q3-7" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q3_7=1;
//sounds[48].play();
playSound(48);
  snap2grid('q3-7','placeholder-2');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});
	}

////////////////////
	
function hasColided(obj1_left, obj1_top, obj2_left, obj2_top){
	var w = $('#q3-1').width();
	var h = $('#q3-1').height();
	var w2 = ($('#placeholder-1').width()-5);
	var h2= ($('#placeholder-1').height()-5);
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left+30;
    var br = obj2_left+30+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top+20;
    var bb = obj2_top+20+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}

