// JavaScript Document

var viewportwidth;
var viewportheight;
if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
/////////////////////////////////////
function setScreen() {
	if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
	
	var newHt= Math.round((719/1143)*viewportwidth);
	var newTop = (viewportheight - newHt)/2;
	//console.log('new Ht is ' +newHt);
	
	
	if (newHt > (viewportheight+5)) {
		searchForRight(2);
		}
		else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',viewportwidth);
	$('.scene').css('left','0px');
			}
	}
//////////////////////////////////////
function searchForRight(percent) {
		if (typeof window.innerWidth != 'undefined')
 	{
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 	}
	var newWd = viewportwidth-(viewportwidth*percent/100);
	var newHt= Math.round((719/1143)*newWd);
	var newTop = (viewportheight - newHt)/2;
	var newLeft = (viewportwidth - newWd)/2;
	
	if (newHt>viewportheight){
		searchForRight(percent+2)
		}
	else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',newWd);
	$('.scene').css('left',newLeft);
		}
	
	}
/////////////////////////////////////
function gotoScene(which){
	document.getElementById('kala').style.display='block';
	document.getElementById('iframe').style.display='block';
	document.getElementById('iframe').src=which+"?seed="+Math.random();
	}
function hideKala(){
	document.getElementById('kala').style.display='none';
	}
/////////////////////////////////////

/////////////////////////////////////
var checked=true;
function chkPrevious() {

if (localStorage.getItem('lastScene')==null){
	mkLS();
	}
setScreen();
document.getElementById('scene-0').style.display='block';
//showScene ('scene-1');
	}
/////////////////////////
function mkLS() {
	localStorage.setItem('v2-q1','0');
	localStorage.setItem('v2-q2','0');
	localStorage.setItem('v2-q3','0');
	localStorage.setItem('v2-q4','0');
	localStorage.setItem('v2-q5','0');
	localStorage.setItem('v2-q6','0');
	localStorage.setItem('v2-pangoData','');
	localStorage.setItem('v2-phoneData','');
	localStorage.setItem('secs','900');
	localStorage.setItem('mins','15');
	localStorage.setItem('lastScene','scene-2.html');
	//localStorage.setItem('key','');
	localStorage.setItem('nick','Stewie Griffin');
	//localStorage.setItem('v2-isThereElectricity','0');
	localStorage.setItem('v2-countdown','off');
	localStorage.setItem('v2-completed','000');

	}
function destroyLS(){
	localStorage.removeItem('lastScene');
	}
/////////////////////////
function storePhoneData(what) {
	var currentData=localStorage.getItem('v2-phoneData');
	currentData=currentData+what;
	localStorage.setItem('v2-phoneData',currentData);
	return currentData;
	}
////////////////////////
function storePangoData(what) {
	var currentData=localStorage.getItem('v2-pangoData');
	currentData=currentData+what;
	localStorage.setItem('v2-pangoData',currentData);
	return currentData;
	}
function storeTimer(_sec,_min){
	localStorage.setItem('secs',_sec.toString());
	localStorage.setItem('mins',_min.toString());
	}

function tempSndPlay(){
	var ext='.mp3';
	var path='snd/';
	snd= new Audio;
	//alert(path+'sfx/02_desert_wind_loopable_01'+ext);
	snd.src=path+'sfx/02_dog_howl'+ext;
	snd.play();
	setTimeout(function() {snd.pause();}, 100);
	snd.volume=0.01;
	}

//////////////////////////
var filesArray = Array (96);
var sounds = Array (52);	
var snd = new Audio;
var sndArray = Array(52);

function preload() {
	//alert('hi');
	gender=localStorage.getItem('gender');
	if (!checked) return;
	
	document.getElementById('loading').style.display='block';
	
	var path= 'snd/';
	var ext='.ogg';
	if(	BrowserDetect.browser == 'Safari') ext='.mp3';
	
	var imgArray= Array(44);
	
	//filling in the sndArray
	sndArray = [
	path+'sfx/01_desert_wind_loop'+ext,
	path+'sfx/02_dog_howl'+ext,
	path+'sfx/03_Land_yacht_cabin_noise_loop'+ext,
	path+'sfx/04_dome_control_to_security_team_a'+ext,
	path+'sfx/05_desal_interior_atmos_loopable'+ext,
	path+'sfx/06_come_to_the_dome'+ext,
	path+'sfx/07_radio_static'+ext,
	path+'sfx/08_hey_skip'+ext,
	path+'sfx/09_de_sal_atmos_loopable'+ext,
	path+'sfx/09_desal_start_up'+ext,
	path+'sfx/10_land_yacht_fade_out'+ext,
	path+'char/04_you_look_lost'+ext,
	path+'char/06_the_city_well_I_am_headed'+ext,
	path+'char/08_hey_one_question_at_a_time'+ext,
	path+'char/10_never_seen_a_land_yacht'+ext,
	path+'char/12_yeah_you_and_me_both'+ext,
	path+'char/13_are_you_coming'+ext,
	path+'char/14_about_time_you_nearly'+ext,
	path+'char/16_about_20_clicks_from'+ext,
	path+'char/19_you_dont_want_to_go_there'+ext,
	path+'char/20_nobody_fills_them_in'+ext,
	path+'char/22_remember_dont_listen_to'+ext,
	path+'char/23_can_tell_you_havent'+ext,
	path+'char/24_dont_you_hear_me'+ext,
	path+'char/25_i_hear_you_loud_'+ext,
	path+'char/27_your_learning_fast'+ext,
	path+'char/28_so_how_much_do_'+ext,
	path+'char/29_um_how_about'+ext,
	path+'char/25_i_hear_you_loud_'+ext,
	path+'char/31_of_cours_I_always'+ext,
	path+'char/33_your_a_pretty_smart'+ext,
	path+'char/34_okay_mate_best_of_luck'+ext,
	path+gender+'/01_right_now_I_need_to_get_to_the_city'+ext,
	path+gender+'/02_Oh_great'+ext,
	path+gender+'/03_hey_over_here'+ext,
	path+gender+'/04_you_look_lost-old1'+ext,
	path+gender+'/05_to_the_city_I_think'+ext,
	path+gender+'/07_who_are_you_and_whats'+ext,
	path+gender+'/09_outpost_im_not_getting_any_of'+ext,
	path+gender+'/11_i_dontreally_know'+ext,
	path+gender+'/15_so_where_exactly_are_we'+ext,
	path+gender+'/17_thats_not_exactly_what'+ext,
	path+gender+'/18_I_keep_hearing_about'+ext,
	path+gender+'/21_its_the_dome_again'+ext,
	path+gender+'/26_now_we_need_to_fix_the'+ext, //44
	path+gender+'/30_okay_ive_helped'+ext,
	path+gender+'/32_so_iver_worked_out'+ext,
	path+gender+'/35_thanks_see_you_around'+ext,
	path+'game/016_peep_01'+ext,
	path+'game/017_peep_02'+ext,
	path+'game/018_tool_tip_01'+ext,
	path+'game/019_map_green_dot_beep_01'+ext,
	];
	//filling in images
	path='imgs/';
	imgArray = [
	path+'bg1.jpg',
	path+'bg2.jpg',
	path+'bg3.jpg',
	path+'bg4.jpg',
	path+'bg5.jpg',
	path+'bg6.jpg',
	path+'bg7.jpg',
	path+'bg8.jpg',
	path+'bg9.jpg',
	path+'bg10.jpg',
	path+'book-over.png',
	path+'door-over.png',
	path+'radio-over.png',
	path+'transmittor-over.png',
	path+'fracture.png',
	path+'fracture1.png',
	path+'frame.png',
	path+'mos.png',
	path+'mute.png',
	path+'obj-1.png',
	path+'obj-2.png',
	path+'obj-3.png',
	path+'obj-4.png',
	path+'obj-1-2.png',//new
	path+'obj-2-2.png',
	path+'obj-3-2.png',
	path+'obj-4-2.png',
	path+'pangoUpdates.png',
	path+'phone.png',
	path+'welldone.png',
	path+'q5/q5.png',
	path+'q5/q5-1.png',
	path+'q5/q5-2.png',
	path+'q5/q5-3.png',
	path+'q5/q5-4.png',
	path+'q5/q5-5.png',
	path+'q5/q5-6.png',
	path+'q5/q5-7.png',
	path+'phone-map-1.png',
	path+'help_box.png',
	path+'question_box.png',
	path+'y1.png',
	path+'y2.png',
	path+'dust.png',
	];
	
	
	//loading sounds
	for (i=0; i< 52; i++ ) {
		filesArray[i] = sndArray[i];
		//console.log(i+ ' loading '+filesArray[i]);
		sounds[i]= loadSound (filesArray[i]) ;
		}
	//loading imgs
	for (i=52; i<96; i++){
		filesArray[i] = imgArray[i-52];
		//console.log(i+ ' loading '+filesArray[i]);
		loadImg (filesArray[i]);
		}
		

	}

	
function loadImg(which){
	var img = new Image();
	img.src = which;
	img.onload= function (){ 
	rm4Array(which);
	}
	}

function loadSound(which){
//snd = new Audio();
	sounds[which]=new Audio();
		rm4Array(which);
		return snd;
	}
	
function rm4Array(what){
	for (i=0; i<filesArray.length;i++){
		if(filesArray[i]==what){
			filesArray.splice(i,1);
			//console.log('removing '+what+' left with '+filesArray.length);
			document.getElementById('loadingPercent').innerHTML=Math.floor((96-filesArray.length)*100/96);
			}
				if (filesArray.length<53) {
					init ();
					//alert('loaded');
					
					}
		}

	}


///////////////////////
var inited=false;
function init() {
	
	if(inited) return;
	
	inited=true;
		getQs();
		getPango();
		
			
			document.getElementById('scene-1').style.display='none';
			var sc=localStorage.getItem('lastScene');
			gotoScene(sc);
			
	}
	
	
///////////////////////////
function sendResults (vig,qno,attempt,matrix) {
	var user = localStorage.getItem('key');
	//alert(user);
	//console.log(qno);
	$.post("../resource/2.php", { v: vig, q: qno, a: attempt, m: matrix, u: user } );
	}

function markComplete(){
	$.post("../resource/3.php", { v: 2, u: localStorage.getItem('key') } );
	}
function revert() {
	window.location='../index.php?userId='+localStorage.getItem('key');
	}
	
function getPango() {
	$.get('../resource/5.php',{ v: 2, u: localStorage.getItem('key') },  function(data) {
		var results = JSON.parse(data);
		var str='';
		
		for (i = 0; i < results.Feeds.length; i++) {

                    f = results.Feeds[i];
					
					str=str+'<br><a class="glow" target="_blank" href="http://www.pango.edu.au/DiscussionDetails/?id=' + f.CategoryId + '">'+
					f.TopicTitle +"</a><br>";
		}
		localStorage.setItem('v2-pangoData',str);
	});
	}

//////////////////////////
function playSound(which) {
if(!soundsMuted) {snd.volume=1;}
	snd.src=sndArray [which];
	snd.load();
	snd.play();
	}
	
function pauseSound(which){
	snd.pause();
	}
	
var soundsMuted=false;
function muteSounds() {
	
	if (!soundsMuted){
	for (i=0; i<sounds.length; i++) {
		snd.volume=0;
		}
		soundsMuted=true;
	}
	else {
		for (i=0; i<sounds.length; i++) {
		snd.volume=1;
		}
		
		soundsMuted=false;
		}
}

function decreaseBGSound() {
	//sounds[1].volume=0.2;
	}
	
	
function lowerBgSound(){
			snd.volume=0.2;
	}
function increaseBgSound(){
		snd.volume=0.4;
	}
	
function saveGame() {
	
	}
/////////////////////
//////////////////////
//////Helper Functions

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

///////////////////
function getTransformProperty(element) {
    // Note that in some versions of IE9 it is critical that
    // msTransform appear in this list before MozTransform
    var properties = [
        'transform',
        'WebkitTransform',
        'msTransform',
        'MozTransform',
        'OTransform'
    ];
    var p;
    while (p = properties.shift()) {
        if (typeof element.style[p] != 'undefined') {
            return p;
        }
    }
    return false;
}
//////////////////
function randomXToY(minVal,maxVal,floatVal)
{
  var randVal = minVal+(Math.random()*(maxVal-minVal));
  return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
}
/////////////////////////

