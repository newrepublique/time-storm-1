// JavaScript Document
function scene6(){
	
	showScene('scene-6');
	updatePangoTxt();
	window.parent.increaseBgSound();
	window.parent.pauseSound('0');
	
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ahista ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s6-continaer').style.display='block';
	setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;},2000);
	
	updateCompleted(80);
	
	setTimeout (function () {
		window.parent.playSound(2);
		}, 1000);
		
	setTimeout(function() {
		showDialogue('s6-dia-1');
		window.parent.playSound(46);
		},4000);
		
	setTimeout(function() {
		hideDialogue('s6-dia-1');
		},7000);
		
	setTimeout(function() {
		window.parent.playSound(30);
		showDialogue('s6-dia-2');
		},8000);
		
	setTimeout(function() {
		hideDialogue('s6-dia-2');
		},1200);
		
	setTimeout(function() {
		window.parent.showQ6();
		listen6_6();
		},12500);
		
	
	}

///////////////////////////
function listen6_6() {
	
	if (localStorage.getItem('v2-q6') == '1') {
		window.parent.hideQ6();
		updateCompleted(90);
		
		setTimeout(function() {
			document.getElementById('s6-continaer').style.opacity=0;
		//setTimeout(function() {window.parent.gotoScene('scene-6.html');} , 1000);
		
		setTimeout(function(){document.getElementById('s6-continaer').style.backgroundImage='url(imgs/bg9.jpg)';},1000);
		setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;},3000);
		
		//setTimeout(function(){showDialogue('s6-dia-3');},5000);
		
		//setTimeout(function(){hideDialogue('s6-dia-3');},8000);
		
		setTimeout(function(){document.getElementById('s6-continaer').style.opacity=0;},7000);
		setTimeout(function(){document.getElementById('s6-continaer').style.backgroundImage='url(imgs/bg10.jpg)';},8000);
		setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;},9000);
		
		setTimeout(function(){showDialogue('s6-dia-4'); document.getElementById('phone').style.backgroundImage='url(imgs/phone-map-1.png)';},11000);
		setTimeout(function(){hideDialogue('s6-dia-4');updatePhoneTxt(3,'You have arrived at the outskirts of the city.');updateCompleted(100);},14500);
		
		setTimeout(function(){showDialogue('s6-dia-5'); window.parent.playSound(31);},16000);
		setTimeout(function(){hideDialogue('s6-dia-5'); },22500);
		
		setTimeout(function(){showDialogue('s6-dia-6'); window.parent.playSound(47);},24000);
		setTimeout(function(){hideDialogue('s6-dia-6'); },27500);
		
		setTimeout(function(){document.getElementById('s6-continaer').style.opacity=0; },28500);
		
		
		setTimeout(function(){document.getElementById('s6-continaer').style.backgroundImage='url(imgs/bg11.jpg)';},30000);
		setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;},31000);
		
		setTimeout(function(){window.parent.gotoScene('countdown.html'); },34000);
		
			},2000);
	}
	
	else{
		setTimeout(listen6_6,200);
		}
	}