// JavaScript Document
var hasFixedRadio=false;
var turn=0;

function scene5() {
	showScene('scene-5');
	updatePangoTxt();
	window.parent.lowerBgSound();
	window.parent.pauseSound(2);
	window.parent.playSound('0');
	

	
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ahista ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s5-continaer').style.display='block';
	document.getElementById('s5-continaer').style.opacity=1;

	if (localStorage.getItem('v2-q3')=='1' && localStorage.getItem('v2-q4')=='1') {
		//window.parent.pauseSound('0');
		document.getElementById('s5-continaer').style.opacity=0;
		setTimeout(function(){document.getElementById('s5-continaer').style.backgroundImage='url(imgs/bg3.jpg)';},1000);
		setTimeout(function () {
			document.getElementById('s5-continaer').style.opacity=1;
			updateCompleted('');
		window.parent.showQ5();
		} , 2000);
		
			listen5_5();
			return;
		}
		
	if (localStorage.getItem('v2-q3')=='1' && localStorage.getItem('v2-q4')=='0') {
		document.getElementById('s5-continaer').style.opacity=0;
		setTimeout(function(){document.getElementById('s5-continaer').style.backgroundImage='url(imgs/bg3.jpg)';},1000);
		setTimeout(function () {
			document.getElementById('s5-continaer').style.opacity=1;
		window.parent.pauseSound('0');
		updateCompleted('');
		showQ4();
		},2000);
		return;
		}
		
		
	updatePhoneTxt(1,'You are inside the de-sal plant.');
		
		/*document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');*/
		
		updateCompleted(40);
	
	var iDelay=4000;
	
	
	setTimeout (function () {
		document.getElementById('radio').style.display='block';
		document.getElementById('book').style.display='block';
		document.getElementById('door').style.display='block';
		document.getElementById('transmitter').style.display='block';
		} , 1000+iDelay);	
}

function listen5_1(){
	if (localStorage.getItem('v2-q3') == '1') {
		
		//window.parent.showQ4();
		afterPlantFix();
		}
		
	else{
		
		setTimeout(listen5_1,200);
		}
	
	}
	
function showDoor() {
	window.parent.playSound(48);
	
	if (!hasFixedRadio && turn==0) {
		showDialogue('s5-dia-5');
		window.parent.playSound(22);
		turn++;
		setTimeout (function () {hideDialogue('s5-dia-5');} , 6000);
		return;
		}
	
	if (!hasFixedRadio && turn==1) {
		showDialogue('s5-dia-6');
		window.parent.playSound(23);
		setTimeout (function () {hideDialogue('s5-dia-6');} , 4000);
		return;
		}	
		
	document.getElementById('door').style.display='none';
	document.getElementById('black').style.display='block';
	document.getElementById('black').style.opacity=1;
	
	setTimeout(function () {document.getElementById('door-obj').style.display='block';} , 1000);
	
	setTimeout(function(){
		showDialogue('s5-dia-11');
		window.parent.playSound(26);
		},2000);	
		
	setTimeout(function(){
		hideDialogue('s5-dia-11');
		},6000);
		
	setTimeout(function(){
		window.parent.showQ3();
		listen5_1();
		},7000);
	}
	

function showBook () {
	window.parent.playSound(48);
	document.getElementById('black').style.display='block';
	document.getElementById('black').style.opacity=1;
	setTimeout(function () {document.getElementById('book-obj').style.display='block';} , 1000);
	
	setTimeout(function(){
		showDialogue('s5-dia-3');
		window.parent.playSound(20);
		},2000);
		
	setTimeout(function(){
		hideDialogue('s5-dia-3');
		},12000);
	
	setTimeout( function(){
		window.parent.playSound(50);
	document.getElementById('book-obj').style.backgroundImage='url(imgs/obj-1.png)';
	document.getElementById('book-close').style.display='block';
	},13000);
	}

function showRadio () {
	window.parent.playSound(48);
	document.getElementById('black').style.display='block';
	document.getElementById('black').style.opacity=1;
	setTimeout(function () {document.getElementById('radio-obj').style.display='block';} , 1000);
	
	setTimeout(function(){
		//showDialogue('s5-dia-3');
		window.parent.playSound(5);
		},2000);
		
	setTimeout(function(){
		showDialogue('s5-dia-2');
		window.parent.playSound(43);
		},19000);
		
	setTimeout(function(){
		hideDialogue('s5-dia-2');
		},21000);
		
	setTimeout(function(){
		showDialogue('s5-dia-4');
		window.parent.playSound(21);
		},22000);
		
		//iDelay=iDelay+500;
	setTimeout(function(){
		hideDialogue('s5-dia-4');
		},32000);
		
		setTimeout(function(){
			window.parent.playSound(50);
		document.getElementById('radio-obj').style.backgroundImage='url(imgs/obj-4.png)';
	document.getElementById('radio-close').style.display='block';
		},33000);
	
	}
	
function showTrans() {
	window.parent.pauseSound('0');
	window.parent.playSound(48);
	document.getElementById('black').style.display='block';
	document.getElementById('black').style.opacity=1;
	setTimeout(function () {document.getElementById('transmitter-obj').style.display='block';} , 1000);
	
	setTimeout(function() {
		showDialogue('s5-dia-12');
		window.parent.playSound(6);
		},2000);
		
	setTimeout(function() {
		hideDialogue('s5-dia-12');
		},13000);
		
	setTimeout(function() {
		showDialogue('s5-dia-7');
		window.parent.playSound(7);
		},14000);
		
	setTimeout(function() {
		hideDialogue('s5-dia-7');
		},18000);
		
	setTimeout(function() {
		showDialogue('s5-dia-8');
		window.parent.playSound(24);
		},19000);
		
	setTimeout(function() {
		hideDialogue('s5-dia-8');
		updatePhoneTxt(1,'You have fixed de-sal comms.');
		},24500);
		
	setTimeout(function() {
		hideDialogue('s5-dia-13');
		window.parent.playSound(50);
		document.getElementById('transmitter-obj').style.backgroundImage='url(imgs/obj-2.png)';
	document.getElementById('transmitter-close').style.display='block';
		},26000);
	}
	

function transFixed() {
	hasFixedRadio=true;
	document.getElementById('transmitter-obj').style.display='none'; 
	document.getElementById('black').style.display='none'; 
	document.getElementById('transmitter').style.display='none'; 
	
	setTimeout(function() {showDialogue('s5-dia-9');
	window.parent.playSound(44); } ,1000);
	
	setTimeout(function(){hideDialogue('s5-dia-9');},4800);
	
	setTimeout(function(){showDialogue('s5-dia-10'); window.parent.playSound(25);},6000);
	setTimeout(function(){hideDialogue('s5-dia-10');},10000);
	}
	
function afterPlantFix() {
	
	setTimeout(function() { 
	document.getElementById('door-obj').style.display='none';
	document.getElementById('black').style.display='none'; 
	window.parent.hideQ3();
	document.getElementById('radio').style.display='none';
		document.getElementById('book').style.display='none';
		document.getElementById('door').style.display='none';
		document.getElementById('transmitter').style.display='none';
	},1000);
	
	setTimeout(function () {
		showDialogue('s5-dia-14');
		window.parent.playSound(9);
		updateCompleted(60);
		} , 2000);
		
	setTimeout(function () {
		hideDialogue('s5-dia-14');
		updatePhoneTxt(1,'You\'ve fixed the de-sal plant.');
		} , 14000);
		
		setTimeout(function () {
		showDialogue('s5-dia-15');
		} , 15000);
		
		setTimeout(function () {
		hideDialogue('s5-dia-15');
		} , 21000);
	
	setTimeout(function () {
		document.getElementById('s5-continaer').style.opacity=0;
		setTimeout(function(){document.getElementById('s5-continaer').style.backgroundImage='url(imgs/bg7.jpg)';},1000);
		} , 22000);
		
	setTimeout(function () {
		document.getElementById('s5-continaer').style.opacity=1;
		} , 24000);
	
	setTimeout(function () {
		showDialogue('s5-dia-16');
		window.parent.playSound(45);
		} , 26000);
		
	setTimeout(function () {
		hideDialogue('s5-dia-16');
		} , 32000);
		
	setTimeout(function () {
		showDialogue('s5-dia-17');
		window.parent.playSound(29);
		} , 33000);
		
	setTimeout(function () {
		hideDialogue('s5-dia-17');
		} , 37000);
		
	setTimeout(function () {
		
		showQ4();
		
		
		} , 38000);
	}
	
function showQ4() {
	
	document.getElementById('s5-continaer').style.opacity=0;
		//setTimeout(function() {window.parent.gotoScene('scene-6.html');} , 1000);
		
		setTimeout(function(){document.getElementById('s5-continaer').style.backgroundImage='url(imgs/bg5.jpg)';},1000);
		
		setTimeout(function () {
			window.parent.playSound('0'); 
			document.getElementById('s5-continaer').style.opacity=1;
		} , 2000);
		
		setTimeout(function () {
			document.getElementById('greenDot').style.display='block';
			document.getElementById('greenDot').setAttribute('class','stretchBG');
			updatePhoneTxt(1,'You are about to move towards the city.');
			}, 5000);
			
		setTimeout(function () {
			window.parent.showQ4();
			listen5_4();
			}, 7000);
	
	}
	
function listen5_4(){
	if (localStorage.getItem('v2-q4') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ4();
		updateCompleted(60);
		setTimeout(function() {
			window.parent.showQ5();
			listen5_5();
			}, 2000);
		}
		
	else{
		
		setTimeout(listen5_4,200);
		}
	}
	
function listen5_5(){
	if (localStorage.getItem('v2-q5') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ5();
		updateCompleted(70);
		setTimeout(function() {
			document.getElementById('s5-continaer').style.opacity=0;
		}, 2000);
		setTimeout(function() {
			window.parent.gotoScene('scene-6.html');
		}, 4000);
		}
		
	else{
		
		setTimeout(listen5_5,200);
		}
	}