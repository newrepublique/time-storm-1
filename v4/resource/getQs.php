<?php 

$v=$_GET['v'];

switch ($v) {
	
	case "2":
$str= <<<EOF
{
	"q1": 
	{ 	"title": "Get Moving Challenge - one of two",
		"qTxt": "What is the force that powers a land yacht?",
		"ans" : ["Powered by a combustion engine","A solar powered engine","Powered by the wind","Powered by nuclear power"],
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 3
		},
		
	"q2": 
	{ 	"title": "Get Moving Challenge - two of two",
		"qTxt": "When an object accelerates using the power of another force it is subject too?",
		"ans" : ["Newton's First Law","Newton's Second Law","Newton's Third Law","Newtown's Third Law"],
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 2
		},
		
	"q3": 
	{ 	"title": "De-sal Plant challenge",
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		},
		
	"q4": 
	{ 	"title": "Travel Time Challenge - one of two",
		"qTxt": "Which of the formula below would you use to find out the amount of time taken to travel somewhere?",
		"ans" : ["Time = Speed/Distance","Time = Speed x Distance","Time = Distance x Speed","Time = Distance/Speed"],
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 4
		},

	"q5": 
	{ 	"title": "Travel Time Challenge - two of two",
		"qTxt": "If the right formula is Time = Distance/Speed, and we know that the De-Sal plant in 65km a way and that the land yacht travels 40km an hour  how long would it take approximately to the nearest rounded up tens of minutes?",
		"ans" : ["It would take approximately 1 hr 40 mim","It would take approximately 2 hrs","It would take approximately 1 hr 30 min","It would tale approximately 1 hr 50 min"],
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 1
		},

		
	"q6": 
	{ 	"title": "Leaving the plant",
		"qTxt": "Now that you've re-started the desalination plant you'll need to figure out how far it is to the city, the captain mentioned it will take 30 mins travelling at 40 km an hour. How far away is the city?",
		"ans" : ["16km away","18km away","20km away","22km away"],
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 3
		},	
	}
EOF;

		break;
		
		
		case "3": 
$str= <<<EOF
		
	"q1": 
	{ 	"title": "Carbon Cycle Challenge",
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		},

	"q2": 
	{ 	"title": "Coal Power Plant Challenge",
		"helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ",
		},
	
	"q3": 
	{ 	"title": "Energy Output Challenge",
		"helpTxt" : "Help for Energy Output Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset" : 3,
		},
		
	"q4": 
	{ 	"title": "Alternative Energy Challenge - one of two",
		"qTxt": "Which of the following best describe what geothermal energy is?",
		"ans" : ["Geothermal energy is energy obtained from the heat of the sun","Geothermal energy is energy obtained from the heat released by the burning of coal","Geothermal  energy  is  energy  obtained  from  the  heat  within  the  Earth's  crust","Geothermal energy is energy obtained by heating the Earth's crust"],
		"helpTxt" : "Help for Alt Energy Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 3
		},
		
	"q5": 
	{ 	"title": "Alternative Energy Challenge - two of two",
		"qTxt": "How is solar energy normally captured?",
		"ans" : ["Using photovoltaic cells","Using membrane cells","Using photosensitive cells","Using wind turbines"],
		"helpTxt" : "Help for Alt Energy zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 1
		},
	"q6": 
	{ 	"title": "Biosphere Challenge - one of two",
		"qTxt": "Which of the following power sources are NOT considered to be green power?",
		"ans" : ["Biomass","Wave energy","Hydro-electricity","Nuclear power"],
		"helpTxt" : "Help for Biosphere zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 4
		},
	"q7": 
	{ 	"title": "Biosphere Challenge - two of two",
		"qTxt": "Extracting and using natural resources found in the  Earth can also  have detriment effects on the atmosphere and biosphere. What is an essential property of a natural resource?",
		"ans" : ["It has a low cost and is readily available","It has a useful property that other resources don't have","It is easy to take out of the Earth","They are not readily available"],
		"helpTxt" : "Help for Biosphere zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ",
		"offset": 2
		},
	
		
EOF;
		break;
}

echo $str;
?>