// JavaScript Document

var viewportwidth;
var viewportheight;
if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
/////////////////////////////////////
function setScreen() {
	if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
	
	var newHt= Math.round((719/1143)*viewportwidth);
	var newTop = (viewportheight - newHt)/2;
	//console.log('new Ht is ' +newHt);
	
	
	if (newHt > (viewportheight+5)) {
		searchForRight(2);
		}
		else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',viewportwidth);
	$('.scene').css('left','0px');
			}
	}
//////////////////////////////////////
function searchForRight(percent) {
		if (typeof window.innerWidth != 'undefined')
 	{
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 	}
	var newWd = viewportwidth-(viewportwidth*percent/100);
	var newHt= Math.round((719/1143)*newWd);
	var newTop = (viewportheight - newHt)/2;
	var newLeft = (viewportwidth - newWd)/2;
	
	if (newHt>viewportheight){
		searchForRight(percent+2)
		}
	else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',newWd);
	$('.scene').css('left',newLeft);
		}
	
	}
/////////////////////////////////////
function gotoScene(which){
	document.getElementById('kala').style.display='block';
	document.getElementById('iframe').style.display='block';
	document.getElementById('iframe').src=which+"?seed="+Math.random();
	}
function hideKala(){
	document.getElementById('kala').style.display='none';
	}
/////////////////////////////////////

/////////////////////////////////////
var checked=true;
function chkPrevious() {

if (localStorage.getItem('lastScene')==null){
	mkLS();
	}
setScreen();
document.getElementById('scene-0').style.display='block';
//showScene ('scene-1');
	}
/////////////////////////
function mkLS() {
	localStorage.setItem('v4-q1','0');
	localStorage.setItem('v4-q2','0');
	localStorage.setItem('v4-q3','0');
	localStorage.setItem('v4-q4','0');
	localStorage.setItem('v4-q5','0');
	localStorage.setItem('v4-q6','0');
	localStorage.setItem('v4-q7','0');
	localStorage.setItem('v4-q8','0');
	localStorage.setItem('v4-q9','0');
	localStorage.setItem('v4-q10','0');
	localStorage.setItem('v4-pangoData','');
	localStorage.setItem('v4-phoneData','');
	localStorage.setItem('secs','900');
	localStorage.setItem('mins','15');
	localStorage.setItem('lastScene','scene-2.html');
	localStorage.setItem('nick','Stewie Griffin');
	localStorage.setItem('v4-hasCollectedSoil','0');
	localStorage.setItem('v4-countdown','off');
	localStorage.setItem('v4-completed','000');

	}
function destroyLS(){
	localStorage.removeItem('lastScene');
	}
/////////////////////////
function storePhoneData(what) {
	var currentData=localStorage.getItem('v4-phoneData');
	currentData=currentData+what;
	localStorage.setItem('v4-phoneData',currentData);
	return currentData;
	}
////////////////////////
function storePangoData(what) {
	var currentData=localStorage.getItem('v4-pangoData');
	currentData=currentData+what;
	localStorage.setItem('v4-pangoData',currentData);
	return currentData;
	}
function storeTimer(_sec,_min){
	localStorage.setItem('secs',_sec.toString());
	localStorage.setItem('mins',_min.toString());
	}



//////////////////////////
var filesArray = Array (101);
var sounds = Array (38);
var snd = new Audio;
var sndArray = Array(38);	

function preload() {
	
	gender=localStorage.getItem('gender');
	if (!checked) return;
	
	document.getElementById('loading').style.display='block';
	
	var path= 'snd/';
	var ext='.ogg';
	if(	BrowserDetect.browser == 'Safari') ext='.mp3';
	
	var imgArray= Array(63);
	 
	//filling in the sndArray
	sndArray = [
	path+'sfx/01_desert_wind_dogs_loopable'+ext, //0
	path+'sfx/02_girl_from_ipanema_loopable'+ext, //1
	path+'sfx/03_dome_interior_sound_loopable'+ext, //2
	path+'sfx/04_siren'+ext, //3
	path+'sfx/05_lab_atmos_loopable'+ext, //4
	path+'sfx/06_rainforest_loopable'+ext, //5
	path+'sfx/07_mediterranean_loopable'+ext,//6
	path+'sfx/08_grass_plains_loopable'+ext,//7
	path+'sfx/09_cold_temperate_loopable'+ext,//8
	path+'sfx/10_euro_forest'+ext,//9
	path+'sfx/11_warm_temperate'+ext,//10
	path+'sfx/12_siren'+ext,//11
	path+'sfx/pa_voice_1'+ext,//12
	path+'sfx/pa_voice_2'+ext,//13
	path+'sfx/pa_voice_3'+ext,//14
	path+'sfx/pa_voice_4'+ext,//15
	path+'sfx/pa_voice_5'+ext,//16
	path+'char/max_26_not_bad_'+ext,//17
	path+'char/max_27_i_know_'+ext,//18
	path+'char/max_28_its_nothing_'+ext,//19
	path+'char/max_29_dont_worry_'+ext,//20
	path+'char/max_30_great_'+ext,//21
	path+'char/max_31_see'+ext,//22
	path+'char/max_32_how_did_you_go_'+ext,//23
	path+'char/max_33_how_'+ext,//24
	path+'char/max_34_fantastic_'+ext,//25
	path+'char/max_35_just_in_time_'+ext,//26
	path+gender+'/'+gender+'_user_45'+ext,//27
	path+gender+'/'+gender+'_user_46'+ext,//28
	path+gender+'/'+gender+'_user_47'+ext,//29
	path+gender+'/'+gender+'_user_48'+ext,//30
	path+gender+'/'+gender+'_user_49'+ext,//31
	path+gender+'/'+gender+'_user_50'+ext,//32
	path+gender+'/'+gender+'_user_51'+ext,//33
	path+'game/016_peep_01'+ext,//34
	path+'game/017_peep_02'+ext,//35
	path+'game/018_tool_tip_01'+ext,//36
	path+'game/019_map_green_dot_beep_01'+ext,//37
	];
	//filling in images
	path='imgs/';
	imgArray = [
	path+'bg1.jpg',
	path+'bg2.jpg',
	path+'bg3.jpg',
	path+'bg4.jpg',
	path+'bg5.jpg',
	path+'bg6.jpg',
	path+'bg7.jpg',
	path+'bg8.jpg',
	path+'thing-1.png',
	path+'thing-2.png',
	path+'thing-3.png',
	path+'thing-4.png',
	path+'fracture.png',
	path+'fracture1.png',
	path+'frame.png',
	path+'obj-1.png',
	path+'obj-2.png',
	path+'obj-3.png',
	path+'obj-4.png',
	path+'obj-5.png',
	path+'obj-6.png',
	path+'obj-7.png',
	path+'obj-8.png',
	path+'obj-9.png',
	path+'obj-10.png',
	path+'obj-11.png',
	path+'pangoUpdates.png',
	path+'phone.png',
	path+'phone-2.png',
	path+'phone-3.png',
	path+'phone-4.png',
	path+'phone-5.png',
	path+'phone-6.png',
	path+'phone-7.png',
	path+'phone-8.png',
	path+'phone-9.png',
	path+'gola.png',
	path+'q5/q5.png',
	path+'q5/ans1.png',
	path+'q5/ans2.png',
	path+'q5/ans3.png',
	path+'q5/ans4.png',
	path+'q5/ans5.png',
	path+'q5/ans6.png',
	path+'q5/ans7.png',
	path+'q5/ans8.png',
	path+'q5/ans9.png',
	path+'q6/q6.png',
	path+'q6/ans1.png',
	path+'q6/ans2.png',
	path+'q6/ans3.png',
	path+'q6/ans4.png',
	path+'q6/ans5.png',
	path+'q6/ans6.png',
	path+'q6/ans7.png',
	path+'q6/ans8.png',
	path+'help_box.png',
	path+'question_box.png',
	path+'save-1.png',
	path+'save-2.png',
	path+'mute-1.png',
	path+'mute-2.png',
	path+'ufo.png',
	];
	
	
	//loading sounds
	for (i=0; i< 38; i++ ) {
		filesArray[i] = sndArray[i];
		//console.log(i+ ' loading '+filesArray[i]);
		sounds[i]= loadSound (filesArray[i]) ;
		}
	//loading imgs
	for (i=38; i<101; i++){
		filesArray[i] = imgArray[i-38];
		//console.log(i+ ' loading '+filesArray[i]);
		loadImg (filesArray[i]);
		}
		
	}

	
function loadImg(which){
	var img = new Image();
	img.src = which;
	img.onload= function (){ 
	rm4Array(which);
	}
	}

function loadSound(which){
	var snd = new Audio();
	sounds[which]=new Audio();
		rm4Array(which);
		return snd;
	}
	
function rm4Array(what){
	for (i=0; i<filesArray.length;i++){
		if(filesArray[i]==what){
			filesArray.splice(i,1);
			//console.log('removing '+what+' left with '+filesArray.length);
			document.getElementById('loadingPercent').innerHTML=Math.floor((101-filesArray.length)*100/101);
			}
				if (filesArray.length<63) {
					init ();
					//alert('loaded');
					
					}
		}

	}

///////////////////////
var inited=false;

function init() {
	
	if(inited) return;
	
	inited=true;
		getQs();
		getPango();
		
					
			/*sounds[0].volume=0.4;
			sounds[1].volume=0.1;
			sounds[2].volume=0.4;
			sounds[4].volume=0.4;
			sounds[11].volume=0.6;
			sounds[35].volume=0.6;*/
			
			document.getElementById('scene-1').style.display='none';
			var sc=localStorage.getItem('lastScene');
			gotoScene(sc);
			
	}
	
///////////////////////////
function sendResults (vig,qno,attempt,matrix) {
	var user = localStorage.getItem('key');
	//alert(user);
	//console.log(qno);
	$.post("../resource/2.php", { v: vig, q: qno, a: attempt, m: matrix, u: user } );
	}

function markComplete(){
	$.post("../resource/3.php", { v: 4, u: localStorage.getItem('key') } );
	}
function revert() {
	window.location='../index.php?userId='+localStorage.getItem('key');
	}
	
function getPango() {
	$.get('../resource/5.php',{ v: 4, u: localStorage.getItem('key') },  function(data) {
		var results = JSON.parse(data);
		var str='';
		
		for (i = 0; i < results.Feeds.length; i++) {

                    f = results.Feeds[i];
					
					str=str+'<br><a class="glow" target="_blank" href="http://www.pango.edu.au/DiscussionDetails/?id=' + f.CategoryId + '">'+
					f.TopicTitle +"</a><br>";
		}
		localStorage.setItem('v4-pangoData',str);
	});
	}

	
//////////////////////////
function playSound(which) {
if(!soundsMuted) {snd.volume=1;}
	snd.src=sndArray [which];
	snd.load();
	snd.play();
	//alert(sndArray [which]);
	
	
	}
	
function pauseSound(which){
	snd.pause();
	}
function pauseSounds(which){
	snd.pause();
	}
	
var soundsMuted=false;
function muteSounds() {
	
	if (!soundsMuted){
	
		snd.volume=0;
		//alert('mute attempted');
		
		soundsMuted=true;
	}
	else {
		
		snd.volume=1;
		
		
		soundsMuted=false;
		}
}

function tempSndPlay(){
	var ext='.mp3';
	var path='snd/';
	snd= new Audio;
	snd.src=path+'sfx/05_lab_atmos_loopable'+ext;
	snd.play();
	setTimeout(function() {snd.pause();}, 100);
	snd.volume=0.01;
	}
function decreaseBGSound() {
	//sounds[1].volume=0.2;
	}
	
	
function lowerBgSound(){
			snd.volume=0.2;
	}
function increaseBgSound(){
		snd.volume=0.4;
	}
	
function saveGame() {
	
	}/////////////////////
//////////////////////
//////Helper Functions

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

///////////////////
function getTransformProperty(element) {
    // Note that in some versions of IE9 it is critical that
    // msTransform appear in this list before MozTransform
    var properties = [
        'transform',
        'WebkitTransform',
        'msTransform',
        'MozTransform',
        'OTransform'
    ];
    var p;
    while (p = properties.shift()) {
        if (typeof element.style[p] != 'undefined') {
            return p;
        }
    }
    return false;
}
//////////////////
function randomXToY(minVal,maxVal,floatVal)
{
  var randVal = minVal+(Math.random()*(maxVal-minVal));
  return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
}
/////////////////////////

