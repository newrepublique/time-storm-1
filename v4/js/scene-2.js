// JavaScript Document
var samplesCollected=0;

function scene2() {
	var iDelay=1000;
	
	showScene('scene-2');
	
	window.parent.playSound('0');
		
	updatePangoTxt();
	
	
	////////////////actual function begins now
	
	setTimeout (function () {	
		document.getElementById('s2-continaer').style.opacity=1;
		updatePhoneTxt(1,'You have started playing Timestorm - Chapter 4');
		}, iDelay);
		
	
	setTimeout(function() {
		showDialogue('s2-dia-1');
		} , 2000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-1');
		} , 8000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound('0');
		} , 9000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg2.jpg)'; window.parent.playSound(1);
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; }, 1000);
		} , 10000+iDelay);
		
	setTimeout (function() {
		document.getElementById('s2-continaer').style.opacity=0;
		}, 15000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg3.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-2.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; window.parent.pauseSound(1);}, 1000);
		} , 20000+iDelay);
		
	setTimeout(function() {
		window.parent.playSound(2);
		showDialogue('s2-dia-2');
		setTimeout(function() {window.parent.playSound(27);}, 500);
		} , 21000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-2');
		} , 26000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-3');
		setTimeout(function() {window.parent.playSound(17);}, 500);
		} , 27000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-3');
		} , 39000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-4');
		} , 40000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-4');
		} , 43000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-5');
		setTimeout(function() {window.parent.playSound(28);}, 500);
		} , 44000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-5');
		} , 48000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-6');
		setTimeout(function() {window.parent.playSound(18);}, 500);
		} , 49000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-6');
		} , 59000+iDelay);
		
	setTimeout(function() {
		window.parent.playSound(3);
		} , 60000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-7');
		setTimeout(function() {window.parent.playSound(29);}, 500);
		} , 66000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-7');
		} , 70000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-8');
		setTimeout(function() {window.parent.playSound(19);}, 500);
		} , 71000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-8');
		} , 77000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound(1);
		} , 78000+iDelay);
		
	setTimeout(function() {
		gotoLab();
		} , 80000+iDelay);
		
}

///////////////////////////
var labsVisited=0;
function gotoLab() {
	
	var iDelay=1000;
	
	document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg4.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-3.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1;
		updatePhoneTxt(3,'You are in the lab.');
		window.parent.playSound(4);
		 }, 1000);
	
		setTimeout(function() {
		showDialogue('s2-dia-9');
		} , 3000+iDelay);
		
		setTimeout(function() {
		hideDialogue('s2-dia-9');
		} , 6000+iDelay);
		
		setTimeout(function() {
		showDialogue('s2-dia-10');
		} , 7000+iDelay);
		
		setTimeout(function() {
		hideDialogue('s2-dia-10');
		} , 11000+iDelay);
		
		setTimeout(function() {
		document.getElementById('lab-1').style.display='block';
		document.getElementById('lab-2').style.display='block';
		document.getElementById('lab-3').style.display='block';
		document.getElementById('lab-4').style.display='block';
		} , 11500+iDelay);
	}

/////////////////////////
function show2overlay(which) {
	window.parent.pauseSounds();
	document.getElementById('blacky').style.opacity=0;
	
	setTimeout(function () {
		$('.obj').css('display','none');
		}, 1000);
		
	setTimeout(function () {
		showObj(which);
		}, 1500);
		
	switch (which) {
		case 'rain-obj':
		
		updatePhoneTxt(3,'You are in the rain forest.');
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
		setTimeout(function() {showDialogue('s2-dia-17');window.parent.playSound(5);} , 2000);
		setTimeout(function() {hideDialogue('s2-dia-17');} , 6000);
		break;
		
		case 'scrub-obj':
		
		updatePhoneTxt(3,'You are back in Meditranian Scrub.');
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-5.png)';
		setTimeout(function() {showDialogue('s2-dia-21');window.parent.playSound(6);} , 2000);
		setTimeout(function() {hideDialogue('s2-dia-21');} , 6000);
		break;
		
		case 'grass-obj':
		
		updatePhoneTxt(3,'You are back in the grass lands.');
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-8.png)';
		setTimeout(function() {showDialogue('s2-dia-25');window.parent.playSound(7);} , 2000);
		setTimeout(function() {hideDialogue('s2-dia-25');} , 6000);
		break;
		
		case 'cold-obj':
		
		updatePhoneTxt(3,'You are back in the cold temperature environment.');
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-6.png)';
		setTimeout(function() {showDialogue('s2-dia-29');window.parent.playSound(8);} , 2000);
		setTimeout(function() {hideDialogue('s2-dia-29');} , 6000);
		break;
		
		case 'forest-obj':
		
		updatePhoneTxt(3,'You are back in the forest.');
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-7.png)';
		setTimeout(function() {showDialogue('s2-dia-33');window.parent.playSound(9);} , 2000);
		setTimeout(function() {hideDialogue('s2-dia-33');} , 6000);
		break;
		
		case 'warm-obj':
		
		updatePhoneTxt(3,'You are back in the warm temperature environment.');
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-9.png)';
		setTimeout(function() {showDialogue('s2-dia-37');window.parent.playSound(10);} , 2000);
		setTimeout(function() {hideDialogue('s2-dia-37');} , 6000);
		break;
		}
	
	}

	
///////////////////////////////
function showObj(which){
	window.parent.playSound(36);
	document.getElementById('blacky').style.display='block';
	document.getElementById('blacky').style.opacity=1;
	
	setTimeout(function () {
		document.getElementById(which).style.display='block';
		}, 1000);
	
	}
	
function hideObj(which){
	window.parent.playSound(35);
	document.getElementById(which).style.display='none';

	document.getElementById('blacky').style.opacity=0;
	
	setTimeout(function () {
			document.getElementById('blacky').style.display='none';
		}, 1000);
	
	
	/* only for the tip/cars*/
	setTimeout (function () {
		if(labsVisited==4) {
		sirenStuff();
		  document.getElementById('lab-1').style.display='none';
		document.getElementById('lab-2').style.display='none';
		document.getElementById('lab-3').style.display='none';
		document.getElementById('lab-4').style.display='none';
		  };
		},3000);
	}
///////////////////////////////////////////

function sirenStuff() {
	
	var iDelay = 0;
	
	window.parent.playSound(3);
	
	document.getElementById('s2-continaer').style.opacity=0;
	
		setTimeout(function() { 
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg5.jpg)';
		document.getElementById('s2-continaer').style.opacity=1;
		}, 1000);
		
	setTimeout(function() { 
	document.getElementById('s2-continaer').style.opacity=0;
		}, 3000+iDelay);
		
	setTimeout(function() { 
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
		document.getElementById('s2-continaer').style.opacity=1;
		}, 4000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-11');
		setTimeout(function() {window.parent.playSound(20);}, 500);
		} , 6000+iDelay);
	
	setTimeout(function() {
		hideDialogue('s2-dia-11');
		} , 14500+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-12');
		setTimeout(function() {window.parent.playSound(30);}, 500);
		} , 16000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-12');
		} , 21000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-13');
		setTimeout(function() {window.parent.playSound(21);}, 500);
		} , 22000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-13');
		} , 39000+iDelay);
		
	setTimeout(function() { 
	document.getElementById('s2-continaer').style.opacity=0;
		}, 40000+iDelay);
		
	setTimeout(function() { 
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg4.jpg)';
		setTimeout(function(){document.getElementById('s2-continaer').style.opacity=1;},1000);
		}, 42000+iDelay);
		
	setTimeout(function(){
		showObj('map-obj');
		$('.gola').css ('display','none');
		
		setTimeout(function(){$('.gola').css ('display','block');
		document.getElementById('gola-1').setAttribute('class','gola stretchBG blink_continous_slow');
		document.getElementById('gola-2').setAttribute('class','gola stretchBG blink_continous_slow');
		document.getElementById('gola-3').setAttribute('class','gola stretchBG blink_continous_slow');
		document.getElementById('gola-4').setAttribute('class','gola stretchBG blink_continous_slow');
		document.getElementById('gola-5').setAttribute('class','gola stretchBG blink_continous_slow');
		document.getElementById('gola-6').setAttribute('class','gola stretchBG blink_continous_slow');
		}, 2000);
	},44000+iDelay);
	
	}
	
//////////////////////
var samples=0;

function revert2map() {
	samples++;
	
	window.parent.pauseSounds();
	document.getElementById('blacky').style.opacity=0;
	
	setTimeout(function () {
		$('.obj').css('display','none');
		}, 1000);
	
	setTimeout(function () {
		showObj('map-obj');
		}, 2000);
	
	
	if (samples==2) {
		document.getElementById('gola-1').style.display='none';
		document.getElementById('gola-2').style.display='none';
		document.getElementById('gola-3').style.display='none';
		document.getElementById('gola-4').style.display='none';
		document.getElementById('gola-5').style.display='none';
		document.getElementById('gola-6').style.display='none';
		
		setTimeout (function () {showDialogue('s2-dia-41');} , 4000);
		setTimeout (function () {hideDialogue('s2-dia-41');} , 9000);
		setTimeout (function () {document.getElementById('s2-continaer').style.opacity=0;} , 9500);
		setTimeout (function () {window.parent.gotoScene('scene-3.html');} , 10500);
		
		}
	}