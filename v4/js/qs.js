// JavaScript Document

//////////////////////////
////////////////////////
//Qs

var qs;
/*
var qs={
"q1": { "title": "Carbon Cycle Challenge", "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", }, "q2": { "title": "Coal Power Plant Challenge", "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", }, "q3": { "title": "Energy Output Challenge", "helpTxt" : "Help for Energy Output Whatever it is... it will go in here. Hopefully it will be small... ", "offset" : 3, }, "q4": { "title": "Alternative Energy Challenge - one of two", "qTxt": "Which of the following best describe what geothermal energy is?", "ans" : ["Geothermal energy is energy obtained from the heat of the sun","Geothermal energy is energy obtained from the heat released by the burning of coal","Geothermal energy is energy obtained from the heat within the Earth's crust","Geothermal energy is energy obtained by heating the Earth's crust"], "helpTxt" : "Help for Alt Energy Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 3 }, "q5": { "title": "Alternative Energy Challenge - two of two", "qTxt": "How is solar energy normally captured?", "ans" : ["Using photovoltaic cells","Using membrane cells","Using photosensitive cells","Using wind turbines"], "helpTxt" : "Help for Alt Energy zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 1 }, "q6": { "title": "Biosphere Challenge - one of two", "qTxt": "Which of the following power sources are NOT considered to be green power?", "ans" : ["Biomass","Wave energy","Hydro-electricity","Nuclear power"], "helpTxt" : "Help for Biosphere zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 4 }, "q7": { "title": "Biosphere Challenge - two of two", "qTxt": "Extracting and using natural resources found in the Earth can also have detriment effects on the atmosphere and biosphere. What is an essential property of a natural resource?", "ans" : ["It has a low cost and is readily available","It has a useful property that other resources don't have","It is easy to take out of the Earth","They are not readily available"], "helpTxt" : "Help for Biosphere zwei von zweiWhatever it is... it will go in here. Hopefully it will be small... ", "offset": 2 },  
};

*/

function getQs() {
   
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.open("GET", "../resource/6.php?v=4",true);
 xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState==4) {
   //qs=JSON.parse(xmlhttp.responseText);
   eval("qs = {" + xmlhttp.responseText + "}");
   
  }
 }
 xmlhttp.send(null);
	
	}
	
function showQ5() {
	
	document.getElementById('q5').style.display='block';
	document.getElementById('q5').style.opacity=1;
	setTimeout (function(){document.getElementById('q5-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-5');},1000);
	setTimeout (function(){hideDialogue('s1-dia-5'); document.getElementById('q5-splash').style.opacity=0;},8000);
	
		
	setTimeout (function(){
	mkDragables5();
	startRecording();
	listen_5();
	document.getElementById('q5-a').style.display='block';
	var which='q5';
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
		},9000);
	}
	
function showQ6() {
	
	document.getElementById('q6').style.display='block';
	document.getElementById('q6').style.opacity=1;
	setTimeout (function(){document.getElementById('q6-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-6');},1000);
	setTimeout (function(){hideDialogue('s1-dia-6'); document.getElementById('q6-splash').style.opacity=0;},8000);
	
		
	setTimeout (function(){
	mkDragables6();
	startRecording();
	listen_6();
	document.getElementById('q6-a').style.display='block';
	var which='q6';
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
		},9000);
	}
	

function showQ1(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q1').style.display='block';
	document.getElementById('q1').style.opacity=1;
	setTimeout (function(){document.getElementById('q1-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-1');},1000);
	setTimeout (function(){hideDialogue('s1-dia-1'); document.getElementById('q1-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q1');
		document.getElementById('q1-a').style.display='block';
		startRecording();
		},9000);
	
	}

function showQ2(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q2').style.display='block';
	document.getElementById('q2').style.opacity=1;
	setTimeout (function(){document.getElementById('q2-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-2');},1000);
	setTimeout (function(){hideDialogue('s1-dia-2'); document.getElementById('q2-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q2');
		document.getElementById('q2-a').style.display='block';
		startRecording();
		},9000);
	
	}

	
function showQ3(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q3').style.display='block';
	document.getElementById('q3').style.opacity=1;
	setTimeout (function(){document.getElementById('q3-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-3');},1000);
	setTimeout (function(){hideDialogue('s1-dia-3'); document.getElementById('q3-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q3');
		document.getElementById('q3-a').style.display='block';
		startRecording();
		},9000);
	
	}

	
function showQ4(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q4').style.display='block';
	document.getElementById('q4').style.opacity=1;
	setTimeout (function(){document.getElementById('q4-splash').style.opacity=1;},100);
	
	setTimeout (function(){ document.getElementById('q4-splash').style.opacity=0;},4000);
	
	setTimeout (function(){
		mkQs('q4');
		document.getElementById('q4-a').style.display='block';
		startRecording();
		},2000);
	
	}
	
	
function showQ7(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q7').style.display='block';
	document.getElementById('q7').style.opacity=1;
	setTimeout (function(){document.getElementById('q7-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-7');},1000);
	setTimeout (function(){hideDialogue('s1-dia-7'); document.getElementById('q7-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q7');
		document.getElementById('q7-a').style.display='block';
		startRecording();
		},9000);
	
	}
	
function showQ8(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q8').style.display='block';
	document.getElementById('q8').style.opacity=1;
	setTimeout (function(){document.getElementById('q8-splash').style.opacity=1;},100);
	
	setTimeout (function(){ document.getElementById('q8-splash').style.opacity=0;},4000);
	
	setTimeout (function(){
		mkQs('q8');
		document.getElementById('q8-a').style.display='block';
		startRecording();
		},2000);
	
	}
	
function showQ9(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q9').style.display='block';
	document.getElementById('q9').style.opacity=1;
	setTimeout (function(){document.getElementById('q9-splash').style.opacity=1;},100);
	
	setTimeout (function(){ document.getElementById('q9-splash').style.opacity=0;},4000);
	
	setTimeout (function(){
		mkQs('q9');
		document.getElementById('q9-a').style.display='block';
		startRecording();
		},2000);
	
	}
function showQ10(){
	//document.getElementById('q3-a').innerHTML='';
	document.getElementById('q10').style.display='block';
	document.getElementById('q10').style.opacity=1;
	setTimeout (function(){document.getElementById('q10-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-10');},1000);
	setTimeout (function(){hideDialogue('s1-dia-10'); document.getElementById('q10-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q10');
		document.getElementById('q10-a').style.display='block';
		startRecording();
		},9000);
	
	}
	
function hideQ2() {
	document.getElementById('q2-a').innerHTML='';
	document.getElementById('q2').style.opacity=0;
	setTimeout(function(){document.getElementById('q2').style.display='none';},2000);
	
	}
function hideQ1() {
	document.getElementById('q1-a').innerHTML='';
	document.getElementById('q1').style.opacity=0;
	setTimeout(function(){document.getElementById('q1').style.display='none';},2000);
	
	}
	
function hideQ3() {
	document.getElementById('q3-a').innerHTML='';
	document.getElementById('q3').style.opacity=0;
	setTimeout(function(){document.getElementById('q3').style.display='none';},2000);
	
	}
	
function hideQ4() {
	document.getElementById('q4-ans').innerHTML='';
	document.getElementById('q4').style.opacity=0;
	setTimeout(function(){document.getElementById('q4').style.display='none';},2000);
	
	}
	
function hideQ5() {
	document.getElementById('q5-a').innerHTML='';
	document.getElementById('q5').style.opacity=0;
	setTimeout(function(){document.getElementById('q5').style.display='none';},2000);
	
	}

function hideQ6() {
	document.getElementById('q6-a').innerHTML='';
	document.getElementById('q6').style.opacity=0;
	setTimeout(function(){document.getElementById('q6').style.display='none';},2000);
	
	}
function hideQ7() {
	document.getElementById('q7-ans').innerHTML='';
	document.getElementById('q7').style.opacity=0;
	setTimeout(function(){document.getElementById('q7').style.display='none';},2000);
	}
	
function hideQ8() {
	document.getElementById('q8-ans').innerHTML='';
	document.getElementById('q8').style.opacity=0;
	setTimeout(function(){document.getElementById('q8').style.display='none';},2000);
	}
	
function hideQ9() {
	document.getElementById('q9-ans').innerHTML='';
	document.getElementById('q9').style.opacity=0;
	setTimeout(function(){document.getElementById('q9').style.display='none';},2000);
	}
function hideQ10() {
	document.getElementById('q10-ans').innerHTML='';
	document.getElementById('q10').style.opacity=0;
	setTimeout(function(){document.getElementById('q10').style.display='none';},2000);
	}
	
function mkQs(which) {
	document.getElementById(which+'-title').innerHTML=qs[which].title;
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-question').innerHTML=qs[which].qTxt;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
	
	var a1=qs[which].ans[0];
	var a2=qs[which].ans[1];
	var a3=qs[which].ans[2];
	var a4=qs[which].ans[3];

	var rads=		  "<input type=\"radio\" name=\"ans\" id=\""+which+"_1\" value=\"ans1\" onclick=\"chkAns('"+which+"','1')\" />  <label for=\""+which+"_1\">"+a1+"</label><div id=\""+which+"_1X\" class='cross blink_continous'>  X  </div><br />";
        
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_2\"value=\"ans2\" onclick=\"chkAns('"+which+"','2')\" />  <label for=\""+which+"_2\">"+a2+"</label><div id=\""+which+"_2X\" class='cross blink_continous'>  X  </div><br />"; 
		
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_3\" value=\"ans3\" onclick=\"chkAns('"+which+"','3')\" />  <label for=\""+which+"_3\">"+a3+"</label><div id=\""+which+"_3X\" class='cross blink_continous'>  X  </div><br />";
		
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_4\" value=\"ans4\" onclick=\"chkAns('"+which+"','4')\" />  <label for=\""+which+"_4\">"+a4+"</label><div id=\""+which+"_4X\" class='cross blink_continous'>  X  </div><br />";       
	
		//console.log(rads);
	document.getElementById(which+'-ans').innerHTML= rads;
	
		$('input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	
	
	}
	
var iShouldWait=false;
function chkAns(qNo, ansNo) {
//	alert(qNo.toString()+":"+ansNo);

if (!iShouldWait){   //the onclick fires like stupids


if (qs[qNo].offset == ansNo) {
	
	playSound('34');
	//sounds[48].play();
	recLap();
	stopRecording();
	var temp =  qNo.replace(/^q+/, "");
	sendResults(4,parseInt(temp),attempt,matrixStr);
	localStorage.setItem('v4-'+qNo,'1');
	}
	
else{
	recLap();
	document.getElementById(qNo+"_"+ansNo+"X").style.display='block';
	setTimeout (function () {document.getElementById(qNo+"_"+ansNo+"X").style.display='none';} , 700);
	//sounds[49].play();
	playSound(35);
	}
	
	iShouldWait=true;
	setTimeout(function(){iShouldWait=false;},300);
	}
	

	}
	
////////////////////
var q5_1=0;
var q5_2=0;
var q5_3=0;
var q5_4=0;
var q5_5=0;
var q5_6=0;
var q5_7=0;
var q5_8=0;
var q5_9=0;



function listen_5() {
	if (q5_1 == 1)
	if (q5_2 == 1){
	if (q5_3 == 1){
	if (q5_4 == 1){
	if (q5_5 == 1){
	if (q5_6 == 1){
	if (q5_7 == 1){
	if (q5_8 == 1){
	if (q5_9 == 1){
		
	recLap();
	stopRecording();
	sendResults(4,5,attempt,matrixStr);
		localStorage.setItem('v4-q5','1');
		return;
	}}}}}}}}
	

	setTimeout(listen_5,300);
	}
////////////////////
var q6_1=0;
var q6_2=0;
var q6_3=0;
var q6_4=0;
var q6_5=0;
var q6_6=0;
var q6_7=0;
var q6_8=0;


function listen_6() {
	if (q6_1 == 1)
	if (q6_2 == 1){
	if (q6_3 == 1){
	if (q6_4 == 1){
	if (q6_5 == 1){
	if (q6_6 == 1){
	if (q6_7 == 1){
	if (q6_8 == 1){
		
		recLap();
		stopRecording();
		sendResults(4,6,attempt,matrixStr);
		localStorage.setItem('v4-q6','1');
		return;
	}}}}}}}
	

	setTimeout(listen_6,300);
	}
////////////////////
function mkDragables5() {
	var w = $('#q5-1').width();
	var h = $('#q5-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q5Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q5-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-6').position().top-$('#placeholder5-6').parent().position().top;
	var ans_left= $('#placeholder5-6').position().left-$('#placeholder5-6').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_1=1;
 //sounds[48].play();
 playSound(34);
  snap2grid('q5-1','placeholder5-6');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});
//
$( "#q5-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-5').position().top-$('#placeholder5-5').parent().position().top;
	var ans_left= $('#placeholder5-5').position().left-$('#placeholder5-5').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_2=1;
//sounds[48].play();
playSound(34);
  snap2grid('q5-2','placeholder5-5');
 }
 else {
	 //report
//	sounds[49].play();
playSound(35);
	 }
});
//
$( "#q5-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-9').position().top-$('#placeholder5-9').parent().position().top;
	var ans_left= $('#placeholder5-9').position().left-$('#placeholder5-9').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_3=1;
// sounds[48].play();
playSound(34);
  snap2grid('q5-3','placeholder5-9');
 }
 else {
	 //report
//	sounds[49].play();
playSound(35);
	 }
});
//
$( "#q5-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-3').position().top-$('#placeholder5-3').parent().position().top;
	var ans_left= $('#placeholder5-3').position().left-$('#placeholder5-3').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_4=1;
// sounds[48].play();
playSound(34);
  snap2grid('q5-4','placeholder5-3');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});
//
$( "#q5-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-7').position().top-$('#placeholder5-7').parent().position().top;
	var ans_left= $('#placeholder5-7').position().left-$('#placeholder5-7').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_5=1;
//sounds[48].play();
playSound(34);
  snap2grid('q5-5','placeholder5-7');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});

//
$( "#q5-6" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-4').position().top-$('#placeholder5-4').parent().position().top;
	var ans_left= $('#placeholder5-4').position().left-$('#placeholder5-4').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-6" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_6=1;
//sounds[48].play();
playSound(34);
  snap2grid('q5-6','placeholder5-4');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});

//
$( "#q5-7" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-8').position().top-$('#placeholder5-8').parent().position().top;
	var ans_left= $('#placeholder5-8').position().left-$('#placeholder5-8').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-7" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_7=1;
//sounds[48].play();
playSound(34);
  snap2grid('q5-7','placeholder5-8');
 }
 else {
	 //report
//	sounds[49].play();
playSound(35);
	 }
});

$( "#q5-8" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-2').position().top-$('#placeholder5-2').parent().position().top;
	var ans_left= $('#placeholder5-2').position().left-$('#placeholder5-2').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-8" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_8=1;
//sounds[48].play();
playSound(34);
  snap2grid('q5-8','placeholder5-2');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});

$( "#q5-9" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder5-1').position().top-$('#placeholder5-1').parent().position().top;
	var ans_left= $('#placeholder5-1').position().left-$('#placeholder5-1').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q5-9" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q5_9=1;
//sounds[48].play();
playSound(34);
  snap2grid('q5-9','placeholder5-1');
 }
 else {
	 //report
//	sounds[49].play();
playSound(35);
	 }
});

	}

////////////////////
function mkDragables6() {
	var w = $('#q6-1').width();
	var h = $('#q6-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q6Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q6-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-1').position().top-$('#placeholder6-1').parent().position().top;
	var ans_left= $('#placeholder6-1').position().left-$('#placeholder6-1').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_1=1;
// sounds[48].play();
playSound(34);
  snap2grid('q6-1','placeholder6-1');
 }
 else {
	 //report
	 //sounds[49].play();
	 playSound(35);
	 }
});
//
$( "#q6-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-2').position().top-$('#placeholder6-2').parent().position().top;
	var ans_left= $('#placeholder6-2').position().left-$('#placeholder6-2').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_2=1;
//sounds[48].play();
playSound(34);
  snap2grid('q6-2','placeholder6-2');
 }
 else {
	 //report
	//sounds[49].play();
	playSound(35);
	 }
});
//
$( "#q6-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-8').position().top-$('#placeholder6-8').parent().position().top;
	var ans_left= $('#placeholder6-8').position().left-$('#placeholder6-8').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_3=1;
// sounds[48].play();
playSound(34);
  snap2grid('q6-3','placeholder6-8');
 }
 else {
	 //report
//	sounds[49].play();
playSound(35);
	 }
});
//
$( "#q6-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-4').position().top-$('#placeholder6-4').parent().position().top;
	var ans_left= $('#placeholder6-4').position().left-$('#placeholder6-4').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_4=1;
// sounds[48].play();
playSound(34);
  snap2grid('q6-4','placeholder6-4');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});
//
$( "#q6-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-5').position().top-$('#placeholder6-5').parent().position().top;
	var ans_left= $('#placeholder6-5').position().left-$('#placeholder6-5').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_5=1;
//sounds[48].play();
playSound(34);
  snap2grid('q6-5','placeholder6-5');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});

//
$( "#q6-6" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-6').position().top-$('#placeholder6-6').parent().position().top;
	var ans_left= $('#placeholder6-6').position().left-$('#placeholder6-6').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-6" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_6=1;
//sounds[48].play();
playSound(34);
  snap2grid('q6-6','placeholder6-6');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});

//
$( "#q6-7" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-3').position().top-$('#placeholder6-3').parent().position().top;
	var ans_left= $('#placeholder6-3').position().left-$('#placeholder6-3').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-7" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_7=1;
//sounds[48].play();
playSound(34);
  snap2grid('q6-7','placeholder6-3');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});

$( "#q6-8" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder6-7').position().top-$('#placeholder6-7').parent().position().top;
	var ans_left= $('#placeholder6-7').position().left-$('#placeholder6-7').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q6-8" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q6_8=1;
//sounds[48].play();
playSound(34);
  snap2grid('q6-8','placeholder6-7');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(35);
	 }
});


	}

////////////////////
	
function hasColided(obj1_left, obj1_top, obj2_left, obj2_top){
	var w = $('#q5-1').width();
	var h = $('#q5-1').height();
	var w2 = ($('#placeholder5-1').width());
	var h2= ($('#placeholder5-1').height());
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left;
    var br = obj2_left+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top;
    var bb = obj2_top+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}
///////////////////////////
function hasColided2(obj1_left, obj1_top, obj2_left, obj2_top){
	var w = $('#q6-1').width();
	var h = $('#q6-1').height();
	var w2 = ($('#placeholder6-1').width());
	var h2= ($('#placeholder6-1').height());
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left;
    var br = obj2_left+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top;
    var bb = obj2_top+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}

