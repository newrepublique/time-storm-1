// JavaScript Document

function scene3() {
	window.parent.pauseSounds();
	window.parent.playSound(2);
	var iDelay=2000;
	
	showScene('scene-3');
	updatePangoTxt();
	updatePhoneTxt(3,'You are back in the lab.');
	updateCompleted(10);
	
	
	setTimeout(function() { document.getElementById('s3-continaer').style.opacity=1; }, iDelay);
		//refreshPhoneTxt();
	
	setTimeout (function () {showDialogue('s3-dia-1');}, 1500 + iDelay);
	setTimeout (function () {hideDialogue('s3-dia-1');}, 4500 + iDelay);
	
	setTimeout (function() {
		document.getElementById('lab-1').style.display='block';
		document.getElementById('lab-2').style.display='block';
		document.getElementById('lab-3').style.display='block';
		document.getElementById('lab-4').style.display='block';
		}, 5500+iDelay);
	
	
}


///////////////////////////
var err=0;
function showErr() {
	
	err++;
	dia='s3-dia-4';
	
	if (err==1) dia='s3-dia-2';
	if (err==2) dia='s3-dia-3';
	
	
	window.parent.playSound(35);
	
	setTimeout (function () {showDialogue(dia);} , 1000);
	setTimeout (function () {hideDialogue(dia);} , 4000);
	}
////////////////////////////////
function showQ1() {
	document.getElementById('lab-1').style.display='none';
		document.getElementById('lab-2').style.display='none';
		document.getElementById('lab-3').style.display='none';
		document.getElementById('lab-4').style.display='none';
	listen_1();
	window.parent.showQ1();
	}
	
function listen_1 () {
	if (localStorage.getItem('v4-q1')=='1'){
		window.parent.hideQ1();
		updateCompleted(15);
		setTimeout(function(){window.parent.showQ2(); listen_2();},1000);
		}
	else{
		setTimeout(listen_1,200);
		}	
	}
//////////////////////////////////
function listen_2 () {
	if (localStorage.getItem('v4-q2')=='1'){
		window.parent.hideQ2();
		updateCompleted(20);
		setTimeout(function(){window.parent.showQ3(); listen_3();},1000);
		}
	else{
		setTimeout(listen_2,200);
		}	
	}
////////////////////////////////
function listen_3 () {
	if (localStorage.getItem('v4-q3')=='1'){
		window.parent.hideQ3();
		updateCompleted(25);
		setTimeout(function(){window.parent.showQ4(); listen_4();},1000);
		}
	else{
		setTimeout(listen_3,200);
		}	
	}
////////////////////////////////
function listen_4 () {
	if (localStorage.getItem('v4-q4')=='1'){
		window.parent.hideQ4();
		updateCompleted(30);
		setTimeout(function(){window.parent.showQ5(); listen_5();},1000);
		}
	else{
		setTimeout(listen_4,200);
		}	
	}
////////////////////////////////
function listen_5 () {
	if (localStorage.getItem('v4-q5')=='1'){
		window.parent.hideQ5();
		updateCompleted(50);
		setTimeout(function(){routine2();},1000);
		}
	else{
		setTimeout(listen_5,200);
		}	
	}

//////////////////////////////////
function routine2() {
	
	document.getElementById('s3-continaer').style.opacity=0;
	
	setTimeout(function() {
		window.parent.playSound(11);
		document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg5.jpg)';
		setTimeout(function() { document.getElementById('s3-continaer').style.opacity=1;},1000);
		},1000);
		
	setTimeout (function() { window.parent.playSound(12);},4000);
	
	setTimeout (function() { window.parent.playSound(13);},10000);
	
	setTimeout (function() { window.parent.pauseSound(11); document.getElementById('s3-continaer').style.opacity=0;},17000);
	
	setTimeout (function() { document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
		setTimeout(function() { document.getElementById('s3-continaer').style.opacity=1;},1000);
		},18000);
		
	setTimeout (function() {  showDialogue('s3-dia-5'); window.parent.pauseSound(22);},20000);
	
	setTimeout (function() {  hideDialogue('s3-dia-5');},23000);
	
	setTimeout (function() {  window.parent.showQ6(); listen_6();},25000);
	}
	

////////////////////////////////
function listen_6 () {
	if (localStorage.getItem('v4-q6')=='1'){
		window.parent.hideQ6();
		updateCompleted(70);
		setTimeout(function(){window.parent.showQ7(); listen_7();},1000);
		}
	else{
		setTimeout(listen_6,200);
		}	
	}
	
////////////////////////////////
function listen_7 () {
	if (localStorage.getItem('v4-q7')=='1'){
		window.parent.hideQ7();
		updateCompleted(75);
		setTimeout(function(){routine3();},1000);
		}
	else{
		setTimeout(listen_7,200);
		}	
	}
////////////////////////////////////////

function routine3() {
	
	var iDelay=1000;
	
	setTimeout (function () {
		showDialogue('s3-dia-6');
		window.parent.playSound(23);
		} , 2000+iDelay);
		
	setTimeout (function () {
		hideDialogue('s3-dia-6');
		} , 4500+iDelay);
		
	setTimeout (function () {
		showDialogue('s3-dia-7');
		window.parent.playSound(31);
		} , 5500+iDelay);
		
	setTimeout (function () {
		hideDialogue('s3-dia-7');
		} , 8500+iDelay);
		
	setTimeout (function () {
		showDialogue('s3-dia-8');
		window.parent.playSound(24);
		} , 9500+iDelay);
		
	setTimeout (function () {
		hideDialogue('s3-dia-8');
		} , 10500+iDelay);
		
	setTimeout (function () {
		showDialogue('s3-dia-9');
		window.parent.playSound(32);
		} , 11500+iDelay);
		
	setTimeout (function () {
		hideDialogue('s3-dia-9');
		} , 13500+iDelay);
		
	setTimeout (function () {
		window.parent.showQ8(); listen_8();
		} , 15000+iDelay);
	}
	
	
///////////////////////////////////////
function listen_8 () {
	if (localStorage.getItem('v4-q8')=='1'){
		window.parent.hideQ8();
		updateCompleted(80);
		setTimeout(function(){window.parent.showQ9(); listen_9();},1000);
		}
	else{
		setTimeout(listen_8,200);
		}	
	}
	
///////////////////////////////////////
function listen_9 () {
	if (localStorage.getItem('v4-q9')=='1'){
		window.parent.hideQ9();
		updateCompleted(85);
		setTimeout(function(){window.parent.showQ10(); listen_10();},1000);
		}
	else{
		setTimeout(listen_9,200);
		}	
	}
	
///////////////////////////////////////
function listen_10 () {
	if (localStorage.getItem('v4-q10')=='1'){
		window.parent.hideQ10();
		updateCompleted(90);
		setTimeout(function(){routine4();},1000);
		}
	else{
		setTimeout(listen_10,200);
		}	
	}
	
///////////////////////////////////////////
function routine4() {
	
	document.getElementById('s3-continaer').style.opacity=0;
	
	setTimeout(function() {
		window.parent.playSound(11);
		document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
		setTimeout(function() { document.getElementById('s3-continaer').style.opacity=1;},1000);
		},1500);
		
	setTimeout (function() { 
	showDialogue('s3-dia-10');
		window.parent.playSound(33);
		},4000);
		
	setTimeout (function() { 
	hideDialogue('s3-dia-10');
		},6500);
		
	setTimeout (function() { 
	showDialogue('s3-dia-11');
		window.parent.playSound(25);
		},7000);
		
	setTimeout (function() { 
	hideDialogue('s3-dia-11');
		},8000);
		
	setTimeout (function() { 
	document.getElementById('s3-continaer').style.opacity=0;
		},8200);
		
	setTimeout(function() {
		document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg8.jpg)';
		setTimeout(function() { document.getElementById('s3-continaer').style.opacity=1;},1000);
		},10200);
		
	setTimeout (function() { 
	window.parent.playSound(15);
		},13000);
		
	setTimeout (function() { 
	showDialogue('s3-dia-12');
	updateCompleted(100);
		window.parent.playSound(26);
		},19000);
		
	setTimeout (function() { 
	hideDialogue('s3-dia-12');
		},22000);
		
	setTimeout (function() { 
	window.parent.playSound(16);
		},23000);
		
	setTimeout (function() { 
	showDialogue('s3-dia-13');
		},29000);
		
	setTimeout (function() { 
	hideDialogue('s3-dia-13');
		},36000);
		
	setTimeout (function() { 
	document.getElementById('s3-continaer').style.opacity=0;
		},37000);
		
	setTimeout (function() { 
	window.parent.gotoScene('countdown.html');
		},38000);
	}