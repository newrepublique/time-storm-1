<?php 

$v=$_GET['v'];

switch ($v) {
	
	case "2":
$str= <<<EOF
{
	"q1": 
	{ 	
		"title": "Get Moving Challenge - one of two",
		"qTxt": "What is the force that powers a land yacht?",
		"ans" : ["Powered by a combustion engine","A solar powered engine","Powered by the wind","Powered by nuclear power"],
		"helpTxt" : "In order to create motion, objects and vehicles often require another force or source of energy to help them do this. Currently, most cars rely on petroleum to fuel their engine. These cars produce carbon dioxide as a by-product that pollutes the atmosphere. Many different organizations recognize that this is unsustainable and are experimenting with other ways of powering vehicles including solar power.<br> <br>Given that the skipper's craft doesn't appear to be powered by either petroleum or solar power, work out what is the most logical answer.",
		"offset": 3
		},
		
	"q2": 
	{ 	"title": "Get Moving Challenge - two of two",
		"qTxt": "When an object accelerates using the power of another force it is subject to?",
		"ans" : ["Newton's First Law","Newton's Second Law","Newton's Third Law","Newtown's Third Law"],
		"helpTxt" : "When we describe the forces that act on an objects motion and movement, we use the series of laws described by Sir Isaac Newton, commonly referred to as Newton's Three Laws of Motion. The three laws are:<br><br>Newton's first law: Objects do not move from their current state unless acted on by another object or force.<br><br>Newton's second law: The acceleration on an object is proportional to the amount of energy used to move the mass of the object.<br><br>Newton's third law: When two objects meet in movement they are equal in force but opposite in direction.",
		"offset": 2
		},
		
	"q3": 
	{ 	"title": "De-sal Plant challenge",
		"helpTxt" : "Solar desalination is one option for providing drinkable water in environments where the water source is either polluted, too salty or has another impurity problem. In systems that utilize solar desalination, water is pumped from the water source in a chamber that can utilize an evaporator (heating the water to become steam to remove impurities) and a condenser (to turn the steam back into water to collect the now clean water known as distillate). This process is powered by a solar collector that is using the heat energy of the sun. Any waste impurities such as brine, if the water is salty, are removed from the system. ",
		},
		
	"q4": 
	{ 	"title": "Travel Time Challenge - one of two",
		"qTxt": "Which of the formula below would you use to find out the amount of time taken to travel somewhere?",
		"ans" : ["Time = Speed/Distance","Time = Speed x Distance","Time = Distance x Speed","Time = Distance/Speed"],
		"helpTxt" : "Time, speed and distance are three factors that are interdependent of each other, meaning that they each provide information about the other. When working out equations to do with measuring these three things, remember the relations between them. They are proportional and dividable in different scenarios. ",
		"offset": 4
		},

	"q5": 
	{ 	"title": "Travel Time Challenge - two of two",
		"qTxt": "If the right formula is Time = Distance/Speed, and we know that the De-Sal plant in 65km a way and that the land yacht travels 40km an hour  how long would it take approximately Rounded up to the nearest tens of minutes?",
		"ans" : ["It would take approximately 1 hr 40 mim","It would take approximately 2 hrs","It would take approximately 1 hr 30 min","It would tale approximately 1 hr 50 min"],
		"helpTxt" : "Time, speed and distance are three factors that are interdependent of each other, meaning that they each provide information about the other. When working out equations to do with measuring these three things, remember the relations between them. They are proportional and dividable in different scenarios.",
		"offset": 1
		},

		
	"q6": 
	{ 	"title": "Leaving the plant",
		"qTxt": "Now that you've re-started the desalination plant you'll need to figure out how far it is to the city, the captain mentioned something about the city is 20km away and that a land yacht travels in this wind at about 40km an hour. How long will it take you to get to the city?",
		"ans" : ["20 mins","25 mins","30 mins","35 mins"],
		"helpTxt" : "Time, speed and distance are three factors that are interdependent of each other, meaning that they each provide information about the other. When working out equations to do with measuring these three things, remember the relations between them. They are proportional and dividable in different scenarios.",
		"offset": 3
		},	
	}
EOF;

		break;
		
		
		case "3": 
$str= <<<EOF
		
	"q1": 
	{ 
	"title": "Carbon Cycle Challenge", 
	"helpTxt" : "The carbon cycle is the name we give to the exchange of carbon in an ecosystem. This includes the release and the capture of carbon in different states as it moves through the environment. Along with the oxygen and water cycles it helps sustain life on Earth.<br><br>The carbon cycle is continually taking place around us, from phytoplankton in the ocean using the carbon to create food, to it being released in the air by animals through respiration. The carbon cycle is delicately balanced and any change to the processes of this cycle can cause phenomena such as global warming. Global warming is generally described as the process of the planet warming up due to excess carbon being released into the atmosphere.", },
	
	 "q2": 
	 { "title": "Coal Power Plant Challenge", 
	 "helpTxt" : "A common way of producing electricity is through the use of coal fired power plants. Coal fired power plants facilitate the super heating of water to drive turbines in the boiler chamber. The energy produced by these turbines is then funneled into the generator and transformer and out into the electricity grid.<br><br>Coal fired power plants are thought of as being a significant contributor to global warmer, due to the amount of carbon dioxide that is released into the atmosphere and the amount of energy that is wasted during the electricity generation process.", }, 
	 
	 "q3": 
	 { "title": "Energy Output Challenge", 
	 "helpTxt" : "Handy hint: When calculating the amount of waste energy that is lost in electricity generation remember that there may be more than one factor to include. ", 
	 "offset" : 3, }, 
	 
	 "q4": 
	 { "title": "Alternative Energy Challenge - one of two", 
	 "qTxt": "Which of the following best describe what geothermal energy is?", 
	 "ans" : ["Geothermal energy is energy obtained from the heat of the sun","Geothermal energy is energy obtained from the heat released by the burning of coal","Geothermal energy is energy obtained from the heat within the Earth's crust","Geothermal energy is energy obtained by heating the Earth's crust"], 
	 "helpTxt" : "Energy can be obtained from a variety of different sources, from naturally replenishing sources such as the wind, solar and ocean current tides, or, heat from the Earth (known as Geo-thermal energy). These are increasingly being seen as alternatives to the use of coal fired power plants to create electricity. ", 
	 "offset": 3 
	 }, 
	 
	 "q5": 
	 { "title": "Alternative Energy Challenge - two of two", 
	 "qTxt": "How is solar energy normally captured?", 
	 "ans" : ["Using photovoltaic cells","Using membrane cells","Using photosensitive cells","Using wind turbines"], 
	 "helpTxt" : "Energy can be obtained from a variety of different sources, from naturally replenishing sources such as the wind, solar and ocean current tides, or, heat from the Earth (known as geo-thermal energy). These are increasingly being seen as alternatives to the use of coal fired power plants to create electricity.<br><br>Energy that is captured through solar means is captured through a special type of cell called a photovoltaic cell. These cells are specially designed to transform the heat and light energy of the sun into an electrical format. ", 
	 "offset": 1 }, 
	 
	 "q6": 
	 { "title": "Biosphere Challenge - one of two", "qTxt": "Which of the following power sources are NOT considered to be green power?", 
	 "ans" : ["Biomass","Wave energy","Hydro-electricity","Nuclear power"], 
	 "helpTxt" : "Green power is a name applied to energy produced through naturally occurring means or processes. Examples of this include solar, wind and biomass. Biomass energy is produced through capturing and using the gases caused by natural processes such as decomposition. ", 
	 "offset": 4 }, 
	 
	 "q7": 
	 { "title": "Biosphere Challenge - two of two", "qTxt": "Extracting and using natural resources found in the Earth can also have detriment effects on the atmosphere and biosphere. What is an essential property of a natural resource?", 
	 "ans" : ["It has a low cost and is readily available","It has a useful property that other resources don't have","It is easy to take out of the Earth","They are not readily available"], 
	 "helpTxt" : "Handy hint: When thinking about natural resources remember that they are often valued for a single property that they have. ", 
	 "offset": 2 },
		
EOF;
		break;
		
		
	case "4": 
	$str= <<<EOF
		
	"q1": 
	{ 
	"title": "Microbe Analysis Challenge - one of five", 
	"qTxt": "Bacteria come in many shapes and arrangements, you need to figure out which type is most abundant in your soil sample. In order to do this, you need to know the difference between the different samples. How would you best describe the shape of \"Bacilli\" type bacteria?",
	"ans": ["They are spherical in shape","They are corkscrew in shape","They are rod shaped","They are spherical with a stalk"],
	"helpTxt" : "Bacteria come in a range of sizes, shapes and formations. Some of the most common shapes include:<br><br>Appendage shaped – Bacteria having a 'tail', 'comma' or 'flageleum'<br><br>Bacillus – Rod shaped bacteria<br><br>Cocci – Spherical shaped bacteria<br><br>Spirillium/Spirochete – Spiral shaped bacteria<br><br>Vibrio – Curved rod shaped", 
	"offset": 3
	},
	
	 "q2": 
	{ 
	"title": "Microbe Analysis Challenge - two of five", 
	"qTxt": "The bacteria under you microscope doesn't appear to be rod shaped, that rules out Bacilli type bacteria. When you're examining the samples you notice that they are some colonies of bacteria that are spherical in shape - what type of bacteria are they likely to be?",
	"ans": ["Cocci type bacteria","Appendaged bacteria","Coco type bacteria","Spirochete type bacteria"],
	"helpTxt" : "Bacteria come in a range of sizes, shapes and formations. Some of the most common shapes include:<br><br>Appendage shaped – Bacteria having a 'tail', 'comma' or 'flageleum'<br><br>Bacillus – Rod shaped bacteria<br><br>Cocci – Spherical shaped bacteria<br><br>Spirillium/Spirochete – Spiral shaped bacteria<br><br>Vibrio – Curved rod shaped", 
	"offset": 1
	},
	 
	"q3": 
	{ 
	"title": "Microbe Analysis Challenge - three of five", 
	"qTxt": "As you examine the Cocci bacteria formations, you notice that they are grouped into 'colonies' on your slide sample. The bacteria are arranged in pairs, what is this type of arrangement of Cocci bacteria known as?",
	"ans": ["Streptococci","Staphylococci","Tetrad","Diplococci"],
	"helpTxt" : "Bacteria come in a range of sizes, shapes and formations. Some of the most common formation of Coccus bacteria include:<br><br>Diplococci – Cocci in pairs<br><br>Sarcinae – Cocci in groups of 8, 16, 32 cells etc<br><br>Streptococci – Cocci in chains<br><br>Tetras – Cocci in groups of four<br><br>", 
	"offset": 4
	},
	
	"q4": 
	{ 
	"title": "Microbe Analysis Challenge - four of five", 
	"qTxt": "After careful analysis it doesn't look like those bacteria under your microscope are Bacilli or Cocci bacterium as there aren't many colonies. The most pre-dominant bacteria under your  microscope seem to have additional features. You notice that they seem to have \"tails\" or \"stalks\" on them. What type of bacteria typically have these items?",
	"ans": ["Helical form","Comma form","Vibrio form","Corkscrew form"],
	"helpTxt" : "Bacteria come in a range of sizes, shapes and formations. Some of the most common shapes include:<br><br>Appendage shaped – Bacteria having a 'tail', 'comma' or 'flageleum'<br><br>Bacillus – Rod shaped bacteria<br><br>Cocci – Spherical shaped bacteria<br><br>Spirillium/Spirochete – Spiral shaped bacteria<br><br>Vibrio – Curved rod shaped", 
	"offset": 2
	},
	
	"q5": 
	{ 
	"title": "Microbe Analysis Challenge - five of five", 
	"helpTxt" : "Bacteria are single celled organisms that contain a range or 'organelles' or special features much like organs in the human body to help regulate their function. Some features of bacterium can include:<br><br>Cell wall: Provides structure to the cell, as well as incasing the cell in a protective layer<br><br>Cytoplasm: The fluid component of a cell that houses the organelles and other components in the interior of the cell<br><br>Pili – Protein based organelles that assist in the exchange of materials, in and out of the cell<br><br>Ribosomes: Organelles that assist in the replication of the cell", 
	},
	
	"q6": 
	{ 
	"title": "Nitrogen Cycle Challenge - one of two", 
	"helpTxt" : "The nitrogen cycle expresses the movement of nitrogen in an environment, as it passes through its various states and forms. There are many other elements that play a role in the nitrogen cycle. One element that plays a substantial role is ammonia. Ammonia is actually converted by some forms of bacteria into nitrogen when it is used to fuel plant growth and development.", 
	},

	"q7": 
	{ 
	"title": "Nitrogen Cycle Challenge - two of two", 
	"qTxt": "It seems like you're making progress! Not far to go till you've cracked what's going on. Now that you know the type of bacteria in the soil that seems to be causing the problems. You need to figure out why. What is one of the typical roles of bacteria in the Nitrogen Cycle?",
	"ans": ["To convert ammonia in the soil to nitrogen","To convert oxygen into nitrogen","To convert ammonia into oxygen","To convert water into ammonia"],
	"helpTxt" : "Other sources of nitrogen include animal waste (excretion) and the burning of plant and animal matter. Other actors, such as some types of bacteria in the nitrogen cycle help convert nitrogen into other elements or break it down into other forms that can be used by plants.", 
	"offset": 1
	},	
	
	"q8": 
	{ 
	"title": "Ecosystem Challenge - one of two", 
	"qTxt": "The dome is low on oxygen. It would seem that with the low oxygen levels in the dome you have a specific sort of bacteria that does not need oxygen for its growth,  what is the common name of this type of bacteria?",
	"ans": ["Anaerobic bacteria","Enerobic bacteria","Aerobic bacteria","Anti-aerobic bacteria"],
	"helpTxt" : "Bacteria are typically classed into two types based on their respiration process, 'anaerobic' (without oxygen) or 'aerobic' (with oxygen). Most bacterium known to us falls into the aerobic category utilizing oxygen to assist in normal functioning and on-going respiration. However, specialized bacteria in the carbon and nitrogen cycles utilize other elements to assist in respiration and the normal functioning of the their cells.<br><br>Increasingly we are discovering new types of bacteria, such as special bacteria that lives under sea volcanoes that actually uses sulpher as the key component in their respiration cycle.", 
	"offset": 1
	},		
	
	"q9": 
	{ 
	"title": "Ecosystem Challenge - two of two", 
	"qTxt": "As you're analysing the bacteria, you remember reading something about closed ecosystems. That would seem to describe how the dome works. What best describes what a closed ecosystem is?",
	"ans": ["Where organisms, materials and elements such as nitrogen and oxygen are able to enter and exit the environment.","Where new  organisms, materials and elements such as nitrogen and oxygen are added to an environment.","Where new organisms, materials and elements are continually taken away from an environment.","Where organisms, materials and elements such as nitrogen and oxygen are not able to enter and exit the environment."],
	"helpTxt" : "Ecosystems are delicately balanced environments that help sustain life, generally specialized to the type of ecosystem. For example Mediterranean ecosystems have particular characteristics that define them, such as soil type, plant types, a mineral profile and other specific weather or geological features that are used to distinguish them. All ecosystems fall into one of two categories:<br><br>Open – Allowing the movement of materials, minerals and organisms in and out of the ecosystem.<br><br>Closed – Where the movement of materials, minerals and organisms are not allowed to move in or out of the ecosystem.", 
	"offset": 4
	},
	
	"q10": 
	{ 
	"title": "Constructing a Solution", 
	"qTxt": "Now that you've discovered why the dome is in such a dangerous state, you need to figure a possible solution. Part of the answer has to do with the amount of oxygen that is currently in the eco-system. Which answer do you think will help you to get the dome back on track?",
	"ans": ["Reduce the amount of plants to slow the rate of photosynthesis"," Increase the amount of plants to increase the rate of photosynthesis","Increase the amount of animals to increase the rate of photosynthesis","Increase the amount of plants to reduce the rate of photosynthesis"],
	"helpTxt" : "Photosynthesis is the process by which plants respire and produce energy for their continued development and life – unfortunately only you can figure out the answer to this one!", 
	"offset": 2
	},
EOF;
		break;
}

echo $str;
?>