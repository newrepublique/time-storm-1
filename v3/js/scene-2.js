// JavaScript Document

function scene2() {
	var iDelay=2000;
	
	showScene('scene-2');
	
	window.parent.playSound('0');
		
	updatePangoTxt();
		
	if (localStorage.getItem('v3-q4')=='1') {		
	document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
	document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
	setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; }, 1000);
	window.parent.showQ5();
	listen_5();
	return;
	}
	
	if (localStorage.getItem('v3-q3')=='1') {		
	document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
	document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
	setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; }, 1000);
	window.parent.showQ4();
	listen_4();
	return;
	}
	
	if (localStorage.getItem('v3-q2')=='1') {		
	document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
	document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
	setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; }, 1000);
	window.parent.showQ3();
	listen_3();
	return;
	}
	
	if (localStorage.getItem('v3-q1')=='1') {		
	document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
	document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
	setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; }, 1000);
	window.parent.showQ2();
	listen_2();
	return;
	}
	
	if(localStorage.getItem('v3-hasClearedTip')=='1') {
		gotoStreet();
		return;
		};
	
	
	////////////////actual function begins now
	
	setTimeout (function () {	
		document.getElementById('s2-continaer').style.opacity=1;
		updatePhoneTxt(3,'You are at the outskirts of the city.');
		}, iDelay);
		
	
	setTimeout(function() {
		showDialogue('s2-dia-1');
		} , 2000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-1');
		} , 8000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		} , 0000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg2.jpg)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1; }, 1000);
		} , 10000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-2');
		setTimeout(function() { window.parent.playSound(31); }, 500);
		} , 13000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-2');
		} , 19000+iDelay);
		
		
	setTimeout(function() {
		window.parent.playSound(51);
		document.getElementById('greenDot').style.display='block';
		} , 21000+iDelay);
		
}

///////////////////////////
var carsVisited=0;
function gotoTip() {
	
	var iDelay=1000;
	document.getElementById('greenDot').style.display='none';
	
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound('0');
		} , iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg3.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-3.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1;window.parent.playSound(1); }, 1000);
		} , 1000+iDelay);
	
	
	setTimeout(function() {
		showDialogue('s2-dia-3');
		} , 3000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-3');
		} , 8000+iDelay);
		
	setTimeout(function() {
		updatePhoneTxt(3,'You have reached the city\'s tip.');
		} , 9000+iDelay);
		
	setTimeout(function() {
		document.getElementById('car-1').style.display='block';
		document.getElementById('car-2').style.display='block';
		document.getElementById('car-3').style.display='block';
		} , 10000+iDelay);
	
	}
	
///////////////////////////////
function gotoStreet() {
	localStorage.setItem('v3-hasClearedTip','1');
	
	var iDelay=1000;
	
	document.getElementById('greenDot2').style.display='none';
	
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound(1);
		} , iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg4.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1;window.parent.playSound(2); }, 1000);
		} , 2000+iDelay);
		
	
	setTimeout(function() {
		showDialogue('s2-dia-5');
		} , 5000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound(1);
		} , 8000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg5.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1;window.parent.playSound(2); }, 1000);
		} , 9000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound(1);
		} , 12000+iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg6.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1;window.parent.playSound(2); }, 1000);
		} , 14000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-5');
		} , 16000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-6');
		setTimeout(function() { window.parent.playSound(6); }, 500);
		} , 17000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-6');
		} , 19000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-7');
		setTimeout(function() { window.parent.playSound(32); }, 500);
		} , 20000+iDelay);
	
	setTimeout(function() {
		hideDialogue('s2-dia-7');
		} , 23000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-8');
		setTimeout(function() { window.parent.playSound(7); }, 500);
		} , 24000+iDelay);
	
	setTimeout(function() {
		hideDialogue('s2-dia-8');
		} , 27000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-9');
		setTimeout(function() { window.parent.playSound(33); }, 500);
		} , 28000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-9');
		} , 31000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-10');
		setTimeout(function() { window.parent.playSound(8); }, 500);
		} , 32000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-10');
		updatePhoneTxt(1,'You have met Max.');
		} , 34000+iDelay);
	
	setTimeout(function() {
		showDialogue('s2-dia-11');
		setTimeout(function() { window.parent.playSound(34); }, 500);
		} , 35000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-11');
		} , 39000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-12');
		setTimeout(function() { window.parent.playSound(9); }, 500);
		} , 40000+iDelay);
	
	setTimeout(function() {
		hideDialogue('s2-dia-12');
		} , 46000+iDelay);
		
	setTimeout(function() {
		showSlides();
		window.parent.pauseSound(2);
		} , 47000+iDelay);
		
}

function showSlides(){
	
	showObj('slide-obj');
	
	var iDelay=1000;
		
	setTimeout(function() {
		showDialogue('s2-dia-13');
		setTimeout(function() { window.parent.playSound(10); }, 500);
		} , iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-13');
		} , 6000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-14');
		setTimeout(function() { window.parent.playSound(35); }, 500);
		} , 7000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-14');
		} , 9000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-15');
		setTimeout(function() { window.parent.playSound(12); }, 500);
		} , 10000+iDelay);
		
	setTimeout(function() {
		document.getElementById('slide-obj').style.backgroundImage='url(imgs/obj-5.png)';
		} , 13000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-15');
		} , 22000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-16');
		setTimeout(function() { window.parent.playSound(36); }, 500);
		} , 23000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-16');
		} , 25000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-17');
		setTimeout(function() { window.parent.playSound(13); }, 500);
		} , 26000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-17');
		} , 33000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-18');
		setTimeout(function() { window.parent.playSound(37); }, 500);
		} , 34000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-18');
		} , 36000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-19');
		setTimeout(function() { window.parent.playSound(11); }, 500);
		} , 37000+iDelay);
		
	setTimeout(function() {
		document.getElementById('slide-obj').style.backgroundImage='url(imgs/obj-5.png)';
		} , 41000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-19');
		} , 49000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-20');
		setTimeout(function() { window.parent.playSound(38); }, 500);
		} , 50000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-20');
		} , 52000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-21');
		setTimeout(function() { window.parent.playSound(14); }, 500);
		} , 53000+iDelay);
		
	setTimeout(function() {
		document.getElementById('slide-obj').style.backgroundImage='url(imgs/obj-6.png)';
		} , 58000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-21');
		} , 65000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-22');
		setTimeout(function() { window.parent.playSound(15); }, 500);
		} , 66000+iDelay);
		
	setTimeout(function() {
		document.getElementById('slide-obj').style.backgroundImage='url(imgs/obj-7.png)';
		} , 70000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-22');
		} , 81000+iDelay);
		
	
	setTimeout(function () {
		hideObj('slide-obj');
		},83000+iDelay);
		
	setTimeout(function () {
		window.parent.showQ1();
		listen_1();
		},85000+iDelay);
	}
	
	

function resumeWithMax() {
	//alert('done');
	window.parent.pauseSound('0');
	window.parent.pauseSound(2);
	
	window.parent.hideQ5();
	
	var iDelay=1000;
	
	setTimeout(function() {
		document.getElementById('s2-continaer').style.opacity=0;
		window.parent.pauseSound(1);
		} , iDelay);
		
	setTimeout(function() {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
		document.getElementById('phone').style.backgroundImage='url(imgs/phone-4.png)';
		setTimeout(function() { document.getElementById('s2-continaer').style.opacity=1;window.parent.playSound(2); }, 1000);
		} , 2000+iDelay);
		
		iDelay=iDelay+5000;
	
	setTimeout(function() {
		showDialogue('s2-dia-24');
		},iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-24');
		},5000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-25');
		},6000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-25');
		},12000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-26');
		setTimeout(function() { window.parent.playSound(39); }, 500);
		},13000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-26');
		},16000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-27');
		setTimeout(function() { window.parent.playSound(16); }, 500);
		},17000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-27');
		},20000+iDelay);
	
	setTimeout(function() {
		showDialogue('s2-dia-28');
		setTimeout(function() { window.parent.playSound(40); }, 500);
		},21000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-28');
		},24000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-29');
		setTimeout(function() { window.parent.playSound(17); }, 500);
		},25000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-29');
		},27000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-30');
		},28000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-30');
		},33000+iDelay);
		
		
	setTimeout(function() {
		showDialogue('s2-dia-31');
		},34000+iDelay);
		
	setTimeout(function() {
		window.parent.playSound(51);
		document.getElementById('greenDot3').style.display='block';
		document.getElementById('greenDot4').style.display='block';
		},36000+iDelay);
	}

function gotoSleep() {
	hideDialogue('s2-dia-31');
	var iDelay=1000;
	
	setTimeout(function() {
		showDialogue('s2-dia-32');
		setTimeout(function() { window.parent.playSound(41); }, 500);
		document.getElementById('greenDot3').style.display='none';
		document.getElementById('greenDot4').style.display='none';
		},1000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-32');
		},6000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-33');
		setTimeout(function() { window.parent.playSound(18); }, 500);
		},7000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-33');
		},11000+iDelay);
		
	setTimeout (function () {
		document.getElementById('s2-continaer').style.opacity=0; 
		setTimeout(function(){window.parent.gotoScene('scene-3.html');},1000);
		} ,12000+iDelay);
	}

function gotoDome(){
	hideDialogue('s2-dia-31');
	var iDelay=1000;
	
	setTimeout(function() {
		showDialogue('s2-dia-34');
		setTimeout(function() { window.parent.playSound(43); }, 500);
		document.getElementById('greenDot3').style.display='none';
		document.getElementById('greenDot4').style.display='none';
		},1000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-34');
		},6000+iDelay);
		
	setTimeout(function() {
		showDialogue('s2-dia-35');
		setTimeout(function() { window.parent.playSound(21); }, 500);
		},7000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-35');
		},11000+iDelay);
		
	setTimeout (function () {
		document.getElementById('s2-continaer').style.opacity=0; 
		setTimeout(function(){window.parent.gotoScene('scene-4.html');},1000);
		} ,12000+iDelay);
	}

///////////////////////////////
function listen_1() {
		if (localStorage.getItem('v3-q1') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ1();
		updateCompleted(20);
		setTimeout(function() {
			window.parent.showQ2();
			listen_2();
			}, 2000);
		}
		
	else{
		
		setTimeout(listen_1,200);
		}
	
	}
	
function listen_2() {
		if (localStorage.getItem('v3-q2') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ2();
		updateCompleted(40);
		setTimeout(function() {
			window.parent.showQ3();
			listen_3();
			}, 2000);
		}
		
	else{
		
		setTimeout(listen_2,200);
		}
	
	}

function listen_3() {
		if (localStorage.getItem('v3-q3') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ3();
		updateCompleted(50);
		setTimeout(function() {
			window.parent.showQ4();
			listen_4();
			}, 3000);
		}
		
	else{
		
		setTimeout(listen_3,200);
		}
	
	}
	

function listen_4() {
		if (localStorage.getItem('v3-q4') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ4();
		updateCompleted(60);
		setTimeout(function() {
			window.parent.showQ5();
			listen_5();
			}, 3000);
		}
		
	else{
		
		setTimeout(listen_4,200);
		}
	
	}
	
function listen_5() {
		if (localStorage.getItem('v3-q5') == '1') {
		
		//window.parent.showQ4();
		window.parent.hideQ4();
		updateCompleted(70);
		setTimeout(function() {
			//window.parent.showQ4();
			//listen_4();
			resumeWithMax();
			}, 3000);
		}
		
	else{
		
		setTimeout(listen_5,200);
		}
	
	}
	
///////////////////////////////
function showObj(which){
	window.parent.playSound(50);
	document.getElementById('blacky').style.display='block';
	document.getElementById('blacky').style.opacity=1;
	
	setTimeout(function () {
		document.getElementById(which).style.display='block';
		}, 1000);
	
	}
	
function hideObj(which){
	window.parent.playSound(49);
	document.getElementById(which).style.display='none';

	document.getElementById('blacky').style.opacity=0;
	
	setTimeout(function () {
			document.getElementById('blacky').style.display='none';
		}, 1000);
	
	
	/* only for the tip/cars*/
	setTimeout (function () {
		if(carsVisited==3) {window.parent.playSound(51);
		document.getElementById('greenDot2').style.display='block';
		  carsVisited=0;
		  document.getElementById('car-1').style.display='none';
		document.getElementById('car-2').style.display='none';
		document.getElementById('car-3').style.display='none';
		  };
		},3000);
	}