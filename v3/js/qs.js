// JavaScript Document

//////////////////////////
////////////////////////
//Qs

var qs;
/*
var qs={
"q1": { "title": "Carbon Cycle Challenge", "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", }, "q2": { "title": "Coal Power Plant Challenge", "helpTxt" : "Whatever it is... it will go in here. Hopefully it will be small... ", }, "q3": { "title": "Energy Output Challenge", "helpTxt" : "Help for Energy Output Whatever it is... it will go in here. Hopefully it will be small... ", "offset" : 3, }, "q4": { "title": "Alternative Energy Challenge - one of two", "qTxt": "Which of the following best describe what geothermal energy is?", "ans" : ["Geothermal energy is energy obtained from the heat of the sun","Geothermal energy is energy obtained from the heat released by the burning of coal","Geothermal energy is energy obtained from the heat within the Earth's crust","Geothermal energy is energy obtained by heating the Earth's crust"], "helpTxt" : "Help for Alt Energy Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 3 }, "q5": { "title": "Alternative Energy Challenge - two of two", "qTxt": "How is solar energy normally captured?", "ans" : ["Using photovoltaic cells","Using membrane cells","Using photosensitive cells","Using wind turbines"], "helpTxt" : "Help for Alt Energy zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 1 }, "q6": { "title": "Biosphere Challenge - one of two", "qTxt": "Which of the following power sources are NOT considered to be green power?", "ans" : ["Biomass","Wave energy","Hydro-electricity","Nuclear power"], "helpTxt" : "Help for Biosphere zwei von zwei Whatever it is... it will go in here. Hopefully it will be small... ", "offset": 4 }, "q7": { "title": "Biosphere Challenge - two of two", "qTxt": "Extracting and using natural resources found in the Earth can also have detriment effects on the atmosphere and biosphere. What is an essential property of a natural resource?", "ans" : ["It has a low cost and is readily available","It has a useful property that other resources don't have","It is easy to take out of the Earth","They are not readily available"], "helpTxt" : "Help for Biosphere zwei von zweiWhatever it is... it will go in here. Hopefully it will be small... ", "offset": 2 },  
};

*/

function getQs() {
   
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.open("GET", "../resource/6.php?v=3",true);
 xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState==4) {
   //qs=JSON.parse(xmlhttp.responseText);
   eval("qs = {" + xmlhttp.responseText + "}");
   
  }
 }
 xmlhttp.send(null);
	
	}
	
function showQ1() {
	
	document.getElementById('q1').style.display='block';
	document.getElementById('q1').style.opacity=1;
	setTimeout (function(){document.getElementById('q1-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-1');},1000);
	setTimeout (function(){hideDialogue('s1-dia-1'); document.getElementById('q1-splash').style.opacity=0;},8000);
	
		
	setTimeout (function(){
	mkDragables1();
	startRecording();
	listen_1();
	document.getElementById('q1-a').style.display='block';
	var which='q1';
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
		},9000);
	}
	
function showQ2(){
	document.getElementById('q1-a').innerHTML='';
	document.getElementById('q1').style.display='none';
	document.getElementById('q2').style.display='block';
	document.getElementById('q2').style.opacity=1;
	setTimeout (function(){document.getElementById('q2-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-2');},1000);
	setTimeout (function(){hideDialogue('s1-dia-2'); document.getElementById('q2-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
	mkDragables2();
	listen_2();
	startRecording();
	document.getElementById('q2-a').style.display='block';
	var which='q2';
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
		},9000);
	}
	
function showQ3 () {
	document.getElementById('q2-a').innerHTML='';
	document.getElementById('q3').style.display='block';
	document.getElementById('q3').style.opacity=1;
	setTimeout (function(){document.getElementById('q3-splash').style.opacity=1;},100);
	setTimeout (function(){ document.getElementById('q3-splash').style.opacity=0;},3000);
	
	setTimeout (function(){
	//mkDragables3();
	//listen_3();
	document.getElementById('q3-a').style.display='block';
	startRecording();
	var which='q3';
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
	$('input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	
	},2000);
	}
	
function showQ4(){
	document.getElementById('q3-a').innerHTML='';
	document.getElementById('q4').style.display='block';
	document.getElementById('q4').style.opacity=1;
	setTimeout (function(){document.getElementById('q4-splash').style.opacity=1;},100);
	
	setTimeout (function(){showDialogue('s1-dia-4');},1000);
	setTimeout (function(){hideDialogue('s1-dia-4'); document.getElementById('q4-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q4');
		document.getElementById('q4-a').style.display='block';
		startRecording();
		},9000);
	
	}
	
function showQ5(){
	document.getElementById('q4-ans').innerHTML='';
	document.getElementById('q5').style.display='none';
	document.getElementById('q5').style.display='block';
	document.getElementById('q5').style.opacity=1;
	setTimeout (function(){document.getElementById('q5-splash').style.opacity=1;},100);
	setTimeout (function(){ document.getElementById('q5-splash').style.opacity=0;},4500);
	
	setTimeout (function(){
		mkQs('q5');
		document.getElementById('q5-a').style.display='block';
		startRecording();
		},2000);
	}
	
function showQ6(){
	document.getElementById('q5-ans').innerHTML='';
	document.getElementById('q5').style.display='none';
	document.getElementById('q6').style.display='block';
	document.getElementById('q6').style.opacity=1;
	setTimeout (function(){document.getElementById('q6-splash').style.opacity=1;},100);
	setTimeout (function(){showDialogue('s1-dia-6');},1000);
	setTimeout (function(){hideDialogue('s1-dia-6'); document.getElementById('q6-splash').style.opacity=0;},8000);
	
	setTimeout (function(){
		mkQs('q6');
		document.getElementById('q6-a').style.display='block';
		startRecording();
		},9000);
	}
	

function showQ7(){
	document.getElementById('q6-ans').innerHTML='';
	document.getElementById('q7').style.display='none';
	document.getElementById('q7').style.display='block';
	document.getElementById('q7').style.opacity=1;
	setTimeout (function(){document.getElementById('q7-splash').style.opacity=1;},100);
	setTimeout (function(){ document.getElementById('q7-splash').style.opacity=0;},4500);
	
	setTimeout (function(){
		mkQs('q7');
		document.getElementById('q7-a').style.display='block';
		startRecording();
		},2000);
	}
	
function hideQ2() {
	document.getElementById('q2-a').innerHTML='';
	document.getElementById('q2').style.opacity=0;
	setTimeout(function(){document.getElementById('q2').style.display='none';},2000);
	
	}
function hideQ1() {
	document.getElementById('q1-a').innerHTML='';
	document.getElementById('q1').style.opacity=0;
	setTimeout(function(){document.getElementById('q1').style.display='none';},2000);
	
	}
	
function hideQ3() {
	document.getElementById('q3-a').innerHTML='';
	document.getElementById('q3').style.opacity=0;
	setTimeout(function(){document.getElementById('q3').style.display='none';},2000);
	
	}
	
function hideQ4() {
	document.getElementById('q4-ans').innerHTML='';
	document.getElementById('q4').style.opacity=0;
	setTimeout(function(){document.getElementById('q4').style.display='none';},2000);
	
	}
	
function hideQ5() {
	document.getElementById('q5-ans').innerHTML='';
	document.getElementById('q5').style.opacity=0;
	setTimeout(function(){document.getElementById('q5').style.display='none';},2000);
	
	}

function hideQ6() {
	document.getElementById('q6-ans').innerHTML='';
	document.getElementById('q6').style.opacity=0;
	setTimeout(function(){document.getElementById('q6').style.display='none';},2000);
	
	}
function hideQ7() {
	document.getElementById('q7-ans').innerHTML='';
	document.getElementById('q7').style.opacity=0;
	setTimeout(function(){document.getElementById('q7').style.display='none';},2000);
	
	}
function mkQs(which) {
	document.getElementById(which+'-title').innerHTML=qs[which].title;
	document.getElementById(which+'-title-help').innerHTML=qs[which].title;
	document.getElementById(which+'-question').innerHTML=qs[which].qTxt;
	document.getElementById(which+'-helpTxt').innerHTML=qs[which].helpTxt;
	
	var a1=qs[which].ans[0];
	var a2=qs[which].ans[1];
	var a3=qs[which].ans[2];
	var a4=qs[which].ans[3];

	var rads=		  "<input type=\"radio\" name=\"ans\" id=\""+which+"_1\" value=\"ans1\" onclick=\"chkAns('"+which+"','1')\" />  <label for=\""+which+"_1\">"+a1+"</label><div id=\""+which+"_1X\" class='cross blink_continous'>  X  </div><br />";
        
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_2\"value=\"ans2\" onclick=\"chkAns('"+which+"','2')\" />  <label for=\""+which+"_2\">"+a2+"</label><div id=\""+which+"_2X\" class='cross blink_continous'>  X  </div><br />"; 
		
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_3\" value=\"ans3\" onclick=\"chkAns('"+which+"','3')\" />  <label for=\""+which+"_3\">"+a3+"</label><div id=\""+which+"_3X\" class='cross blink_continous'>  X  </div><br />";
		
		rads = rads + "<input type=\"radio\" name=\"ans\" id=\""+which+"_4\" value=\"ans4\" onclick=\"chkAns('"+which+"','4')\" />  <label for=\""+which+"_4\">"+a4+"</label><div id=\""+which+"_4X\" class='cross blink_continous'>  X  </div><br />";       
	
		//console.log(rads);
	document.getElementById(which+'-ans').innerHTML= rads;
	
		$('input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	
	
	}
	
var iShouldWait=false;
function chkAns(qNo, ansNo) {
//	alert(qNo.toString()+":"+ansNo);

if (!iShouldWait){   //the onclick fires like stupids


if (qs[qNo].offset == ansNo) {
	
	playSound('48');
	//sounds[48].play();
	recLap();
	stopRecording();
	var temp =  qNo.replace(/^q+/, "");
	sendResults(3,parseInt(temp),attempt,matrixStr);
	localStorage.setItem('v3-'+qNo,'1');
	}
	
else{
	recLap();
	document.getElementById(qNo+"_"+ansNo+"X").style.display='block';
	setTimeout (function () {document.getElementById(qNo+"_"+ansNo+"X").style.display='none';} , 700);
	//sounds[49].play();
	playSound(49);
	}
	
	iShouldWait=true;
	setTimeout(function(){iShouldWait=false;},300);
	}
	

	}
	
////////////////////
var q1_1=0;
var q1_2=0;
var q1_3=0;
var q1_4=0;
var q1_5=0;
var q1_6=0;
var q1_7=0;
var q1_8=0;
var q1_9=0;
var q1_10=0;
var q1_11=0;


function listen_1() {
	if (q1_1 == 1)
	if (q1_2 == 1){
	if (q1_3 == 1){
	if (q1_4 == 1){
	if (q1_5 == 1){
	if (q1_6 == 1){
	if (q1_7 == 1){
	if (q1_8 == 1){
	if (q1_9 == 1){
	if (q1_10 == 1){
	if (q1_11 == 1){
		
	recLap();
	stopRecording();
	sendResults(3,1,attempt,matrixStr);
		localStorage.setItem('v3-q1','1');
		return;
	}}}}}}}}}}
	

	setTimeout(listen_1,300);
	}
////////////////////
var q2_1=0;
var q2_2=0;
var q2_3=0;
var q2_4=0;
var q2_5=0;
var q2_6=0;
var q2_7=0;
var q2_8=0;
var q2_9=0;
var q2_10=0;


function listen_2() {
	if (q2_1 == 1)
	if (q2_2 == 1){
	if (q2_3 == 1){
	if (q2_4 == 1){
	if (q2_5 == 1){
	if (q2_6 == 1){
	if (q2_7 == 1){
	if (q2_8 == 1){
	if (q2_9 == 1){
	if (q2_10 == 1){
		
		recLap();
		stopRecording();
		sendResults(3,2,attempt,matrixStr);
		localStorage.setItem('v3-q2','1');
		return;
	}}}}}}}}}
	

	setTimeout(listen_2,300);
	}
////////////////////
function mkDragables1() {
	var w = $('#q1-1').width();
	var h = $('#q1-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q1Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q1-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-1').position().top-$('#placeholder-1').parent().position().top;
	var ans_left= $('#placeholder-1').position().left-$('#placeholder-1').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_1=1;
 //sounds[48].play();
 playSound(48);
  snap2grid('q1-1','placeholder-1');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});
//
$( "#q1-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-6').position().top-$('#placeholder-6').parent().position().top;
	var ans_left= $('#placeholder-6').position().left-$('#placeholder-6').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_2=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-2','placeholder-6');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});
//
$( "#q1-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-7').position().top-$('#placeholder-7').parent().position().top;
	var ans_left= $('#placeholder-7').position().left-$('#placeholder-7').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_3=1;
// sounds[48].play();
playSound(48);
  snap2grid('q1-3','placeholder-7');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});
//
$( "#q1-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-10').position().top-$('#placeholder-10').parent().position().top;
	var ans_left= $('#placeholder-10').position().left-$('#placeholder-10').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_4=1;
// sounds[48].play();
playSound(48);
  snap2grid('q1-4','placeholder-10');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});
//
$( "#q1-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-5').position().top-$('#placeholder-5').parent().position().top;
	var ans_left= $('#placeholder-5').position().left-$('#placeholder-5').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_5=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-5','placeholder-5');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

//
$( "#q1-6" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-3').position().top-$('#placeholder-3').parent().position().top;
	var ans_left= $('#placeholder-3').position().left-$('#placeholder-3').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-6" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_6=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-6','placeholder-3');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

//
$( "#q1-7" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-4').position().top-$('#placeholder-4').parent().position().top;
	var ans_left= $('#placeholder-4').position().left-$('#placeholder-4').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-7" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_7=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-7','placeholder-4');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});

$( "#q1-8" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-11').position().top-$('#placeholder-11').parent().position().top;
	var ans_left= $('#placeholder-11').position().left-$('#placeholder-11').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-8" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_8=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-8','placeholder-11');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

$( "#q1-9" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-2').position().top-$('#placeholder-2').parent().position().top;
	var ans_left= $('#placeholder-2').position().left-$('#placeholder-2').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-9" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_9=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-9','placeholder-2');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});

$( "#q1-10" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-9').position().top-$('#placeholder-9').parent().position().top;
	var ans_left= $('#placeholder-9').position().left-$('#placeholder-9').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-10" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_10=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-10','placeholder-9');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

$( "#q1-11" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-8').position().top-$('#placeholder-8').parent().position().top;
	var ans_left= $('#placeholder-8').position().left-$('#placeholder-8').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-11" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_11=1;
//sounds[48].play();
playSound(48);
  snap2grid('q1-11','placeholder-8');
 }
 else {
	 //report
	 //sounds[49].play();
	 playSound(49);
	 }
});
	}

////////////////////
function mkDragables2() {
	var w = $('#q2-1').width();
	var h = $('#q2-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q2Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q2-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-3').position().top-$('#placeholder2-3').parent().position().top;
	var ans_left= $('#placeholder2-3').position().left-$('#placeholder2-3').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_1=1;
// sounds[48].play();
playSound(48);
  snap2grid('q2-1','placeholder2-3');
 }
 else {
	 //report
	 //sounds[49].play();
	 playSound(49);
	 }
});
//
$( "#q2-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-5').position().top-$('#placeholder2-5').parent().position().top;
	var ans_left= $('#placeholder2-5').position().left-$('#placeholder2-5').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_2=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-2','placeholder2-5');
 }
 else {
	 //report
	//sounds[49].play();
	playSound(49);
	 }
});
//
$( "#q2-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-10').position().top-$('#placeholder2-10').parent().position().top;
	var ans_left= $('#placeholder2-10').position().left-$('#placeholder2-10').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_3=1;
// sounds[48].play();
playSound(48);
  snap2grid('q2-3','placeholder2-10');
 }
 else {
	 //report
//	sounds[49].play();
playSound(49);
	 }
});
//
$( "#q2-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-9').position().top-$('#placeholder2-9').parent().position().top;
	var ans_left= $('#placeholder2-9').position().left-$('#placeholder2-9').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_4=1;
// sounds[48].play();
playSound(48);
  snap2grid('q2-4','placeholder2-9');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});
//
$( "#q2-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-1').position().top-$('#placeholder2-1').parent().position().top;
	var ans_left= $('#placeholder2-1').position().left-$('#placeholder2-1').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_5=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-5','placeholder2-1');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

//
$( "#q2-6" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-7').position().top-$('#placeholder2-7').parent().position().top;
	var ans_left= $('#placeholder2-7').position().left-$('#placeholder2-7').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-6" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_6=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-6','placeholder2-7');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

//
$( "#q2-7" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-6').position().top-$('#placeholder2-6').parent().position().top;
	var ans_left= $('#placeholder2-6').position().left-$('#placeholder2-6').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-7" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_7=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-7','placeholder2-6');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

$( "#q2-8" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-8').position().top-$('#placeholder2-8').parent().position().top;
	var ans_left= $('#placeholder2-8').position().left-$('#placeholder2-8').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-8" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_8=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-8','placeholder2-8');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

$( "#q2-9" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-4').position().top-$('#placeholder2-4').parent().position().top;
	var ans_left= $('#placeholder2-4').position().left-$('#placeholder2-4').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-9" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_9=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-9','placeholder2-4');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

$( "#q2-10" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-2').position().top-$('#placeholder2-2').parent().position().top;
	var ans_left= $('#placeholder2-2').position().left-$('#placeholder2-2').parent().position().left;
					
  if(hasColided2(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-10" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_10=1;
//sounds[48].play();
playSound(48);
  snap2grid('q2-10','placeholder2-2');
 }
 else {
	 //report
//	 sounds[49].play();
playSound(49);
	 }
});

	}

////////////////////
	
function hasColided(obj1_left, obj1_top, obj2_left, obj2_top){
	var w = $('#q1-1').width();
	var h = $('#q1-1').height();
	var w2 = ($('#placeholder-1').width());
	var h2= ($('#placeholder-1').height());
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left+30;
    var br = obj2_left+30+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top+20;
    var bb = obj2_top+20+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}
///////////////////////////
function hasColided2(obj1_left, obj1_top, obj2_left, obj2_top){
	var w = $('#q2-1').width();
	var h = $('#q2-1').height();
	var w2 = ($('#placeholder2-1').width());
	var h2= ($('#placeholder2-1').height());
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left;
    var br = obj2_left+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top;
    var bb = obj2_top+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}

