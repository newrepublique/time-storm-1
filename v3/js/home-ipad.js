// JavaScript Document

var viewportwidth;
var viewportheight;
if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
/////////////////////////////////////
function setScreen() {
	if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
	
	var newHt= Math.round((719/1143)*viewportwidth);
	var newTop = (viewportheight - newHt)/2;
	//console.log('new Ht is ' +newHt);
	
	
	if (newHt > (viewportheight+5)) {
		searchForRight(2);
		}
		else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',viewportwidth);
	$('.scene').css('left','0px');
			}
	}
//////////////////////////////////////
function searchForRight(percent) {
		if (typeof window.innerWidth != 'undefined')
 	{
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 	}
	var newWd = viewportwidth-(viewportwidth*percent/100);
	var newHt= Math.round((719/1143)*newWd);
	var newTop = (viewportheight - newHt)/2;
	var newLeft = (viewportwidth - newWd)/2;
	
	if (newHt>viewportheight){
		searchForRight(percent+2)
		}
	else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',newWd);
	$('.scene').css('left',newLeft);
		}
	
	}
/////////////////////////////////////
function gotoScene(which){
	document.getElementById('kala').style.display='block';
	document.getElementById('iframe').style.display='block';
	document.getElementById('iframe').src=which+"?seed="+Math.random();
	}
function hideKala(){
	document.getElementById('kala').style.display='none';
	}
/////////////////////////////////////

/////////////////////////////////////
var checked=true;
function chkPrevious() {

if (localStorage.getItem('lastScene')==null){
	mkLS();
	}
setScreen();
document.getElementById('scene-0').style.display='block';
//showScene ('scene-1');
	}
/////////////////////////
function mkLS() {
	localStorage.setItem('v3-q1','0');
	localStorage.setItem('v3-q2','0');
	localStorage.setItem('v3-q3','0');
	localStorage.setItem('v3-q4','0');
	localStorage.setItem('v3-q5','0');
	localStorage.setItem('v3-q6','0');
	localStorage.setItem('v3-q7','0');
	localStorage.setItem('v3-pangoData','');
	localStorage.setItem('v3-phoneData','');
	localStorage.setItem('secs','900');
	localStorage.setItem('mins','15');
	localStorage.setItem('lastScene','scene-2.html');
	localStorage.setItem('nick','Stewie Griffin');
	localStorage.setItem('v3-hasClearedTip','0');
	localStorage.setItem('v3-countdown','off');
	localStorage.setItem('v3-completed','000');

	}
function destroyLS(){
	localStorage.removeItem('lastScene');
	}
/////////////////////////
function storePhoneData(what) {
	var currentData=localStorage.getItem('v3-phoneData');
	currentData=currentData+what;
	localStorage.setItem('v3-phoneData',currentData);
	return currentData;
	}
////////////////////////
function storePangoData(what) {
	var currentData=localStorage.getItem('v3-pangoData');
	currentData=currentData+what;
	localStorage.setItem('v3-pangoData',currentData);
	return currentData;
	}
function storeTimer(_sec,_min){
	localStorage.setItem('secs',_sec.toString());
	localStorage.setItem('mins',_min.toString());
	}



//////////////////////////
var filesArray = Array (126);
var sounds = Array (53);
	var sndArray = Array(52);
var snd = new Audio;
	

function preload() {
	
	gender=localStorage.getItem('gender');
	if (!checked) return;
	
	document.getElementById('loading').style.display='block';
	
	var path= 'snd/';
	var ext='.ogg';
	if(	BrowserDetect.browser == 'Safari') ext='.mp3';
	
	var imgArray= Array(74);

	//filling in the sndArray
	sndArray = [
	path+'sfx/01_desert_wind'+ext,
	path+'sfx/02_desert_wind_tip'+ext,
	path+'sfx/03_desert_wind_dogs'+ext,
	path+'sfx/04_savage_dogs'+ext,
	path+'sfx/05_heavy_breathing'+ext,
	path+'sfx/07_siren'+ext,
	path+'char/02_hi_there'+ext,
	path+'char/04_youre_not_from_around_here'+ext,
	path+'char/06_my_names_max'+ext,
	path+'char/08_doesnt_look_good_does_it'+ext,
	path+'char/09_this_was_a_vibrant_city'+ext,
	path+'char/10_deep_down_most_people_knew'+ext,
	path+'char/11_well_it_didnt_happen_overnight'+ext,
	path+'char/13_and_the_rest'+ext,
	path+'char/17_oil_and_coal_were_a_big_part'+ext,
	path+'char/18_it_wasnt_long'+ext,
	path+'char/20_have_you_heard_of_the_dome'+ext,
	path+'char/22_so_what_do_you_know'+ext,
	path+'char/24_okay_your_choice'+ext,
	path+'char/25_hi_again_good_nights_sleep'+ext,
	path+'char/27_i_knew_you_come_around'+ext,
	path+'char/29_smart_move'+ext,
	path+'char/30_while_the_rest_of_the_world'+ext,
	path+'char/31_while_the_rest_of_the_world'+ext,
	path+'char/32_with_a_closed_environment'+ext,
	path+'char/33_and_in_perfect_balance'+ext,
	path+'char/34_welcome_to_the_dome'+ext,
	path+'char/36_directly_under_your_feet'+ext,
	path+'char/37_so_while_we_have_sealed'+ext,
	path+'char/39_well_theres_been_a_few'+ext,
	path+'char/41_dont_worry_its_nothing'+ext,
	path+gender+'/01_from_the_middle_of_nowhere'+ext,
	path+gender+'/03_youre_the_first_person'+ext,
	path+gender+'/05_guess_its_not_hard'+ext,
	path+gender+'/07_hi_max'+ext,
	path+gender+'/10_what_do_you_mean_disaster'+ext,
	path+gender+'/12_thats_why_desal'+ext,
	path+gender+'/14_that_explains_it'+ext,
	path+gender+'/16_so_what_did_you_do'+ext,
	path+gender+'/19_so_how_are_you_living_now'+ext,
	path+gender+'/21_errr_yeah_yes'+ext,
	path+gender+'/23_ive_been_told_not_to'+ext,
	path+gender+'/26_all_right_take_me'+ext,
	path+gender+'/28_ive_heard_its_lovely'+ext,
	path+gender+'/35_huh_where'+ext, //44
	path+gender+'/38_what_do_you_mean'+ext, //45
	path+gender+'/40_whats_that_sound'+ext, //46
	path+gender+'/42_who_me'+ext,
	path+'game/016_peep_01'+ext,
	path+'game/017_peep_02'+ext,
	path+'game/018_tool_tip_01'+ext,
	path+'game/019_map_green_dot_beep_01'+ext,
	];
	//filling in images
	path='imgs/';
	imgArray = [
	path+'bg1.jpg',
	path+'bg2.jpg',
	path+'bg3.jpg',
	path+'bg4.jpg',
	path+'bg5.jpg',
	path+'bg6.jpg',
	path+'bg7.jpg',
	path+'bg8.jpg',
	path+'bg9.jpg',
	path+'bg10.jpg',
	path+'bg11.jpg',
	path+'bg12.jpg',
	path+'bg13.jpg',
	path+'car-1.png',
	path+'car-2.png',
	path+'car-3.png',
	path+'door-1.png',
	path+'door-2.png',
	path+'door-3.png',
	path+'fracture.png',
	path+'fracture1.png',
	path+'frame.png',
	path+'obj-1.png',
	path+'obj-2.png',
	path+'obj-3.png',
	path+'obj-4.png',
	path+'obj-5.png',
	path+'obj-6.png',
	path+'obj-7.png',
	path+'obj-8.png',
	path+'obj-9.png',
	path+'obj-10.png',
	path+'obj-11.png',
	path+'obj-12.png',//new
	path+'obj-13.png',
	path+'obj-14.png',
	path+'pangoUpdates.png',
	path+'phone.png',
	path+'phone-2.png',
	path+'phone-3.png',
	path+'phone-4.png',
	path+'phone-5.png',
	path+'phone-6.png',
	path+'welldone.png',
	path+'q1/q1.png',
	path+'q1/ans1.png',
	path+'q1/ans2.png',
	path+'q1/ans3.png',
	path+'q1/ans4.png',
	path+'q1/ans5.png',
	path+'q1/ans6.png',
	path+'q1/ans7.png',
	path+'q1/ans8.png',
	path+'q1/ans9.png',
	path+'q1/ans10.png',
	path+'q1/ans11.png',
	path+'q2/q2.png',
	path+'q2/ans1.png',
	path+'q2/ans2.png',
	path+'q2/ans3.png',
	path+'q2/ans4.png',
	path+'q2/ans5.png',
	path+'q2/ans6.png',
	path+'q2/ans7.png',
	path+'q2/ans8.png',
	path+'q2/ans9.png',
	path+'q2/ans10.png',
	path+'q3/q3.png',
	path+'help_box.png',
	path+'question_box.png',
	path+'save-1.png',
	path+'save-2.png',
	path+'mute-1.png',
	path+'mute-2.png',
	];
	
	
	//loading sounds
	for (i=0; i< 52; i++ ) {
		filesArray[i] = sndArray[i];
		//console.log(i+ ' loading '+filesArray[i]);
		sounds[i]= loadSound (filesArray[i]) ;
		}
	//loading imgs
	for (i=52; i<126; i++){
		filesArray[i] = imgArray[i-52];
		//console.log(i+ ' loading '+filesArray[i]);
		loadImg (filesArray[i]);
		}
		

	}

	
function loadImg(which){
	var img = new Image();
	img.src = which;
	img.onload= function (){ 
	rm4Array(which);
	}
	}

function loadSound(which){
	//var snd = new Audio();
	//snd.src = which;
	
		rm4Array(which);

	
		return snd;
	}
	
function rm4Array(what){
	for (i=0; i<filesArray.length;i++){
		if(filesArray[i]==what){
			filesArray.splice(i,1);
			//console.log('removing '+what+' left with '+filesArray.length);
			document.getElementById('loadingPercent').innerHTML=Math.floor((126-filesArray.length)*100/126);
			}
				if (filesArray.length<53) {
					init ();
					//alert('loaded');
					
					}
		}

	}

///////////////////////
var inited=false;
function init() {
	
	if(inited) return;
	
	inited=true;
		getQs();
		getPango();
		
			
			document.getElementById('scene-1').style.display='none';
			var sc=localStorage.getItem('lastScene');
			gotoScene(sc);
			
	}
	
///////////////////////////
function sendResults (vig,qno,attempt,matrix) {
	var user = localStorage.getItem('key');
	//alert(user);
	//console.log(qno);
	$.post("../resource/2.php", { v: vig, q: qno, a: attempt, m: matrix, u: user } );
	}

function markComplete(){
	$.post("../resource/3.php", { v: 3, u: localStorage.getItem('key') } );
	}
function revert() {
	window.location='../index.php?userId='+localStorage.getItem('key');
	}
	
function getPango() {
	$.get('../resource/5.php',{ v: 3, u: localStorage.getItem('key') },  function(data) {
		var results = JSON.parse(data);
		var str='';
		
		for (i = 0; i < results.Feeds.length; i++) {

                    f = results.Feeds[i];
					
					str=str+'<br><a class="glow" target="_blank" href="http://www.pango.edu.au/DiscussionDetails/?id=' + f.CategoryId + '">'+
					f.TopicTitle +"</a><br>";
		}
		localStorage.setItem('v3-pangoData',str);
	});
	}

	
//////////////////////////
function playSound(which) {
if(!soundsMuted) {snd.volume=1;}
	snd.src=sndArray [which];
	snd.load();
	snd.play();
	}
	
function pauseSound(which){
	snd.pause();
	}
	
var soundsMuted=false;
function muteSounds() {
	
	if (!soundsMuted){
	for (i=0; i<sounds.length; i++) {
		snd.volume=0;
		}
		soundsMuted=true;
	}
	else {
		for (i=0; i<sounds.length; i++) {
		snd.volume=1;
		}
		
		soundsMuted=false;
		}
}

function decreaseBGSound() {
	//sounds[1].volume=0.2;
	}
	
function tempSndPlay(){
	var ext='.mp3';
	var path='snd/';
	snd= new Audio;
	snd.src=path+'sfx/07_siren'+ext;
	snd.play();
	setTimeout(function() {snd.pause();}, 100);
	snd.volume=0.01;
	}
	
function lowerBgSound(){
			snd.volume=0.2;
	}
function increaseBgSound(){
		snd.volume=0.4;
	}
	
function saveGame() {
	
	}
/////////////////////
//////////////////////
//////Helper Functions

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

///////////////////
function getTransformProperty(element) {
    // Note that in some versions of IE9 it is critical that
    // msTransform appear in this list before MozTransform
    var properties = [
        'transform',
        'WebkitTransform',
        'msTransform',
        'MozTransform',
        'OTransform'
    ];
    var p;
    while (p = properties.shift()) {
        if (typeof element.style[p] != 'undefined') {
            return p;
        }
    }
    return false;
}
//////////////////
function randomXToY(minVal,maxVal,floatVal)
{
  var randVal = minVal+(Math.random()*(maxVal-minVal));
  return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
}
/////////////////////////

