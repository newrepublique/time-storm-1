// JavaScript Document

function scene3() {
	//window.parent.playSound(2);
	var iDelay=1000;
	
	showScene('scene-3');
	updatePangoTxt();
	updatePhoneTxt(3,'You are in the Cold Temperate Zone.');
	updateCompleted(25);
	window.parent.playSound(7);
	
	
	if (localStorage.getItem('v5-q6')=='1'){
		document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg8.jpg)'; 
		document.getElementById('s3-continaer').style.opacity=1;
		document.getElementById('timer').style.display='block';
		countdown();
		gotoD();
		return;
		}
	
	if (localStorage.getItem('v5-q5')=='1'){
		document.getElementById('timer').style.display='block';
		countdown();
		gotoC();
		return;
		}
	
	if (localStorage.getItem('v5-q4')=='1'){
		document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
		document.getElementById('s3-continaer').style.opacity=1;
		document.getElementById('timer').style.display='block';
		countdown();
		gotoB();
		return;
		}
	
	mins = 15;
   secs = 15 * 60;
  	localStorage.setItem('mins',mins.toString()+"");
	localStorage.setItem('secs',secs.toString()+"");
	
	setTimeout(function() { document.getElementById('s3-continaer').style.opacity=1; }, iDelay);
		//refreshPhoneTxt();
	
	setTimeout (function () {showDialogue('s3-dia-1');}, 1000 + iDelay);
	setTimeout (function () {hideDialogue('s3-dia-1');}, 4000 + iDelay);
	
	setTimeout (function () {showDialogue('s3-dia-2');}, 5000 + iDelay);
	setTimeout (function () {hideDialogue('s3-dia-2');}, 10000 + iDelay);
	
	setTimeout (function() {
		document.getElementById('boat').style.display='block';
		window.parent.playSound(100);
		}, 11000+iDelay);
	
	
}


//////////////////////////////////
var boatTurn = 0;

function boatClicked() {
	boatTurn++;
	var iDelay=1000;
	
	document.getElementById('boat').style.display='none';
	
	if (boatTurn==1){
		
		setTimeout(function() {
			
			},iDelay);
			
		setTimeout(function() {
			window.parent.playSound(5);//this plays for 10 seconds
			document.getElementById('s3-continaer').style.opacity=0;
			}, 1000+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
			}, 3000+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=1;
			}, 5000+iDelay);
			
		setTimeout(function() {
			showDialogue('s3-dia-3');
			window.parent.playSound(92);
			}, 6000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-3');
			}, 9000+iDelay);
			
		setTimeout(function() {
			showDialogue('s3-dia-4');
			window.parent.playSound(69);
			}, 10000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-4');
			}, 23000+iDelay);
			
		
		setTimeout(function() {
		document.getElementById('timer').style.display='block';
		countdown();
		window.parent.playSound(100);
		document.getElementById('boat').style.display='block';
			}, 24000+iDelay);
		
		}
		
	if(boatTurn == 2){
		
		
		setTimeout(function() {
			showDialogue('s3-dia-5');
			}, 1000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-5');
			window.parent.playSound(6);
			}, 3000+iDelay);
		
		setTimeout(function() {
			showDialogue('s3-dia-6');
			window.parent.playSound(24);
			}, 10000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-6');
			}, 17000+iDelay);
		
		setTimeout(function() {
			showQ4();
			}, 18000+iDelay);
		}
	
	}
	
/////////////////////////////////////////////////
function showQ4() {
	var iDelay=0;
	
	setTimeout(function() {
		document.getElementById('q4').style.display='block';
		document.getElementById('q4').style.opacity=1;
		} , iDelay);
		
	setTimeout(function() {
		document.getElementById('q4-a').style.display='block';
		window.parent.playSound(100);
	
		$('input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	
		} , 1000+iDelay);
	
	}
///////////////////////////////////////////////
function markQ4(score){
	
	window.parent.playSound(98);
	
	window.parent.sendResults (4,score) ;
		localStorage.setItem('v5-q4','1');
		
	setTimeout(function () {document.getElementById('q4').style.opacity=0;document.getElementById('q4-a').style.display='none';} , 200);
	
	setTimeout(function () {document.getElementById('q4').style.display='none';} , 1000);
	
	gotoB();
	}
	
//////////////////////////////////////////////
function gotoB() {
	var iDelay=0;
	
	setTimeout ( function() {
		showDialogue('s3-dia-7');
			window.parent.playSound(70);
		}, 1000+iDelay);
		
	setTimeout ( function() {
		hideDialogue('s3-dia-7');
		}, 9500+iDelay);
		
	setTimeout ( function() {
		showDialogue('s3-dia-8');
		window.parent.playSound(40);
		}, 10000+iDelay);
		
	setTimeout ( function() {
		hideDialogue('s3-dia-8');
		}, 18000+iDelay);
		
	setTimeout (function () {
		showQ5();
		} , 19000+iDelay);
	}
///////////////////////////////////////////////
function showQ5(){
		var iDelay=0;
	
	setTimeout(function() {
		document.getElementById('q5').style.display='block';
		document.getElementById('q5').style.opacity=1;
		} , iDelay);
		
	setTimeout(function() {
		document.getElementById('q5-a').style.display='block';
		window.parent.playSound(100);
	
		} , 1000+iDelay);

	
	}
////////////////////////////////////////////////////
function markQ5(score){
	
	window.parent.playSound(98);
	
	window.parent.sendResults (5,score) ;
		localStorage.setItem('v5-q5','1');
		
	setTimeout(function () {document.getElementById('q5').style.opacity=0;document.getElementById('q5-a').style.display='none';} , 200);
	
	setTimeout(function () {document.getElementById('q5').style.display='none';} , 1000);
	
	setTimeout(function () {gotoC();} , 2000);
	
	}
//////////////////////////////////////////////////
function gotoC() {
	var iDelay = 0;
	
	window.parent.pauseSound(7);
	window.parent.playSound(9);
	
	setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=0;
			}, 1000+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg8.jpg)';
			}, 1500+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=1;
			}, 2000+iDelay);
	
	setTimeout(function() {
			showDialogue('s3-dia-9');
			window.parent.playSound(41);
			}, 3000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-9');
			document.getElementById('s3-continaer').style.opacity=0;
			}, 7000+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
			}, 7500+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=1;
			}, 8000+iDelay);
			
		setTimeout(function() {
			showDialogue('s3-dia-10');
			}, 9000+iDelay);
	
	setTimeout(function() {
			hideDialogue('s3-dia-10');
			}, 13000+iDelay);
			
	setTimeout(function() {
			showDialogue('s3-dia-10b');
			}, 13000+iDelay);
			
	setTimeout(function() {
			document.getElementById('t1').style.display='block';
			document.getElementById('t2').style.display='block';
			document.getElementById('t3').style.display='block';
			document.getElementById('t4').style.display='block';
			document.getElementById('t5').style.display='block';
			document.getElementById('t6').style.display='block';
			document.getElementById('t7').style.display='block';
			document.getElementById('t8').style.display='block';
			
			
			}, 14000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-10b');
			}, 18000+iDelay);
	}
	
/////////////////////////////////
var selected=0;
function markObj(which) {
	
	selected++;
	document.getElementById(which).style.display='none';
	window.parent.playSound(98);
	
	if(selected==8) {
		
		
		setTimeout(function () {
			showObj('door1-obj');
			}, 1000);
		
		}
	}
	
	
////////////////////////////////////
function gotoD() {
	
	var iDelay=1000;
	
	document.getElementById('blacky').style.opacity=0;
		
	setTimeout(function(){document.getElementById('blacky').style.display='none';},iDelay);
	
	setTimeout(function() {
		showDialogue('s3-dia-12');
		window.parent.playSound(54);
		}, 1000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s3-dia-12');
		}, 9000+iDelay);
		
	setTimeout(function() {
		showDialogue('s3-dia-13');
		window.parent.playSound(60);
		}, 10000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s3-dia-13');
		}, 16000+iDelay);
		
	setTimeout(function() {
		showQ6();
		}, 17000+iDelay);
	
	}

///////////////////////////////////////////////
function showQ6(){
		var iDelay=0;
	
	setTimeout(function() {
		document.getElementById('q6').style.display='block';
		document.getElementById('q6').style.opacity=1;
		} , iDelay);
		
	setTimeout(function() {
		document.getElementById('q6-a').style.display='block';
		window.parent.playSound(100);
	
		} , 1000+iDelay);

	
	}
////////////////////////////////////////////////////
var attempt=0;
function markQ6(score){
	
	 attempt++;
	 if(attempt==1) window.parent.sendResults (6,score) ;
	
	window.parent.playSound(98);
	
	setTimeout(function () {document.getElementById('q6').style.opacity=0;document.getElementById('q6-a').style.display='none';} , 200);
	
	setTimeout(function () {document.getElementById('q6').style.display='none';} , 1000);
	
	if(score==1){
	localStorage.setItem('v5-q6','1');
	setTimeout(function () {gotoE();} , 1000);
	}
	
	if(score==0){
		
		var iDelay=1000;
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=0;
			}, 1000+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg9.jpg)';
			}, 1500+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=1;
			}, 1800+iDelay);
	
	setTimeout(function() {
			showDialogue('s3-dia-14');
			window.parent.playSound(55);
			}, 2100+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-14');
			document.getElementById('s3-continaer').style.opacity=0;
			}, 6100+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
			}, 7400+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=1;
			}, 7500+iDelay);
			
		
		setTimeout(function() {
			showQ6();
			}, 7500+iDelay);
		}
	
	}
	
/////////////////////////////////
function gotoE() {
	
	var iDelay=0;
	
	setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=0;
			}, 1000+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.backgroundImage='url(imgs/bg10.jpg)';
			}, 1500+iDelay);
			
		setTimeout(function() {
			document.getElementById('s3-continaer').style.opacity=1;
			}, 2000+iDelay);
			
			//iDelay= iDelay+4000;
	
	setTimeout(function() {
			showDialogue('s3-dia-15');
			window.parent.playSound(56);
			}, 3000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-15');
			updateCompleted(50);
			}, 8000+iDelay);
			
			iDelay=iDelay+2000;
		setTimeout(function() {
			window.parent.pauseSound(9);
			window.parent.pauseSound(7);
			window.parent.playSound(11);
			document.getElementById('s3-continaer').style.opacity=0;
			timerPaused=true;
			}, 9000+iDelay);
	
		setTimeout(function() {
			showDialogue('s3-dia-16');
			}, 9000+iDelay);
			
		setTimeout(function() {
			hideDialogue('s3-dia-16');
			}, 17000+iDelay);
			
		setTimeout(function() {
			window.parent.gotoScene('scene-4.html');
			}, 18000+iDelay);
	}
/////////////////////////////////

function showObj(which) {
	
	$('.obj').css('display','none');
	
	document.getElementById('blacky').style.display='block';
		document.getElementById('blacky').style.opacity=1;
		
		setTimeout(function() {
			document.getElementById(which).style.display='block';
			},1000);
	}