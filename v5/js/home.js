// JavaScript Document

var viewportwidth;
var viewportheight;
if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
/////////////////////////////////////
function setScreen() {
	if (typeof window.innerWidth != 'undefined')
 {
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 }
	
	var newHt= Math.round((719/1143)*viewportwidth);
	var newTop = (viewportheight - newHt)/2;
	//console.log('new Ht is ' +newHt);
	
	
	if (newHt > (viewportheight+5)) {
		searchForRight(2);
		}
		else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',viewportwidth);
	$('.scene').css('left','0px');
			}
	}
//////////////////////////////////////
function searchForRight(percent) {
		if (typeof window.innerWidth != 'undefined')
 	{
       viewportwidth = window.innerWidth;
       viewportheight = window.innerHeight;
 	}
	var newWd = viewportwidth-(viewportwidth*percent/100);
	var newHt= Math.round((719/1143)*newWd);
	var newTop = (viewportheight - newHt)/2;
	var newLeft = (viewportwidth - newWd)/2;
	
	if (newHt>viewportheight){
		searchForRight(percent+2)
		}
	else{
	$('.scene').css('height',newHt);
	$('.scene').css('top',newTop);
	$('.scene').css('width',newWd);
	$('.scene').css('left',newLeft);
		}
	
	}
/////////////////////////////////////
function gotoScene(which){
	document.getElementById('kala').style.display='block';
	document.getElementById('iframe').style.display='block';
	document.getElementById('iframe').src=which+"?seed="+Math.random();
	}
function hideKala(){
	document.getElementById('kala').style.display='none';
	}
/////////////////////////////////////

/////////////////////////////////////
var checked=true;
function chkPrevious() {

if (localStorage.getItem('lastScene')==null){
	mkLS();
	}
setScreen();
document.getElementById('scene-0').style.display='block';
//showScene ('scene-1');
	}
/////////////////////////
function mkLS() {
	localStorage.setItem('v5-q1','0');
	localStorage.setItem('v5-q2','0');
	localStorage.setItem('v5-q3','0');
	localStorage.setItem('v5-q4','0');
	localStorage.setItem('v5-q5','0');
	localStorage.setItem('v5-q6','0');
	localStorage.setItem('v5-q7','0');
	localStorage.setItem('v5-q8','0');
	localStorage.setItem('v5-q9','0');
	localStorage.setItem('v5-q10','0');
	localStorage.setItem('v5-pangoData','');
	localStorage.setItem('v5-phoneData','');
	localStorage.setItem('secs','900');
	localStorage.setItem('mins','15');
	localStorage.setItem('lastScene','scene-2.html');
	localStorage.setItem('nick','Stewie Griffin');
	localStorage.setItem('v5-hasCollectedSoil','0');
	localStorage.setItem('v5-countdown','off');
	localStorage.setItem('v5-completed','000');

	}
function destroyLS(){
	localStorage.removeItem('lastScene');
	}
/////////////////////////
function storePhoneData(what) {
	var currentData=localStorage.getItem('v5-phoneData');
	currentData=currentData+what;
	localStorage.setItem('v5-phoneData',currentData);
	return currentData;
	}
////////////////////////
function storePangoData(what) {
	var currentData=localStorage.getItem('v5-pangoData');
	currentData=currentData+what;
	localStorage.setItem('v5-pangoData',currentData);
	return currentData;
	}
function storeTimer(_sec,_min){
	localStorage.setItem('secs',_sec.toString());
	localStorage.setItem('mins',_min.toString());
	}


//////////////////////////
var filesArray = Array (168);
var sounds = Array (103);	

function preload() {
	
	gender=localStorage.getItem('gender');
	if (!checked) return;
	
	document.getElementById('loading').style.display='block';
	
	var path= 'snd/';
	var ext='.ogg';
	if(	BrowserDetect.browser == 'Safari') ext='.mp3';
	
	var imgArray= Array(65);
	var sndArray = Array(103);
	//filling in the sndArray
	sndArray = [
	path+'sfx/01_siren'+ext, //0
	path+'sfx/02_lift_and_alarm'+ext, //1
	path+'sfx/03_steel_doors'+ext, //2
	path+'sfx/04_woof_woof'+ext, //3
	path+'sfx/05_sparks_and_run'+ext, //4
	path+'sfx/06_crash_and_pipe_burst'+ext, //5
	path+'sfx/07_barking_and_running away'+ext,//6
	path+'sfx/08_river'+ext,//7
	path+'sfx/09_splintering_wood'+ext,//8
	path+'sfx/10_bigger_river'+ext,//9
	path+'sfx/11_groaning'+ext,//10
	path+'sfx/12_walking_on_twigs'+ext,//11
	path+'sfx/13_forest_with_gas_escaping'+ext,//12
	path+'sfx/14_broken_hydraulics'+ext,//13
	path+'sfx/15_dog_growl'+ext,//14
	path+'sfx/16_dog_barks'+ext,//15
	path+'sfx/17_drop_stick'+ext,//16
	path+'sfx/18_bravo'+ext,//17
	path+'sfx/19_hatchway_slide'+ext,//18
	path+'sfx/20_cave_atmos_loopable'+ext,//19
	path+'sfx/21_rockfall'+ext,//20
	path+'sfx/22_wind_and_creaking_wood'+ext,//21
	path+'sfx/23_thump_and_break'+ext,//22
	path+'char/doug_01'+ext,//23
	path+'char/doug_02'+ext,//24
	path+'char/doug_03'+ext,//25
	path+'char/doug_04'+ext,//26
	path+'char/doug_05'+ext,//27
	path+'char/doug_06'+ext,//28
	path+'char/doug_07'+ext,//29
	path+'char/doug_08'+ext,//30
	path+'char/doug_09'+ext,//31
	path+'char/doug_10'+ext,//32
	path+'char/go_doug_01'+ext,//33
	path+'char/go_doug_02'+ext,//34
	path+'char/max_36_all_this_'+ext,//35
	path+'char/max_37_i_dont_think_'+ext,//36
	path+'char/max_38_whatever_'+ext,//37
	path+'char/max_39_that_doesnt_'+ext,//38
	path+'char/max_40_looks_like_'+ext,//39
	path+'char/max_41_yes_the_dog_'+ext,//40
	path+'char/max_42_darn_'+ext,//41
	path+'char/max_43_theres_only_'+ext,//42
	path+'char/max_44_i_was_trying_'+ext,//43
	path+'char/max_45_little_dorian_'+ext,//44
	path+'char/max_46_come_on_kid_'+ext,//45
	path+'char/max_47_youre_telling_me_'+ext,//46
	path+'char/max_48_fine_thanks_'+ext,//47
	path+'char/max_49_look_'+ext,//48
	path+'char/max_50_mineshaft_'+ext,//49
	path+'char/max_51_take_me_'+ext,//50
	path+'char/max_52_well_'+ext,//51
	path+'char/max_100'+ext,//52
	path+'char/mr_edwards_01'+ext,//53
	path+'char/mr_edwards_02'+ext,//54
	path+'char/mr_edwards_03'+ext,//55
	path+'char/mr_edwards_04'+ext,//56
	path+'char/mr_edwards_05'+ext,//57
	path+'char/mr_edwards_06'+ext,//58
	path+'char/mre_edwards_01'+ext,//59
	path+'char/mre_edwards_02'+ext,//60
	path+'char/mre_edwards_03'+ext,//61
	path+'char/mre_edwards_04'+ext,//62
	path+'char/mre_edwards_05'+ext,//63
	path+'char/mre_edwards_06'+ext,//64
	path+'char/mre_edwards_07'+ext,//65
	path+'char/sandinsky_01'+ext,//66
	path+'char/sandinsky_02'+ext,//67
	path+'char/sandinsky_02b'+ext,//68
	path+'char/sandinsky_03'+ext,//69
	path+'char/sandinsky_04'+ext,//70
	path+'char/sandinsky_05'+ext,//71
	path+'char/sandinsky_06'+ext,//72
	path+'char/sandinsky_07'+ext,//73
	path+'char/sandinsky_08'+ext,//74
	path+'char/sandinsky_09'+ext,//75
	path+'char/sandinsky_10'+ext,//76
	path+'char/sandinsky_11'+ext,//77
	path+'char/sandinsky_12'+ext,//78
	path+'char/sandinsky_13'+ext,//79
	path+'char/tyres_01'+ext,//80
	path+'char/tyres_02'+ext,//81
	path+'char/tyres_03'+ext,//82
	path+'char/tyres_04'+ext,//83
	path+'char/tyres_05'+ext,//84
	path+'char/tyres_06'+ext,//85
	path+'char/tyres_07'+ext,//86
	path+'char/tyres_08'+ext,//87
	path+'char/well_eductated_female_01'+ext,//88
	path+'char/well_eductated_female_02'+ext,//89
	path+'char/well_eductated_female_03'+ext,//90
	path+gender+'/'+gender+'_user_52'+ext,//91
	path+gender+'/'+gender+'_user_53'+ext,//92
	path+gender+'/'+gender+'_user_54'+ext,//93
	path+gender+'/'+gender+'_user_55'+ext,//94
	path+gender+'/'+gender+'_user_56'+ext,//95
	path+gender+'/'+gender+'_user_57'+ext,//96
	path+gender+'/'+gender+'_user_58'+ext,//97
	path+'game/016_peep_01'+ext,//98
	path+'game/017_peep_02'+ext,//99
	path+'game/018_tool_tip_01'+ext,//100
	path+'game/019_map_green_dot_beep_01'+ext,//101
	path+'sfx/01_white_light_vortex_01'+ext, //102
	];
	//filling in images
	path='imgs/';
	imgArray = [
	path+'bg1.jpg',
	path+'bg2.jpg',
	path+'bg3.jpg',
	path+'bg4.jpg',
	path+'bg5.jpg',
	path+'bg6.jpg',
	path+'bg7.jpg',
	path+'bg8.jpg',
	path+'bg9.jpg',
	path+'bg10.jpg',
	path+'bg11.jpg',
	path+'bg12.jpg',
	path+'bg13.jpg',
	path+'bg14.jpg',
	path+'bg15.jpg',
	path+'bg16.jpg',
	path+'bg17.jpg',
	path+'bg18.jpg',
	path+'bg19.jpg',
	path+'bg20.jpg',
	path+'bg21.jpg',
	path+'bg22.jpg',
	path+'bg23.jpg',
	path+'boat.png',
	path+'fracture.png',
	path+'fracture1.png',
	path+'frame.png',
	path+'obj-1.png',
	path+'obj-2.png',
	path+'obj-3.png',
	path+'pangoUpdates.png',
	path+'phone-6.png',
	path+'phone-3.png',
	path+'phone-5.png',
	path+'q3/q3.png',
	path+'q3/dorian-1.png',
	path+'q3/dorian-2.png',
	path+'q3/doug-1.png',
	path+'q3/doug-2.png',
	path+'q3/ed-1.png',
	path+'q3/ed-2.png',
	path+'q3/finished-txt.png',
	path+'q3/fr-txt.png',
	path+'q3/med-1.png',
	path+'q3/med-2.png',
	path+'q3/san-1.png',
	path+'q3/san-2.png',
	path+'q3/tyres-1.png',
	path+'q3/tyres-2.png',
	path+'help_box.png',
	path+'question_box.png',
	path+'save-1.png',
	path+'save-2.png',
	path+'mute-1.png',
	path+'mute-2.png',
	path+'kp-1.png',
	path+'t1.png',
	path+'t2.png',
	path+'t3.png',
	path+'t4.png',
	path+'t5.png',
	path+'t6.png',
	path+'t7.png',
	path+'t8.png',
	path+'vortex.jpg',
	];
	
	
	//loading sounds
	for (i=0; i< 103; i++ ) {
		filesArray[i] = sndArray[i];
		//console.log(i+ ' loading '+filesArray[i]);
		sounds[i]= loadSound (filesArray[i]) ;
		}
	//loading imgs
	for (i=103; i<168; i++){
		filesArray[i] = imgArray[i-103];
		//console.log(i+ ' loading '+filesArray[i]);
		loadImg (filesArray[i]);
		}
		
	}

	
function loadImg(which){
	var img = new Image();
	img.src = which;
	img.onload= function (){ 
	rm4Array(which);
	}
	}

function loadSound(which){
	var snd = new Audio();
	snd.src = which;
	
	snd.addEventListener('canplaythrough',function () {
		rm4Array(which);
		} );
	snd.onerror =function(e){//document.getElementById('temp').innerHTML=snd.src+' gave error';
	console.log(snd.src+' gave an error');
	}
		return snd;
	}
	
function rm4Array(what){
	for (i=0; i<filesArray.length;i++){
		if(filesArray[i]==what){
			filesArray.splice(i,1);
			//console.log('removing '+what+' left with '+filesArray.length);
			document.getElementById('loadingPercent').innerHTML=Math.floor((168-filesArray.length)*100/168);
			}
				if (filesArray.length<1) {
					init ();
					//alert('loaded');
					
					}
		}

	}

///////////////////////
function init() {
	
		getPango();
		
		//puting sounds in loop
			sounds[0].addEventListener('ended', function() {
    		this.currentTime = 0;
    		this.play();
			}, false);
			
			sounds[7].addEventListener('ended', function() {
    		this.currentTime = 0;
    		this.play();
			}, false);
		
			sounds[9].addEventListener('ended', function() {
    		this.currentTime = 0;
    		this.play();
			}, false);
			
			sounds[12].addEventListener('ended', function() {
    		this.currentTime = 0;
    		this.play();
			}, false);
			
			sounds[19].addEventListener('ended', function() {
    		this.currentTime = 0;
    		this.play();
			}, false);
			
			sounds[21].addEventListener('ended', function() {
    		this.currentTime = 0;
    		this.play();
			}, false);
		
			
			sounds[0].volume=0.3;
			sounds[7].volume=0.5;
			sounds[9].volume=0.5;
			sounds[12].volume=0.6;
			sounds[19].volume=0.5;
			sounds[21].volume=0.5;
			sounds[99].volume=0.6;
			
			document.getElementById('scene-1').style.display='none';
			var sc=localStorage.getItem('lastScene');
			gotoScene(sc);
			
	}
	
///////////////////////////
var sendR = true;

function sendResults (qno,sc) {
if(!sendR) return;

sendR=false;
	var user = localStorage.getItem('key');
	$.post("../resource/7.php", { q: qno, s: sc, u: user } );
setTimeout(function(){sendR=true;},400);
	}

function markComplete(){
	$.post("../resource/3.php", { v: 5, u: localStorage.getItem('key') } );
	}
function revert() {
	window.location='../index.php?userId='+localStorage.getItem('key');
	}
	
function getPango() {
	$.get('../resource/5.php',{ v: 5, u: localStorage.getItem('key') },  function(data) {
		var results = JSON.parse(data);
		var str='';
		
		for (i = 0; i < results.Feeds.length; i++) {

                    f = results.Feeds[i];
					
					str=str+'<br><a class="glow" target="_blank" href="http://www.pango.edu.au/DiscussionDetails/?id=' + f.CategoryId + '">'+
					f.TopicTitle +"</a><br>";
		}
		localStorage.setItem('v4-pangoData',str);
	});
	}

	
//////////////////////////
function playSound(which) {
	if (which=='' || typeof(which)=='undefined') {return;}
	sounds[which].play();
	}
function pauseSound(which){
	sounds[which].pause();
	}
function lowerBgSound(){
	sounds[0].volume=0.2;
			sounds[2].volume=0.2;
	}
function increaseBgSound(){
	sounds[0].volume=0.4;
			sounds[2].volume=0.4;
	}
function pauseSounds() {
	
	for (i=0; i< 101; i++ ) {
			sounds[i].pause();
		}
	}
	
var isMuted=false;
function muteSounds() {
	if (!isMuted) {
		for (i=0; i< 101; i++ ) {
			sounds[i].volume=0;
		}
		isMuted=true;
		}
	else{
		for (i=0; i< 101; i++ ) {
			sounds[i].volume=1;
		}
		/*
		sounds[0].volume=0.4;
			sounds[1].volume=0.1;
			sounds[2].volume=0.4;
			sounds[4].volume=0.4;
			sounds[35].volume=0.6;*/
			isMuted=false;
		}
	
	}
	
function saveGame() {
	
	}
	
	
function dieUBastard() {
	
	alert('You have all died. \nHappy?');
	
	localStorage.setItem('v5-q1','0');
	localStorage.setItem('v5-q2','0');
	localStorage.setItem('v5-q3','0');
	localStorage.setItem('v5-q4','0');
	localStorage.setItem('v5-q5','0');
	localStorage.setItem('v5-q6','0');
	localStorage.setItem('v5-q7','0');
	localStorage.setItem('v5-q8','0');
	localStorage.setItem('v5-q9','0');
	localStorage.setItem('v5-q10','0');
	localStorage.setItem('v5-pangoData','');
	localStorage.setItem('v5-phoneData','');
	localStorage.setItem('secs','900');
	localStorage.setItem('mins','15');
	localStorage.setItem('v5-completed','25');
	
	
		setTimeout(function(){gotoScene('scene-3.html');},2000);
	}
/////////////////////
//////////////////////
//////Helper Functions

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

///////////////////
function getTransformProperty(element) {
    // Note that in some versions of IE9 it is critical that
    // msTransform appear in this list before MozTransform
    var properties = [
        'transform',
        'WebkitTransform',
        'msTransform',
        'MozTransform',
        'OTransform'
    ];
    var p;
    while (p = properties.shift()) {
        if (typeof element.style[p] != 'undefined') {
            return p;
        }
    }
    return false;
}
//////////////////
function randomXToY(minVal,maxVal,floatVal)
{
  var randVal = minVal+(Math.random()*(maxVal-minVal));
  return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
}
/////////////////////////

