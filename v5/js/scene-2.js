// JavaScript Document


function scene2() {
	var iDelay=1000;
	
	showScene('scene-2');
	
	updatePangoTxt();
	
		$('input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	
	
	
	if (localStorage.getItem('v5-q2')=='1'){
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg5.jpg)';
		document.getElementById('s2-continaer').style.opacity=1;
		showQ3();
		return;
		}
		
		
	if (localStorage.getItem('v5-q1')=='1'){
		gotoB();
		return;
		}
		
	
	
	
	////////////////actual function begins now
	
	setTimeout (function () {	
		document.getElementById('s2-continaer').style.opacity=1;
		updatePhoneTxt(1,'You have started playing Timestorm - Chapter 5');
		window.parent.playSound('0');
		}, 1000+iDelay);
		
		iDelay=iDelay+1000;
		
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.opacity=0;
		}, 4000+iDelay);
	
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg3.jpg)';
		}, 5500+iDelay);
		
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.opacity=1;
		}, 7000+iDelay);
	
	setTimeout(function() {
		showDialogue('s2-dia-1');
		parent.playSound(35);
		} , 14000+iDelay);
		
	setTimeout(function() {
		hideDialogue('s2-dia-1');
		} , 18000+iDelay);
		
	setTimeout(function() {
		document.getElementById('q1').style.display='block';
		document.getElementById('q1').style.opacity=1;
		} , 19000+iDelay);
		
	setTimeout(function() {
		document.getElementById('q1-a').style.display='block';
		window.parent.playSound(100);
	
	
		} , 20000+iDelay);
		
}

///////////////////////////////////
var firstAns=true;

function markQ1(score) {
	//score = parseInt(score);
	
	window.parent.playSound(98);
	
	if (score == 0 ) {
		
		setTimeout (function() {
			document.getElementById('q1').style.opacity=0;
			}, 1000);
		
		setTimeout (function() {
			showDialogue('s2-dia-2');
			window.parent.playSound(36);
			}, 2000);
			
		setTimeout (function() {
			hideDialogue('s2-dia-2');
			}, 5000);
			
		setTimeout (function() {
			window.parent.playSound(100);
			document.getElementById('q1').style.opacity=1;
			}, 6000);
		
		}
	
	if (score == 1) {
		
		setTimeout (function() {
			document.getElementById('q1').style.opacity=0;
			}, 500);
		
		setTimeout (function() {
			showDialogue('s2-dia-3');
			window.parent.playSound(37);
			}, 2000);
			
		setTimeout (function() {
			hideDialogue('s2-dia-3');
			}, 5000);
		
		setTimeout (function() {
			gotoB();
			window.parent.pauseSound('0');
			}, 6000);
			
		}
	
	if (firstAns) {
		window.parent.sendResults (1,score) ;
		localStorage.setItem('v5-q1','1');
		updateCompleted(5);
		firstAns=false;
		}
	
	}
	
/////////////////////////////
function gotoB(){
	
	var iDelay=1000;
	window.parent.playSound(1);
	
	setTimeout (function() {
		document.getElementById('s2-continaer').style.opacity=0;
		}, iDelay);
		
	/*setTimeout (function() {
		document.getElementById('s2-continaer').style.backgroundImage='none';
		document.getElementById('s2-continaer').style.backgroundColor='black';
		}, 1000+iDelay);
		
	setTimeout (function() {
		document.getElementById('s2-continaer').style.opacity=1;
		}, 2000+iDelay);
		*/
	setTimeout( function () {
		showDialogue('s2-dia-4a');
		},2000+iDelay);
		
	setTimeout( function () {
		hideDialogue('s2-dia-4a');
		},7000+iDelay);
		
	setTimeout( function () {
		showDialogue('s2-dia-4');
		},9000+iDelay);
		
	
	setTimeout( function () {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg4.jpg)';

		},18000+iDelay);
		
	setTimeout ( function () {
		hideDialogue('s2-dia-4');
		}, 20000+iDelay);
		
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.opacity=1;
		}, 21000+iDelay);
		
	setTimeout ( function () {
		showDialogue('s2-dia-5');
		}, 23000+iDelay);
		
	setTimeout ( function () {
		updatePhoneTxt(3,'You are in the control tower lobby.');
		}, 24000+iDelay);
	
	setTimeout ( function () {
		hideDialogue('s2-dia-5');
		updateCompleted(10);
		}, 27000+iDelay);
		
	setTimeout ( function () {
		showDialogue('s2-dia-6a');
		window.parent.playSound(91);
		}, 28000+iDelay);
		
	setTimeout ( function () {
		hideDialogue('s2-dia-6a');
		}, 31000+iDelay);
		
	setTimeout ( function () {
		window.parent.playSound(2);
	},32000+iDelay);
	
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.opacity=0;
	},34000+iDelay);
	
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.backgroundImage='url(imgs/bg5.jpg)';
	},36000+iDelay);
	
	setTimeout ( function () {
		document.getElementById('s2-continaer').style.opacity=1;
	},37000+iDelay);
	
	setTimeout ( function () {
		showDialogue('s2-dia-6');
		window.parent.playSound(38);
		}, 40000+iDelay);
		
	setTimeout ( function () {
		hideDialogue('s2-dia-6');
		}, 45000+iDelay);
	
	setTimeout(function() {
		document.getElementById('q1-a').innerHTML='';
		document.getElementById('q2').style.display='block';
		document.getElementById('q2').style.opacity=1;
		} , 46000+iDelay);
		
	setTimeout(function() {
		document.getElementById('q2-a').style.display='block';
		window.parent.playSound(100);
	/*
		$('#q2-ans input:radio').screwDefaultButtons({
		checked: 	"url(imgs/radio2.png)",
		unchecked:	"url(imgs/radio1.png)",
		width:		22,
		height:		22,
	});
	*/
	 firstAns=true;
		} , 47000+iDelay);

	}
///////////////////////////////////


function markQ2(score) {
	//score = parseInt(score);
	
	window.parent.playSound(98);
	
	if (score == 0 ) {
		
		setTimeout (function() {
			document.getElementById('q2').style.opacity=0;
			}, 1000);
		
		setTimeout (function() {
			showDialogue('s2-dia-7');
			window.parent.playSound(39);
			}, 2500);
			
		setTimeout (function() {
			hideDialogue('s2-dia-7');
			}, 6000);
			
		setTimeout (function() {
			window.parent.playSound(100);
			document.getElementById('q2').style.opacity=1;
			}, 7500);
		
		}
	
	if (score == 1) {
		
		setTimeout (function() {
			document.getElementById('q2').style.opacity=0;
			}, 500);
			
		setTimeout (function () {
			showQ3();
			}, 1000);
		}
	
	if (firstAns) {
		window.parent.sendResults (2,score) ;
		localStorage.setItem('v5-q2','1');
		firstAns=false;
		}
	
	}
	
///////////////////////////////////////////
var q3= new Array(6);

var san=0;
var ed=0;
var med=0;
var doug=0;
var tyres=0;
var dorian=0;

function showQ3() {
	updateCompleted(15);
	
	setTimeout (function() {
		document.getElementById('q3').style.display='block';
			document.getElementById('q3').style.opacity=1;
			}, 1000);
	
	setTimeout (function() {
			window.parent.playSound(100);
			document.getElementById('q3-a').style.display='block';
			}, 1700);
	
	}
	
///////////////////////////////
var dont=false;
var score = 0;
function showMe(what){
	//alert(what);
	if (dont) return;
	
	
	document.getElementById(what).style.backgroundImage='url(imgs/q3/'+what+'-2.png)';
	//document.getElementById(what+'Txt').style.opacity=1;
	document.getElementById('clickTxt').style.display='none';

	
	if (what=='san' && san==0){
		dont=true; san++; score++;
		window.parent.playSound(66);
		document.getElementById(what+'Txt').style.display='block';
		showDialogue('s2-dia-9');
		
		setTimeout(function() {hideDialogue('s2-dia-9');dont=false;updatePhoneTxt(1,'You have met Sandinsky.');} , 14000);
		}
	
	if (what=='ed' && ed==0){
		dont=true; ed++; score++;
		window.parent.playSound(53);
		document.getElementById(what+'Txt').style.display='block';
		showDialogue('s2-dia-10');
		
		setTimeout(function() {hideDialogue('s2-dia-10');dont=false;updatePhoneTxt(1,'You have met Mr. Edwards.');} , 4000);
		}
		
	if (what=='med' && med==0){
		dont=true; med++; score++;
		window.parent.playSound(59);
		document.getElementById(what+'Txt').style.display='block';
		showDialogue('s2-dia-11');
		
		setTimeout(function() {hideDialogue('s2-dia-11');dont=false;updatePhoneTxt(1,'You have met Mrs Edwards.');} , 6000);
		}
		
	if (what=='doug' && doug==0){
		dont=true; doug++; score++;
		window.parent.playSound(23);
		document.getElementById(what+'Txt').style.display='block';
		showDialogue('s2-dia-12');
		setTimeout(function() {hideDialogue('s2-dia-12');dont=false;updatePhoneTxt(1,'You have met Doug.');} , 13000);
		}
		
	if (what=='tyres' && tyres==0){
		dont=true; tyres++; score++;
		window.parent.playSound(80);
		document.getElementById(what+'Txt').style.display='block';
		showDialogue('s2-dia-13');
		setTimeout(function() {hideDialogue('s2-dia-13');dont=false;updatePhoneTxt(1,'You have met Tyler.');} , 14000);
		}
		
	if (what=='dorian' && dorian==0 && score==5){
		dont=true; dorian++; 
		window.parent.playSound(3);
		document.getElementById(what+'Txt').style.display='block';
		
		setTimeout(function() {dont=false;updatePhoneTxt(1,'You have met Dorian.');} , 1000);
		setTimeout (function () {endQ3();}, 2000);
		}
		

	if (score==5) {document.getElementById('dorian').style.backgroundImage='url(imgs/q3/dorian-2.png)';}

	
	}
	
///////////////////////
function endQ3(){
	
	if (dont) return;
	
	dont=true;
	window.parent.sendResults (3,score) ;
	localStorage.setItem('v5-q3','1');
	updateCompleted(25);
	
	document.getElementById('q3-a').style.display='none';
	setTimeout(function () {document.getElementById('q3').style.opacity=0;} , 500);
	
	setTimeout(function() {
		window.parent.playSound(67);
		showDialogue('s2-dia-14');
		}, 1500);
		
	setTimeout(function() {
		hideDialogue('s2-dia-14');
		window.parent.playSound(4);
		}, 9000);
		
	setTimeout(function() {document.getElementById('s2-continaer').style.opacity=0; },11300);
	
	setTimeout(function() {showDialogue('s2-dia-15');}, 14000); 
	setTimeout(function() {hideDialogue('s2-dia-15');}, 19000); 
	
	setTimeout(function() {window.parent.gotoScene('scene-3.html');}, 20000); 
	}
	
