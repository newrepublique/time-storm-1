// JavaScript Document

function scene5() {
    window.parent.playSound(19);
    window.parent.playSound(21);
    var iDelay = 2000;


    showScene('scene-5');
    updatePangoTxt();
    updatePhoneTxt(3, 'You are in the Giant Cave.');
    updateCompleted(90);
    //window.parent.playSound(7);


    setTimeout(function () { document.getElementById('s5-continaer').style.opacity = 1; }, iDelay);
    //refreshPhoneTxt();

    if (localStorage.getItem('v5-q10') == '1') {
        gotoC();
        return;
    }

    setTimeout(function () {
        showDialogue('s5-dia-1');
        window.parent.playSound(94);
    }, 2500 + iDelay);

    setTimeout(function () { hideDialogue('s5-dia-1'); window.parent.playSound(34); }, 5500 + iDelay);

    setTimeout(function () {
        gotoB();
    }, 6500 + iDelay);

}



/////////////////////////////////
function gotoB() {

    var iDelay = 0;

    setTimeout(function () {
        document.getElementById('s5-continaer').style.opacity = 0;
    }, 1000 + iDelay);

    setTimeout(function () {
        document.getElementById('s5-continaer').style.backgroundImage = 'url(imgs/bg21.jpg)';
    }, 1500 + iDelay);

    setTimeout(function () {
        document.getElementById('s5-continaer').style.opacity = 1;
    }, 1800 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-2');
        window.parent.playSound(47);
    }, 4100 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-2');
    }, 7500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-3');
        window.parent.playSound(86);
    }, 8000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-3');
    }, 11000 + iDelay);


    setTimeout(function () {
        showDialogue('s5-dia-4');
        window.parent.playSound(48);
    }, 11500 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-4');
    }, 17000 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-5');
        window.parent.playSound(30);
    }, 18000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-5');
        window.parent.playSound(20);
    }, 21500 + iDelay);

    iDelay = iDelay + 2000;

    setTimeout(function () {
        showDialogue('s5-dia-7');
        window.parent.playSound(63);
    }, 22000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-7');
    }, 26500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-8');
        window.parent.playSound(76);
    }, 27000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-8');
    }, 37500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-9');
        window.parent.playSound(31);
    }, 38000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-9');
    }, 44500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-10');
        window.parent.playSound(77);
    }, 45000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-10');
    }, 50500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-11');
        window.parent.playSound(96);
    }, 51000 + iDelay);

    iDelay = iDelay + 1000;
    setTimeout(function () {
        hideDialogue('s5-dia-11');
    }, 61500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-12');
        window.parent.playSound(78);
    }, 62000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-12');
        document.getElementById('greenDot4').style.display = 'block';
    }, 73500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-13');
        window.parent.playSound(49);
    }, 74000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-13');
    }, 77500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-14');
        window.parent.playSound(79);
    }, 78000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-14');
    }, 79500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-15');
        window.parent.playSound(64);
    }, 80000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-15');
    }, 83500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-16');
        window.parent.playSound(50);
    }, 84000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-16');
    }, 88500 + iDelay);

    setTimeout(function () {
        showQ10();
    }, 89000 + iDelay);
}

///////////////////////////////////////////////
function showQ10() {
    var iDelay = 0;
    $('input:radio').screwDefaultButtons({
        checked: "url(imgs/radio2.png)",
        unchecked: "url(imgs/radio1.png)",
        width: 22,
        height: 22,
    });

    setTimeout(function () {
        document.getElementById('q10').style.display = 'block';
        document.getElementById('q10').style.opacity = 1;
    }, iDelay);

    setTimeout(function () {
        document.getElementById('q10-a').style.display = 'block';
        window.parent.playSound(100);

    }, 1000 + iDelay);


}
////////////////////////////////////////////////////
function markQ10(score) {
    var iDelay = 1000;

    updateCompleted(90);
    window.parent.playSound(98);

    window.parent.sendResults(10, score);
    localStorage.setItem('v5-q10', '1');

    setTimeout(function () { document.getElementById('q10').style.opacity = 0; document.getElementById('q10-a').style.display = 'none'; }, 200);

    setTimeout(function () { document.getElementById('q10').style.display = 'none'; }, 1000);


    setTimeout(function () { gotoC(); }, 1000);

}

/////////////////////////////////
function gotoC() {
    window.parent.pauseSound(19);
    var iDelay = 1000;

    setTimeout(function () {
        document.getElementById('s5-continaer').style.opacity = 0;
        showDialogue('s5-dia-17');
    }, 1000 + iDelay);

    setTimeout(function () {
        document.getElementById('s5-continaer').style.backgroundImage = 'url(imgs/bg22.jpg)';
        window.parent.playSound(21);
    }, 1500 + iDelay);

    setTimeout(function () {
        document.getElementById('s5-continaer').style.opacity = 1;
        hideDialogue('s5-dia-17');
        updatePhoneTxt(3, 'You are outside The Old Mineshaft.');
    }, 7800 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-18');
        window.parent.playSound(87);
    }, 10000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-18');
    }, 13500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-19');
        window.parent.playSound(97);
    }, 14000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-19');
    }, 17500 + iDelay);

    //iDelay=iDelay+15000;

    setTimeout(function () {
        showDialogue('s5-dia-24');
        window.parent.playSound(51);
    }, 19000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-24');
    }, 29500 + iDelay);

    setTimeout(function () {
        window.parent.playSound(22);
        document.getElementById('greenDot4').style.display = 'none';
    }, 30000 + iDelay);

    setTimeout(function () {
        showVortex();
    }, 32000 + iDelay);


}

////////////////////////////////////////
function showVortex() {
    window.parent.pauseSound(21);

    document.getElementById('scene-5').style.backgroundColor = 'white';

    document.getElementById('s5-continaer').style.opacity = 0;

    setTimeout(function () { document.getElementById('s5-continaer').style.display = 'none'; }, 1000);

    setTimeout(function () {
        //alert('start');
        document.getElementById('vortex').style.display = 'block';
        document.getElementById('vortex').setAttribute('class', 'vortex_active');
        setTimeout(function () { window.parent.playSound(102); }, 500);
        //sounds[0].play();
    }, 1500);

    setTimeout(function () {

        gotoAfterLife();

    }, 16500);

}

/////////////////////////////////////////////////
function gotoAfterLife() {

    var iDelay = 1000;

    document.getElementById('phone').style.display = 'none';
    document.getElementById('pango').style.display = 'none';
    document.getElementById('frame').style.display = 'none';
    document.getElementById('title').style.display = 'none';
    document.getElementById('scene-5').style.backgroundColor = 'black';

    setTimeout(function () {
        document.getElementById('s5-continaer').style.backgroundImage = 'url(imgs/bg23.jpg)';
        document.getElementById('s5-continaer').setAttribute('class', 'appear stretchBG');
        document.getElementById('s5-continaer').style.opacity = 1;
        document.getElementById('s5-continaer').style.display = 'block';
    }, iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-25');
        window.parent.playSound(88);
    }, 2000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-25');
    }, 7500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-26');
        window.parent.playSound(89);
    }, 9000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-26');
    }, 13500 + iDelay);

    setTimeout(function () {
        showDialogue('s5-dia-27');
        window.parent.playSound(90);
    }, 15000 + iDelay);

    setTimeout(function () {
        hideDialogue('s5-dia-27');
    }, 24500 + iDelay);

    setTimeout(function () {
        window.parent.gotoScene('countdown.html');
    }, 26500 + iDelay);

}