// JavaScript Document
//var state_7='off';
var state_7=localStorage.getItem('v1-state_7');
var p=localStorage.getItem('v1-isThereElectricity');
if(p=='1')
var isThereElectricity=true;
else
var isThereElectricity=false;
var currentQuestion=1;
if(localStorage.getItem('v1-q3')=='1'){
	currentQuestion=2;
	}
var q1_1=0;
var q1_2=0;
var q1_3=0;
var q1_4=0;
var q1_5=0;
var q1_6=0;
var q1_7=0;
var q1_8=0;
var q2_1=0;
var q2_2=0;
var q2_3=0;

function scene7() {
		showScene('scene-7');
		getLSTimer();
		updatePangoTxt();
		updateHas();
	
	if (state_7=='on') {showOn_7(); return;}
	if (state_7=='again') {showAgain_7(); return;}
	
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s7-continaer').style.display='block';
	setTimeout(function(){document.getElementById('s7-continaer').style.opacity=1;},2000);
	setTimeout(function(){updatePhoneTxt(3,'You are in the boat\'s Gallery');},3000);
	updateCompleted('');
	countdown();

mkADecision();
	
	}
//////////////////
function mkADecision() {
	localStorage.setItem('v1-state_7','again');
	
	if (!isThereElectricity) {
		
		setTimeout(function() {
		showDialogue('s7-dia-2');
		},1000);
		
	setTimeout(function() {
		hideDialogue('s7-dia-2');
		showGreenDots();
		},7000);
		
	
		}
	else{
		
		
	setTimeout(function() {
		showDialogue('s7-dia-1');
		window.parent.playSound(29);
		},1000);
		
	setTimeout(function() {
		hideDialogue('s7-dia-1');
		},4000);
		
	setTimeout(function() {
		document.getElementById('purifier').style.display='block';
		},5000);
		
		}
	
	}
//////////////////
function gotoBridge() {
	//save LS state
	timerPaused=true;
	document.getElementById('s7-continaer').style.opacity=0;
	setTimeout(function(){window.location='scene-4.html'},2000);
	
	}
	
function gotoEngine () {
	//save LS state
	timerPaused=true;
	document.getElementById('s7-continaer').style.opacity=0;
	setTimeout(function(){window.location='scene-6.html'}, 2000); 
	}
	
function gotoCabin () {
	//save LS state
	timerPaused=true;
	document.getElementById('s7-continaer').style.opacity=0;
	setTimeout(function(){window.location='scene-5.html'},2000);
	}
//////////////////
function qRoutine() {
	document.getElementById('purifier').style.display='none';; 
	document.getElementById('q1Overlay').style.display='block';
	document.getElementById('q1Overlay').style.opacity=1; 
	window.parent.playSound(17);

	document.getElementById('q-'+currentQuestion).style.display='block';	
		
		if(currentQuestion==1) {mkDragables1();listen7_1();
		startRecording();
		}
		else { 
		mkDragables2();
		listen7_2();
		}

		}
////////////////
function mkDragables1() {
	var w = $('#q1-1').width();
	var h = $('#q1-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q1Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q1-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-1').position().top-$('#placeholder-1').parent().position().top;
	var ans_left= $('#placeholder-1').position().left-$('#placeholder-1').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_1=1;
 window.parent.playSound(15);
 snap2grid('q1-1','placeholder-1');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-4').position().top-$('#placeholder-4').parent().position().top;
	var ans_left= $('#placeholder-4').position().left-$('#placeholder-4').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_2=1;
window.parent.playSound(15);
snap2grid('q1-2','placeholder-4');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-7').position().top-$('#placeholder-7').parent().position().top;
	var ans_left= $('#placeholder-7').position().left-$('#placeholder-7').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_3=1;
 window.parent.playSound(15);
 snap2grid('q1-3','placeholder-7');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-8').position().top-$('#placeholder-8').parent().position().top;
	var ans_left= $('#placeholder-8').position().left-$('#placeholder-8').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_4=1;
 window.parent.playSound(15);
 snap2grid('q1-4','placeholder-8');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-6').position().top-$('#placeholder-6').parent().position().top;
	var ans_left= $('#placeholder-6').position().left-$('#placeholder-6').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});

$( "#q1-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_5=1;
 window.parent.playSound(15);
 snap2grid('q1-5','placeholder-6');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-6" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-5').position().top-$('#placeholder-5').parent().position().top;
	var ans_left= $('#placeholder-5').position().left-$('#placeholder-5').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});

$( "#q1-6" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_6=1;
 window.parent.playSound(15);
 snap2grid('q1-6','placeholder-5');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-7" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-3').position().top-$('#placeholder-3').parent().position().top;
	var ans_left= $('#placeholder-3').position().left-$('#placeholder-3').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});

$( "#q1-7" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_7=1;
 window.parent.playSound(15);
 snap2grid('q1-7','placeholder-3');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-8" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-2').position().top-$('#placeholder-2').parent().position().top;
	var ans_left= $('#placeholder-2').position().left-$('#placeholder-2').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});

$( "#q1-8" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_8=1;
 window.parent.playSound(15);
 snap2grid('q1-8','placeholder-2');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
}
////////////////////
////////////////
function mkDragables2() {
	var w = $('#q2-1').width();
	var h = $('#q2-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q1Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q2-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-3').position().top-$('#placeholder2-3').parent().position().top;
	var ans_left= $('#placeholder2-3').position().left-$('#placeholder2-3').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_1=1;
window.parent.playSound(15);
snap2grid('q2-1','placeholder2-3');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q2-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-2').position().top-$('#placeholder2-2').parent().position().top;
	var ans_left= $('#placeholder2-2').position().left-$('#placeholder2-2').parent().position().left;
					//console.log(left+","+top+":"+ans_left+","+ans_top);
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_2=1;
 window.parent.playSound(15);
 snap2grid('q2-2','placeholder2-2');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q2-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder2-1').position().top-$('#placeholder2-1').parent().position().top;
	var ans_left= $('#placeholder2-1').position().left-$('#placeholder2-1').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q2-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q2_3=1;
 window.parent.playSound(15);
 snap2grid('q2-3','placeholder2-1');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
}
////////////////////
function hasColided(obj1_left, obj1_top, obj2_left, obj2_top){

	if(currentQuestion==1){
	var w = $('#q1-1').width();
	var h = $('#q1-1').height();
	var w2 = ($('#placeholder-1').width()-30);
	var h2= ($('#placeholder-1').height()-20);
	}
	else{
	var w = $('#q2-1').width();
	var h = $('#q2-1').height();
	var w2 = ($('#placeholder2-1').width()-30);
	var h2= ($('#placeholder2-1').height()-20);	
		}
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left+30;
    var br = obj2_left+30+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top+20;
    var bb = obj2_top+20+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}

////////////////
function listen7_1(){
	
	
	if (q1_1 == 1)
	if (q1_2 == 1){
	if (q1_3 == 1){
	if (q1_4 == 1){
	if (q1_5 == 1){
	if (q1_6 == 1){
	if (q1_7 == 1){
	if (q1_8 == 1){
		
		recLap();
		stopRecording();
		window.parent.sendResults(1,3,attempt,matrixStr);
	
		document.getElementById('welldone1').style.display='block';
		currentQuestion=2;
		

		setTimeout(function() {
			$('#q-1').css('opacity',0);
			$('#q-1').css('display','none');
			window.parent.playSound(17);
			$('#q-2').css('display','block');
			mkDragables2();
			currentQuestion=2;
			updateCompleted(80);
			localStorage.setItem('v1-q3','1');
			listen7_2();
			startRecording();
			},4000);
			
			
		return;
	} } } }}}}
	
	setTimeout (listen7_1,200);
	}
////////////////
function listen7_2(){
	
	if (q2_1 == 1)
	if (q2_2 == 1){
	if (q2_3 == 1){
		
		recLap();
		stopRecording();
		window.parent.sendResults(1,4,attempt,matrixStr);
		
		document.getElementById('welldone2').style.display='block';
		currentQuestion=2;
		localStorage.setItem('v1-q4','1');
		localStorage.setItem('v1-state_7','on');
		localStorage.setItem('v1-state_4','on');
		

		setTimeout(function() {
			$('#q-2').css('opacity',0);
			$('#q-2').css('display','none');
			window.parent.playSound(17);
			$('#q-2').css('display','none');
			},4000);
			
		setTimeout(function() {
			$('#q1Overlay').css('opacity',0);
			$('#q1Overlay').css('display','none');
			window.parent.playSound(17);
			},4000);
			
		updateCompleted(90);
		timerPaused=true;
		completed();
			
			
		return;
	} } 
	
	setTimeout (listen7_2,200);
	}
////////////////
function showHelp(which) {
	
	if (which=='1')  {bg=  "url(imgs/chemical/chemical-help-1.png)";}
	if (which=='2')  {bg=  "url(imgs/chemical/chemical-help-1.png)";}
	$('.q').css('opacity',0);
	document.getElementById('q-help').style.backgroundImage=bg;
	$('#q-help').css('opacity',1);
	$('#q-help').css('display','block');
	}
function showBack() {
	$('.q').css('opacity',1);
	//$('#q-'+currentQuestion).css('opacity',1);
	$('#q-help').css('display','none');
	}
////////////////
function showGreenDots() {
	document.getElementById('greenDot1').style.display='block';
	document.getElementById('greenDot2').style.display='block';
	document.getElementById('greenDot3').style.display='block';
	setTimeout(function(){window.parent.playSound(5);},1000);
	setTimeout(function(){
		window.parent.playSound(17);
		document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');
		},3000);
	}
////////////
function completed() {
	
	setTimeout(function() {
		showDialogue('s7-dia-4');
		setTimeout(function(){hideDialogue('s7-dia-4');},3000);
		},1000);
	
	setTimeout (function () {
		showDialogue('s7-dia-5');
		document.getElementById('s7-continaer').setAttribute('class','stretchBG ahista blink');
		document.getElementById('s7-continaer').style.backgroundImage='url(imgs/bg6b.jpg)';
		setTimeout(function(){hideDialogue('s7-dia-5');},5000);
		
		},5000);
		
	setTimeout(function() {
		timerPaused=true;
		window.parent.playSound(13);
		showDialogue('s7-dia-5');
		setTimeout(function(){hideDialogue('s7-dia-5');},4000);
		},11000);
		
	setTimeout(function() {
		    timerPaused=true;
		    document.getElementById('timer').style.display='none';
		    showDialogue('s7-dia-6');
		    window.parent.playSound(32);
		
		    setTimeout(function () {
		        hideDialogue('s7-dia-6'); showGreenDots();
		    }, 3000);
		    setTimeout(function () {
		        document.getElementById('greenDot2').setAttribute('class', 'blink');
		        document.getElementById('greenDot3').setAttribute('class', 'blink');
		    }, 4000);
		    setTimeout(function(){
			
			    window.parent.playSound(11);
			    updatePhoneTxt(1, 'You\'ve fixed Water Purification Plant');
		    }, 3000);

		},16000);
	}
///////////////
function showOn_7() {
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s7-continaer').style.display='block';
	document.getElementById('s7-continaer').style.backgroundImage='url(imgs/bg6b.jpg)';
	setTimeout(function(){document.getElementById('s7-continaer').style.opacity=1;updatePhoneTxt(3,'You are in the boat\'s Gallery');},2000);
	updateCompleted('');
	//countdown();

showGreenDots();
	
	}
/////////////
function showAgain_7() {
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s7-continaer').style.display='block';
	if(localStorage.getItem('v1-q4')=='1'){
	document.getElementById('s7-continaer').style.backgroundImage='url(imgs/bg6b.jpg)';
	document.getElementById('timer').style.display='none';
	}
	setTimeout(function(){document.getElementById('s7-continaer').style.opacity=1;updatePhoneTxt(3,'You are in the boat\'s Gallery');},2000);
	updateCompleted('');
	countdown();

mkADecision();
	
	}
//////////////////////	
function gotoBridge() {
	//save LS state
	timerPaused=true;
	document.getElementById('s7-continaer').style.opacity=0;
	setTimeout(function(){window.parent.gotoScene('scene-4.html')},2000);
	
	}
	
function gotoEngine () {
	//save LS state
	timerPaused=true;
	document.getElementById('s7-continaer').style.opacity=0;
	setTimeout(function(){window.parent.gotoScene('scene-6.html')}, 2000); 
	}
	
function gotoCabin () {
	//save LS state
	timerPaused=true;
	document.getElementById('s7-continaer').style.opacity=0;
	setTimeout(function(){window.parent.gotoScene('scene-5.html')},2000);
	}