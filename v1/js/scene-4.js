// JavaScript Document
var state_4=localStorage.getItem('v1-state_4'); //can be: again on off

function scene4() {
	//console.log('scene-4 was called');
	updatePangoTxt();
	if (state_4=='again') { showAgain_4(); return;}
	if (state_4=='on') { showOn_4(); return;}
	
	updateCompleted(20);
	showScene('scene-4');
	updatePangoTxt();
	
	
	mins = 15;
   secs = 15 * 60;
   localStorage.setItem('mins',mins.toString()+"");
	localStorage.setItem('secs',secs.toString()+"");
	
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ahista');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s4-continaer').style.display='block';

	
	window.parent.playSound(1);
	window.parent.decreaseBGSound();
	var iDelay=2000;
	
	setTimeout (function() {
		updatePhoneTxt(1,'You have found the boat\'s bridge');
			document.getElementById('s4-continaer').style.opacity=1;
		}, 1000+iDelay);
		
		setTimeout (function() {
		document.getElementById('transmitor').style.display='block';
		document.getElementById('topMap').style.display='block';
		document.getElementById('newspaper').style.display='block';
		//document.getElementById().style.opacity=1;
		}, 3000+iDelay);
		
		setTimeout (function() {
		document.getElementById('s4-dia-1').style.opacity=1;
		listen_4();
		}, 5000+iDelay);
		
		setTimeout (function() {
		document.getElementById('s4-dia-1').style.opacity=0;
		}, 9000+iDelay);

	}
	
var lastClicked='';

function listen_4(){
	
	switch(lastClicked) {
		case '':
		lastClicked='nothing';
		setTimeout(listen_4,100);
		break;
		
		case 'newspaper':
		window.parent.playSound(9);
		setTimeout(function(){
		document.getElementById('newspaperOverlay').style.display='block';
		document.getElementById('newspaperOverlay').style.opacity=1;
		}, 1000);
		
		setTimeout(function() {
			//document.getElementById('s4-dia-2').style.opacity=1;
			showDialogue('s4-dia-2');
			}, 3000);
			
		setTimeout(function() {
			//document.getElementById('s4-dia-2').style.opacity=0;
			hideDialogue('s4-dia-2');
			}, 9000);
		break;
		
		case 'trans':
		window.parent.playSound(8);
		lastClicked='nothing'
		setTimeout(function () {showDialogue('s4-dia-3');}, 3000);
		setTimeout(function () {hideDialogue('s4-dia-3');listen_4();}, 7000);
		break;
				
		
		case 'topMap':
		//sounds[8].play();
		setTimeout(function () {showDialogue('s4-dia-4');}, 200);
		setTimeout(function () {hideDialogue('s4-dia-4');}, 6000);
		
		setTimeout(function() {
			document.getElementById('topMap').setAttribute('class','stretchBG dontBlink');
			document.getElementById('transmitor').setAttribute('class','stretchBG dontBlink');
			document.getElementById('newspaper').setAttribute('class','stretchBG dontBlink');
			document.getElementById('phone').setAttribute('class','stretchBG blink');
					document.getElementById('phone').style.backgroundImage='url(imgs/phone-map-1.png)';
					showGreenDots();
					lastClicked='nothing';
					listen_4();
		},3000);
		break;
		
		case '1':
		finishScene_4(1);
		break;
		
		case '2':
		finishScene_4(2);
		break;
		
		case '3':
		finishScene_4(3);
		break;
		
		default:
		setTimeout(listen_4,100);
		break;
		
		}
	
	}

function showGreenDots() {
	document.getElementById('greenDot1').style.display='block';
	document.getElementById('greenDot2').style.display='block';
	document.getElementById('greenDot3').style.display='block';
	setTimeout(function(){window.parent.playSound(5);},500);
	setTimeout(function(){updatePhoneTxt(1,'You have found a map of the boat');},600);
	setTimeout(function(){
		window.parent.playSound(18);
		document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');
		},1000);
		
		localStorage.setItem('v1-state_4','again');
	}
	
function finishScene_4(where) {
	var nextURL='scene-5.html';
	
	switch (where) {
		case 1:
		nextURL='scene-5.html';
		break;
		
		
		case 2:
		nextURL='scene-7.html';
		break;
		
		case 3:
		nextURL='scene-6.html';
		break;
		
		
		}
		document.getElementById('greenDot1').style.display='none';
		document.getElementById('greenDot2').style.display='none';
		document.getElementById('greenDot3').style.display='none';
					
			document.getElementById('topMap').style.display='none';
			document.getElementById('transmitor').style.display='none';
			document.getElementById('newspaper').style.display='none';
			document.getElementById('phone').style.opacity=0;;
			document.getElementById('pango').style.opacity=0;;
			document.getElementById('frame').style.opacity=0;;
			document.getElementById('title').style.opacity=0;;
			document.getElementById('phone').style.opacity=0;;
			document.getElementById('s4-continaer').style.backgroundImage='';
			
			if(state_4=='off'){
			setTimeout(function() {document.getElementById('loadingGif').style.opacity=1;} , 3000 );
			setTimeout(function() {showDialogue('s4-dia-5'); window.parent.playSound(26);} , 4500 );
			setTimeout(function() {hideDialogue('s4-dia-5'); } , 8000 );
			setTimeout(function() {showDialogue('s4-dia-6'); } , 9500 );
			setTimeout(function() {hideDialogue('s4-dia-6');} , 15000 );
			
			setTimeout(function() {document.getElementById('loadingGif').style.opacity=0; } , 16000 );
			
			setTimeout(function() {window.location=nextURL; } , 17000 );
			}
			
			else {
				setTimeout(function() {window.parent.gotoScene(nextURL); } , 3000 );
				
				}
			
			
	}
	
//////////////////////////////////////////	
function showAgain_4() {
	
	showScene('scene-4');
	updateCompleted('');
	
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ahista');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('transmitor').style.display='block';
	document.getElementById('topMap').style.display='block';
	document.getElementById('newspaper').style.display='block';
	document.getElementById('transmitor').setAttribute('class','dontBlink ');
		document.getElementById('topMap').setAttribute('class','dontBlink ');
		document.getElementById('newspaper').setAttribute('class','dontBlink ');
	document.getElementById('s4-continaer').style.display='block';
	
	
	document.getElementById('timer').style.display='block';
	getLSTimer();
	
	document.getElementById('phone').style.backgroundImage='url(imgs/phone-map-1.png)';
					showGreenDots();
					lastClicked='nothing';
					listen_4();
					
					
	setTimeout (function() {
		updatePhoneTxt(3,'You are in the boat\'s bridge');
			document.getElementById('s4-continaer').style.opacity=1;
			timerPaused=false;
			countdown();
		}, 3000);
	
	}
	
////////////////////////////////////////////////
function showOn_4() {
	showScene('scene-4');
	updateCompleted('');
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s4-continaer').style.backgroundImage='url(imgs/bg7.jpg)';
	
	//document.getElementById('greenDot2').onclick= getBack();;
	//document.getElementById('transmitor1').onclick="function () {scene4_on_end();}";
	
	document.getElementById('s4-continaer').style.display='block';
	
	setTimeout (function() {
		updatePhoneTxt(3,'You are in the boat\'s bridge');
			document.getElementById('s4-continaer').style.opacity=1;
		}, 3000);
	
	setTimeout(function() {
	showDialogue('s4-dia-7');
	window.parent.playSound(14);
	
	setTimeout(function(){document.getElementById('transmitor1').style.display='block';},3000)
	
	}, 5000);
	
	setTimeout(function() {
	hideDialogue('s4-dia-7');
	},6000);
	
}
	
//////////////////
function getBack(){
	
	document.getElementById('greenDot1').style.display='none';
		document.getElementById('greenDot2').style.display='none';
		document.getElementById('greenDot3').style.display='none';
					
			document.getElementById('topMap').style.display='none';
			document.getElementById('transmitor').style.display='none';
			document.getElementById('newspaper').style.display='none';
			document.getElementById('phone').style.opacity=0;;
			document.getElementById('pango').style.opacity=0;;
			
			document.getElementById('frame').style.opacity=0;;
			document.getElementById('title').style.opacity=0;;
			document.getElementById('phone').style.opacity=0;;
			document.getElementById('s4-continaer').style.backgroundImage='';
	
	}
	
/////////////////
function scene4_on_end() {
	window.parent.playSound(22);
	
	setTimeout ( function () {
		showDialogue('s4-dia-8');
		setTimeout(function(){window.parent.playSound(31);},500);
		},17000);
		
		
	setTimeout ( function () {
		hideDialogue('s4-dia-8');
		},20000);
	
	setTimeout ( function(){
			document.getElementById('topMap').style.display='none';
			document.getElementById('transmitor1').style.display='none';
			document.getElementById('newspaper').style.display='none';
			document.getElementById('phone').style.opacity=0;;
			document.getElementById('pango').style.opacity=0;;
			document.getElementById('frame').style.opacity=0;;
			document.getElementById('title').style.opacity=0;;
			document.getElementById('phone').style.opacity=0;;
			document.getElementById('s4-continaer').style.backgroundImage='';
			
			setTimeout(function() {document.getElementById('loadingGif').style.opacity=1;} , 3000 );
			setTimeout(function() {document.getElementById('s4-dia-9').style.opacity=1; } , 5000 );
			setTimeout(function() {document.getElementById('s4-dia-9').style.opacity=0; document.getElementById('loadingGif').style.opacity=0; } , 16000 );
			setTimeout(function() {window.parent.gotoScene('scene-8.html'); } , 17000 );
	
	},21000);
	}
	
	
