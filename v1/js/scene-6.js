// JavaScript Document
var pickedUp=localStorage.getItem('v1-pickedUp');
var currentQuestion=1;
//var state_6='off'; //on off again
var state_6=localStorage.getItem('v1-state_6');

if(localStorage.getItem('v1-q1')=='1'){
	currentQuestion=2;
	}

function scene6(){
	
	showScene('scene-6');
	getLSTimer();
	updatePangoTxt();
	updateCompleted('');
	updateHas();
	if (state_6=='on') {showOn_6(); return;}
	if (state_6=='again') {showAgain_6(); return;}
	
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s6-continaer').style.display='block';
	setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;},2000);
	setTimeout(function(){updatePhoneTxt(3,'You are in the boat\'s Engine Room.');},3000);
	
	document.getElementById('timer').style.display='block';
	countdown();
	
	setTimeout(function() {
		showDialogue('s6-dia-1');
		window.parent.playSound(27);
		},4000);
		
	setTimeout(function() {
		hideDialogue('s6-dia-1');
		},7000);
		
	setTimeout(function() {
		showDialogue('s6-dia-2');
		},8000);
		
	setTimeout(function() {
		hideDialogue('s6-dia-2');
		},16000);
		
	setTimeout(function() {
		window.parent.playSound(28);
		showDialogue('s6-dia-3');
		},17000);
		
	setTimeout(function() {
		hideDialogue('s6-dia-3');
		},20000);
		
	setTimeout(function() {
		mkADecision();
		},21000);
	
	}
////////////////
function mkADecision(){
	localStorage.setItem('v1-state_6','again');
	
	if (pickedUp!='resistor'){
		
		showDialogue('s6-dia-5');
		document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');
		setTimeout(function() {hideDialogue('s6-dia-5');},4000);
		}
	else{
		document.getElementById('engine').style.display='block';
		}
	}
/////////////////
var q1_1=0;
var q1_2=0;
var q1_3=0;
var q1_4=0;
var q1_5=0;
function mkDragables1() {
	var w = $('#q1-1').width();
	var h = $('#q1-1').height();
	
	jQuery(".acQ").draggable({
            //containment: "#q1Overlay",
            scroll: false,
            revert: true,
            stop: function() {
                    
            }
        });
	
//	
$( "#q1-1" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-3').position().top-$('#placeholder-3').parent().position().top;
	var ans_left= $('#placeholder-3').position().left-$('#placeholder-3').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-1" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_1=1;
 window.parent.playSound(15);
  snap2grid('q1-1','placeholder-3');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-2" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-1').position().top-$('#placeholder-1').parent().position().top;
	var ans_left= $('#placeholder-1').position().left-$('#placeholder-1').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-2" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_2=1;
 window.parent.playSound(15);
  snap2grid('q1-2','placeholder-1');
 }
 else {
	 //report
	window.parent.playSound(16);
	 }
});
//
$( "#q1-3" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-5').position().top-$('#placeholder-5').parent().position().top;
	var ans_left= $('#placeholder-5').position().left-$('#placeholder-5').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-3" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_3=1;
 window.parent.playSound(15);
  snap2grid('q1-3','placeholder-5');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-4" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-4').position().top-$('#placeholder-4').parent().position().top;
	var ans_left= $('#placeholder-4').position().left-$('#placeholder-4').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-4" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_4=1;
 window.parent.playSound(15);
  snap2grid('q1-4','placeholder-4');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
//
$( "#q1-5" ).bind( "drag", function(event, ui) {
  var top=($(this).position().top
                    -$(this).parent().position().top);
  var left= ($(this).position().left
                    -$(this).parent().position().left);
	
	var ans_top= $('#placeholder-2').position().top-$('#placeholder-2').parent().position().top;
	var ans_left= $('#placeholder-2').position().left-$('#placeholder-2').parent().position().left;
					
  if(hasColided(left,top,ans_left,ans_top)) 
  {$(this).draggable('option','revert',false);}
  else{
	 $(this).draggable('option','revert',true); 
	  }
});
$( "#q1-5" ).bind( "dragstop", function(event, ui) {
 if (! $(this).draggable( "option", "revert" )) 
 {q1_5=1;
 window.parent.playSound(15);
  snap2grid('q1-5','placeholder-2');
 }
 else {
	 //report
	 window.parent.playSound(16);
	 }
});
	}

////////////////////
	
function hasColided(obj1_left, obj1_top, obj2_left, obj2_top){
	var w = $('#q1-1').width();
	var h = $('#q1-1').height();
	var w2 = ($('#placeholder-1').width()-30);
	var h2= ($('#placeholder-1').height()-20);
	
	var al = obj1_left;
    var ar = obj1_left+w;
    var bl = obj2_left+30;
    var br = obj2_left+30+w2;

    var at = obj1_top;
    var ab = obj1_top+h;
    var bt = obj2_top+20;
    var bb = obj2_top+20+h2;

    if(bl>ar || br<al){return false;}//overlap not possible
    if(bt>ab || bb<at){return false;}//overlap not possible

    if(bl>al && bl<ar){return true;}
    if(br>al && br<ar){return true;}

    if(bt>at && bt<ab){return true;}
    if(bb>at && bb<ab){return true;}

    return false;
	
	}
	
/////////////////////
function listen6_1(){
	
	if (q1_1 == 1)
	if (q1_2 == 1){
	if (q1_3 == 1){
	if (q1_4 == 1){
	if (q1_5 == 1){
		
		recLap();
		stopRecording();
		window.parent.sendResults(1,1,attempt,matrixStr);
		
		document.getElementById('welldone1').style.display='block';
		localStorage.setItem('v1-q1','1');
		currentQuestion=2;
		updateCompleted(50);

		setTimeout(function() {
			$('#q-1').css('opacity',0);
			$('#q-1').css('display','none');
			window.parent.playSound(17);
			$('#q-2').css('display','block');
			
			startRecording();
			},4000);
			
			
		return;
	} } } }
	
	setTimeout (listen6_1,200);
	}
/////////////////
function showHelp(which) {
	
	if (which=='1')  {bg=  "url(imgs/electrical/electrical-help-1.png)";}
	if (which=='2')  {bg=  "url(imgs/electrical/electrical-help-1.png)";}
	$('.q').css('opacity',0);
	document.getElementById('q-help').style.backgroundImage=bg;
	$('#q-help').css('opacity',1);
	$('#q-help').css('display','block');
	}
function showBack() {
	$('.q').css('opacity',1);
	//$('#q-'+currentQuestion).css('opacity',1);
	$('#q-help').css('display','none');
	}
//////////////////
function qRoutine() {
	document.getElementById('engine').style.display='none';; 
	document.getElementById('q1Overlay').style.display='block';
	document.getElementById('q1Overlay').style.opacity=1; 
	window.parent.playSound(17);

	document.getElementById('q-'+currentQuestion).style.display='block';	
		mkDragables1();
		
		if(currentQuestion==1) {
			startRecording();
			listen6_1();}

	
	showDialogue('s6-dia-4');
	setTimeout(function(){document.getElementById('hand-ani').setAttribute('class','handAn');},1500);
	setTimeout(function(){hideDialogue('s6-dia-4');},6000);
	}
/////////////////////
function q2Routine () {
		recLap();
		stopRecording();
		window.parent.sendResults(1,2,attempt,matrixStr);
	
	localStorage.setItem('v1-state_6','on');
	localStorage.setItem('v1-isThereElectricity','1');
	localStorage.setItem('v1-q2','1');
	localStorage.setItem('v1-pickedUp','');
	updateHas();
	window.parent.playSound(15);
	updateCompleted(60);
	 
	setTimeout(function(){
	document.getElementById('welldone2').style.display='block';
	$('.q-units1').css('display','none');
	},2000);
	
	setTimeout(function(){
	showDialogue('s6-dia-6');
	setTimeout(function(){hideDialogue('s6-dia-6');$('#q-2').css('display','none');},4000);
	},3000);
	
	setTimeout(function(){document.getElementById('q1Overlay').style.opacity=0;},8000);
	
	setTimeout(function(){
		window.parent.playSound(12);
		document.getElementById('q1Overlay').style.display='none;';
		},10000);
		
	setTimeout(function(){
		document.getElementById('s6-continaer').setAttribute('class','stretchBG ahista blink');
		},11000);
	
	setTimeout(function(){
		document.getElementById('s6-continaer').style.backgroundImage='url(imgs/bg5b.jpg)';
		},11000);
		
	setTimeout(function(){
		showDialogue('s6-dia-7');
		},13000);
		
	setTimeout(function(){
		hideDialogue('s6-dia-7');
		},18000);
		
	setTimeout(function(){
		window.parent.playSound(11);
		setTimeout(function(){updatePhoneTxt(1,'You have turned on the generator.');},3000);
		showGreenDots() ;
		},19000);
	}
/////////////////////
function showGreenDots() {
	document.getElementById('greenDot1').style.display='block';
	document.getElementById('greenDot2').style.display='block';
	document.getElementById('greenDot3').style.display='block';
	setTimeout(function(){window.parent.playSound(5);},1000);
	setTimeout(function(){
		window.parent.playSound(17);
		document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');
		},3000);
	}
////////////////////////
function gotoBridge() {
	//save LS state
	timerPaused=true;
	document.getElementById('s6-continaer').style.opacity=0;
	setTimeout(function(){window.parent.gotoScene('scene-4.html');},2000);
	
	}
	
function gotoGallery () {
	//save LS state
	timerPaused=true;
	document.getElementById('s6-continaer').style.opacity=0;
	setTimeout(function(){window.parent.gotoScene('scene-7.html');}, 2000); 
	}
	
function gotoCabin () {
	//save LS state
	timerPaused=true;
	document.getElementById('s6-continaer').style.opacity=0;
	setTimeout(function(){window.parent.gotoScene('scene-5.html');},2000);
	}
////////////////////////
function showOn_6() {
	document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s6-continaer').style.display='block';
	document.getElementById('s6-continaer').style.backgroundImage='url(imgs/bg5b.jpg)';
	setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;updatePhoneTxt(3,'You are in the boat\'s Engine Room');},2000);
	
	updateCompleted('');
		document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');
		
	if(localStorage.getItem('v1-q4')=='1'){
	document.getElementById('timer').style.display='none';
	}else{
		document.getElementById('timer').style.display='block';
		countdown();
	}
		
		//countdown();
	}
/////////////////////////
function showAgain_6() {
		document.getElementById('loading').style.display='none';
	document.getElementById('phone').style.display='block';
	document.getElementById('phoneTxt').style.display='block';
	document.getElementById('phone').setAttribute('class','stretchBG ');
	document.getElementById('pango').style.display='block';document.getElementById('pango').setAttribute('class','stretchBG ');
	document.getElementById('frame').style.display='block';document.getElementById('frame').setAttribute('class','stretchBG ');
	document.getElementById('title').style.display='block';document.getElementById('title').setAttribute('class','stretchBG ');
	document.getElementById('s6-continaer').style.display='block';
	if(localStorage.getItem('v1-q2')=='1'){
	document.getElementById('s6-continaer').style.backgroundImage='url(imgs/bg5b.jpg)';
	}

	setTimeout(function(){document.getElementById('s6-continaer').style.opacity=1;updatePhoneTxt(3,'You are in the boat\'s Engine Room');},2000);
	
	updateCompleted('');
		document.getElementById('greenDot1').setAttribute('class','blink_continous');
		document.getElementById('greenDot2').setAttribute('class','blink_continous');
		document.getElementById('greenDot3').setAttribute('class','blink_continous');
		
		mkADecision();
		document.getElementById('timer').style.display='block';
		
			if(localStorage.getItem('v1-q4')=='1'){
	document.getElementById('timer').style.display='none';
	}else{
		countdown();
	}
	}
	